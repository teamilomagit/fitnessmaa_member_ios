//
//  CityListCell.swift
//  FitnessMaa Member
//
//  Created by iLoma on 23/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class CityListCell: UITableViewCell {
    @IBOutlet weak var lblCityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
