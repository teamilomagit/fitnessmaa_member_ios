//
//  AttendanceListCell.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 04/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class AttendanceListCell: UITableViewCell {

    var cellClosure : ((IndexPath) -> ())?

    @IBOutlet weak var baseView: MaterialView!
    
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    var indexPath : IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgViewDP.layer.cornerRadius = 40
        imgViewDP.layer.masksToBounds = true
        self.bringSubview(toFront: imgViewDP)
    }

    @objc func btnCheckClicked()
    {
        if cellClosure != nil {
            cellClosure!(indexPath)
        }
    }
    
    func onMarkAttendanceClicked(closuree: @escaping (IndexPath) -> ())
    {
        cellClosure = closuree
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
