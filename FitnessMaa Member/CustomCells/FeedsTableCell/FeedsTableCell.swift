//
//  FeedsTableCell.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class FeedsTableCell: UITableViewCell {

    var videoClosure : ((IndexPath)->())?
    var likeClosure : ((IndexPath)->())?
    var shareClosure : ((IndexPath)->())?
    var viewProfileClosure : ((IndexPath)->())?
    
    @IBOutlet weak var imgViewBanner: UIImageView!
    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnViewProfile: UIButton!
    
    @IBOutlet weak var PostUserPic: UIImageView!
    @IBOutlet weak var lblPostUserName: UILabel!
    
    var indexPath : IndexPath!
    
    @IBOutlet weak var viewShare: MaterialView!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoClicked), for: .touchUpInside)
        btnShare.addTarget(self, action: #selector(btnShareClicked), for: .touchUpInside)
        btnLike.addTarget(self, action: #selector(btnLikeClicked), for: .touchUpInside)
        btnViewProfile.addTarget(self, action: #selector(btnViewProfileClicked), for: .touchUpInside)
        
        btnLike.setImage(UIImage(named: "ic_like")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnLike.setImage(UIImage(named: "ic_like")?.withRenderingMode(.alwaysTemplate), for: .selected)
    }
    
    @objc func btnPlayVideoClicked() {
        if videoClosure != nil {
            videoClosure!(indexPath)
        }
    }
    
    func onPlayVideoClicked(closure : @escaping (IndexPath)->()) {
        videoClosure = closure
    }
    
    @objc func btnShareClicked() {
        if shareClosure != nil {
            shareClosure!(indexPath)
        }
    }
    
    func onShareBtnClicked(closure : @escaping (IndexPath)->()) {
        shareClosure = closure
    }
    
    @objc func btnViewProfileClicked() {
        if viewProfileClosure != nil {
            viewProfileClosure!(indexPath)
        }
    }
    
    func onViewProfileBtnClicked(closure : @escaping (IndexPath)->()) {
        viewProfileClosure = closure
    }
    
    
    @objc func btnLikeClicked() {
        if likeClosure != nil {
            likeClosure!(indexPath)
        }
    }
    
    func onLikeBtnClicked(closure : @escaping (IndexPath)->()) {
        likeClosure = closure
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewShare.layer.cornerRadius = 25
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
