//
//  ExerciseListCell.swift
//  FitnessMaa
//
//  Created by iLoma on 18/05/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class ExerciseListCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblWeeks: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
 
    var settingClosure : ((IndexPath) -> ())?
    var indexPath : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
        
      

    }
    
   
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
