//
//  PackageHistoryCell.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 14/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class PackageHistoryCell: UITableViewCell {

    @IBOutlet weak var lblPackageName: UILabel!
    @IBOutlet weak var lblDaysLeft: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblPackageAmt: UILabel!
    @IBOutlet weak var lblRegistrationFeesTitle: UILabel!
    @IBOutlet weak var lblRegistrationFees: UILabel!
    @IBOutlet weak var lblDiscountAmtTitle: UILabel!
    @IBOutlet weak var lblDiscountAmt: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
