//
//  AddCentreCell.swift
//  FitnessMaa Member
//
//  Created by iLoma on 28/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class AddCentreCell: UITableViewCell {

    @IBOutlet weak var lblCenterId: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
