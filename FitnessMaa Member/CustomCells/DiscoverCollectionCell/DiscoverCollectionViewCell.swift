//
//  DiscoverCollectionViewCell.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 01/06/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class DiscoverCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgType: UIImageView!
    @IBOutlet weak var btnMenuSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.viewBg.layer.borderWidth = 1
        self.viewBg.layer.borderColor = UIColor(red:128/255, green:128/255, blue:128/255, alpha: 1).cgColor

        lblTitle.font = UIFont.appRegularFont(size: 16)
    }

}
