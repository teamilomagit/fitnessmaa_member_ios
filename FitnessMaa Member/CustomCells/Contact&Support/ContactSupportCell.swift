//
//  ContactSupportCell.swift
//  FitnessMaa
//
//  Created by iLoma on 10/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class ContactSupportCell: UITableViewCell {
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnContent: UIButton!
    var indexPath : IndexPath!
    var onClicked : ((IndexPath) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnContent.titleLabel?.font = UIFont.appMediumFont(size: 16)
        btnContent.setTitleColor(UIColor.appDarkThemeColor, for: .normal)
        lblTitle.font = UIFont.appRegularFont(size: 16)
        btnContent.addTarget(self, action: #selector(btnContentClicked), for: .touchUpInside)
    }
    
    func onDataSelection(closure:@escaping (IndexPath) -> ()) {
        onClicked = closure
    }
    
    
    @objc func btnContentClicked() {
        if onClicked != nil {
            onClicked!(indexPath)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
 
    
}
