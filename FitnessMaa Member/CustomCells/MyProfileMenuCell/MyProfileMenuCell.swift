//
//  MyProfileMenuCell.swift
//  FitnessMaa Member
//
//  Created by iLoma on 28/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class MyProfileMenuCell: UITableViewCell {
    @IBOutlet weak var imgViewMenu: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    var indexPath : IndexPath!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
