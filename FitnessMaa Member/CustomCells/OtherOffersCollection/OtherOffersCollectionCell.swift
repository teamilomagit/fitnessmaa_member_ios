//
//  OtherOffersCollectionCell.swift
//  FitnessMaa Member
//
//  Created by iLoma on 24/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class OtherOffersCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgViewOthersOffer: UIImageView!
    @IBOutlet weak var lblTitleOther: UILabel!
    @IBOutlet weak var lblSubTitleOther: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewOthersOffer.layer.cornerRadius = 10
        imgViewOthersOffer.layer.masksToBounds = true
    }

}
