//
//  TodaysOffersCell.swift
//  FitnessMaa Member
//
//  Created by iLoma on 24/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class TodaysOffersCell: UITableViewCell {
    @IBOutlet weak var imgViewTodaysOffer: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgViewTodaysOffer.layer.cornerRadius = 10
        imgViewTodaysOffer.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
