//
//  Genaral.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
let SCREEN_WIDTH    =   UIScreen.main.bounds.size.width
let SCREEN_HEIGHT   =   UIScreen.main.bounds.size.height

let VIEWMANAGER = ViewManager.shared
let MAYUR_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let PAWAN_STORYBOARD = UIStoryboard(name: "Pawan", bundle: nil)


let CONTACT_NO = "8208545489"

let DRAWER_SELECTION_NOTIFICATION   = "DRAWER_SELECTION_NOTIFICATION"
let TABLEVIEW_MENU_SELECT  = "TABLEVIEW_MENU_SELECT"

let FIREBASE_TOPIC   = "Fitnessmaa_gyms"
let app_del         =   UIApplication.shared.delegate as! AppDelegate
let TERMS       =       "http://fitnessmaa.com/fitnessmaaadmin/docs/terms.html"

//Staging
let API_BASE_URL = "http://fitnessmaa.com/FitnessMaaAPI_V1_Staging/"
let API_BASE_URL_2 = "https://www.fitnessmaa.com/FitnessMaaAPI_V2_Staging/public/api/"
let MEDIA_URL = "https://www.fitnessmaa.com/FitnessMaaAPI_V2_Staging/public/"

let NO_INTERNET     =   "NO_INTERNET"


//Production
//let API_BASE_URL = "http://fitnessmaa.com/fitnessmaaadmin/"
//let API_BASE_URL_2 = "https://www.fitnessmaa.com/fitnessmaa_customer/public/api/"

let LIMIT = 10

let MEDIA_BASE_URL  = "http://fitnessmaa.com/fitnessmaaadmin/"
let MEDIA_BASE_URL2 = "https://www.fitnessmaa.com/FitnessMaaAPI_V2_Staging/public/"

let API_SECRET          =   "DV=.X9<Jju2;kX)@5CUP"

//Login URL
let LOGIN_URL           =  API_BASE_URL_2 + "v1/customer/login"
let LOGIN_NORMAL_USER_URL = API_BASE_URL_2 + "v1/customer/login_normal_user"

let MEMBER_PAYMENT_LIST_URL = API_BASE_URL + "Admin/get_payment_history_with_pagination";
let MEMBER_ATTENDANCE_LIST_URL = API_BASE_URL + "Admin/get_attendance_history_with_pagination"
let MEMBER_PACKAGE_LIST_URL = API_BASE_URL + "Admin/get_my_packages_with_pagination";

let MEMBER_PROFILE_URL = API_BASE_URL_2 + "v1/customer/get_user_profile";


let CURRENCY_SYMBOL = "₹"

// Exercise
let GET_EXERCISE_LIST =  API_BASE_URL_2 + "v1/exercise/get_member_excercise_plan"

// Feedback
let SEND_FEEDBACK_API = API_BASE_URL_2 + "v1/feedback/add_feedback"

//Register
let REGISTRATION_API   =  API_BASE_URL_2 + "v1/customer/register_app_user"
let GET_COUNTRIES_API     = API_BASE_URL + "Admin/get_countries"
let GET_STATE_API         = API_BASE_URL +  "Admin/get_states_by_country"
let GET_CITIES_API        = API_BASE_URL +  "Admin/get_cities_by_state"

//Offers
let GET_OFFERS_API = API_BASE_URL_2 + "v1/offers/get_by_city"
let GET_OFFER_DETAIL_API = API_BASE_URL_2 + "v1/offers/get_offer_details"
let GET_OFFER_INTERESTED_API = API_BASE_URL_2+"v1/offers/interested_in_offer"

//Feeds
let GET_LATEST_FEED_API =   API_BASE_URL_2 + "v1/feeds/latest_feeds"
let GET_FEED_LIKES_API = API_BASE_URL_2 + "v1/feeds/like_feed"
let GET_FEED_UNLIKE_API = API_BASE_URL_2 + "v1/feeds/unlike_feed"
let GET_FEED_VIEWS_API = API_BASE_URL_2 + "v1/feeds/increment_views"
let GET_FEED_INCREMENT_VIEW_API = API_BASE_URL_2 + "v1/feeds/increment_views"

// Recipe Api

let GET_LATEST_RECIPE_API = API_BASE_URL_2 + "v1/recipes/latest_recipes"
let UNLIKE_RECIPE_API = API_BASE_URL_2 + "v1/recipes/unlike_recipe"
let LIKE_RECIPE_API = API_BASE_URL_2 + "v1/recipes/like_recipe"
let GET_RECIPE_INCREMENT_VIEW_API = API_BASE_URL_2 + "v1/recipes/increment_views"


//Notification
let GET_NOTIFICATION_API = API_BASE_URL_2 + "v1/notification/user_notifications"



// VIEW PROFILE API
let GET_NUTRITIONIST_PROFILE_API = API_BASE_URL_2 + "v1/nutritionist/get_profile"
let ADD_RETTING_API = API_BASE_URL_2 + "v1/nutritionist/add_rating"

// Social page Detail
let GET_SOCIAL_PAGE_DETAIL_API = API_BASE_URL_2 + "v1/social_page/get_profile"
let ADD_RETTING_SOCIAL_API = API_BASE_URL_2 + "v1/v1/social_page/add_rating/add_rating"

// connected centre
let GET_CONNECTED_CENTER_API = API_BASE_URL_2 + "v1/customer/get_connected_centers"

// update profile

let UPDATE_PROFILE_API = API_BASE_URL_2 + "v1/customer/update_details"
let UPDATE_PROFILE_PHTO_API =  MEDIA_BASE_URL2 + "media/update_user_photo"

// forgot pass
let FORGOT_PASS_API = API_BASE_URL_2 + "v1/customer/forgot_password"
let RESET_PASS_API = API_BASE_URL_2 + "v1/customer/forgot_change_password"

// change password
let CHANGE_PASSWORS_API = API_BASE_URL_2 + "v1/customer/change_password"

// general setting
let GENERAL_SETTING_API = API_BASE_URL_2 + "v1/utils/general_settings"

// update app
let CHECK_APP_UPDATE_API = API_BASE_URL_2 + "v1/utils/user/check_app_update"


func localize(string:String) -> String {
    let path = Bundle.main.path(forResource: Preferences.getAppLanguage(), ofType: "lproj")
    let bundle = Bundle(path: path!)
    let localizedStr = NSLocalizedString(string, tableName: nil, bundle: bundle!, value: "", comment: "")
    return localizedStr
}
let ENGLISH =   "en"
let ARABIC  =   "ar"


let APP_STORE_URL   =   "https://itunes.apple.com/in/app/FitnessMaa/id1463653290?mt=8"
