//
//  CustomAlertView.swift
//  BTS
//
//  Created by Pawan Ramteke on 31/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
class CustomAlertView
{
    class func showAlert(withTitle:String,messsage:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: withTitle, message: messsage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
    
    class func showAlertWithYesNo(title:String,messsage:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: title, message: messsage, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
            if completion != nil {
                completion!()
            }
        }
        
        let noAction = UIAlertAction(title: "No", style: .default) { (action) in
            
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(yesAction)
        alertVC.addAction(noAction)

        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
    }
    
    class func showPermissionEnableAlert(title:String,message:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        let noAction = UIAlertAction(title: "Not Now", style: .default) { (action) in
            
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)

    }
    
    class func showAlertwithLogin(withTitle:String,messsage:String, completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: withTitle, message: messsage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Login", style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        let noAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
            alertVC.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        alertVC.addAction(noAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
    
    class func showAlertWithRetry(completion:(()->Void)?)
    {
        let alertVC = UIAlertController(title: "Opps!!!", message: "Internet Not Available Please Try Again Later", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Retry", style: .default) { (action) in
            
            if completion != nil {
                completion!()
            }
            VIEWMANAGER.topMostController().dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(okAction)
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
   
    
}
