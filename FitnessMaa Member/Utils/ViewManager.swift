//
//  ViewManager.swift
//  BTS
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
//import Toast_Swift

class ViewManager : NSObject,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    static let shared = ViewManager()
    var currentUser: UserModel?

    var tryAgainClosure : ((String)->())?

    var imagePickerBlock : ((UIImage,String)->())?
    var arrBannersData : NSMutableArray!

    private override init(){
    }
    
    func topMostController() -> UIViewController
    {
        var topViewController = UIApplication.shared.keyWindow?.rootViewController
        while true {
            if topViewController?.presentedViewController != nil {
                topViewController = topViewController?.presentedViewController
            }
            
            else if (topViewController is UINavigationController) {
                let nav = topViewController as? UINavigationController
                
                topViewController = nav?.topViewController
            }
            else if (topViewController is UITabBarController) {
                let tab = topViewController as? UITabBarController
                topViewController = tab?.selectedViewController
                break
            }
            else {
                break
            }
        }
        return topViewController!
    }
    
    func showActionSheet(_ actionSheetData:NSArray,completion:@escaping (String) -> Void)
    {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        for i in actionSheetData {
            let alertAction = UIAlertAction(title: i as? String, style: .default, handler: { (action) in
                completion(action.title!)
            })
            alert.addAction(alertAction)
            
        }
        self.topMostController().present(alert, animated: true, completion: nil)
    }
    
    func showImagePicker(sourceType : UIImagePickerController.SourceType,block : @escaping (UIImage,String) -> Void)
    {
        self.imagePickerBlock = block
        let imgPicker =  UIImagePickerController()
        imgPicker.sourceType = sourceType
        imgPicker.delegate = self
        self.topMostController().present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        do{
            let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
            let documentsDirectory : URL = paths[0]
            let localPath = documentsDirectory.appendingPathComponent("Profile_pic.png")
            
            let data = UIImageJPEGRepresentation(image, 0.5)
            
            try data?.write(to: localPath)
            
            if self.imagePickerBlock != nil {
                self.imagePickerBlock!(image,localPath.path)
            }
        }
        catch{
            
        }
        self.topMostController().dismiss(animated: true, completion: nil)
    }
    
    func showActivityIndicator()
    {
       // SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setForegroundColor(UIColor.appDarkThemeColor)
        SVProgressHUD.setRingThickness(3)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        SVProgressHUD.show()

    }
    
    func hideActivityIndicator()
    {
        SVProgressHUD.dismiss()
    }
    
    func showToast(_ message:String?)
    {
        ToastManager.shared.style.messageFont = UIFont.appRegularFont(size: 18)
        UIApplication.shared.keyWindow?.makeToast(message)
    }

   
    
    func formattedDate(strDate:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: strDate)
        if date != nil {
            formatter.dateFormat = "dd MMM yy"
            return formatter.string(from: date!)
        }
        return ""
    }
    
    func formattedTime(strDate:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = formatter.date(from: strDate)
        if date != nil {
            formatter.dateFormat = "hh:mm a"
            return formatter.string(from: date!)
        }
        return ""
    }
    
    
    func tableBGView() -> UILabel {
        let lbl = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        lbl.font = UIFont.appRegularFont(size: 18)
        lbl.textAlignment = .center
        lbl.text = "No data found"
        return lbl
    }
    
    
    func getProperImageUrl(strUrl:String?) -> String?
    {
        if strUrl == nil {
            return nil
        }
        return strUrl!.replacingOccurrences(of: " ", with: "%20")
    }
    func showUpdateAlert(isOptional : Bool)
    {
        let alertVC = UIAlertController(title: "Update App", message: "You are using older version of APP", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "UPDATE", style: .default) { (action) in
            
            if let url = URL(string: APP_STORE_URL),
                UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url, options: [:]) { (opened) in
                    if(opened){
                        print("App Store Opened")
                        VIEWMANAGER.showToast("App Store Opened")
                    }
                }
            } else {
                //  print("Can't Open URL on Simulator")
                VIEWMANAGER.showToast("Can't Open URL on Simulator")
            }
            
        }
        
        if isOptional {
        let laterAction = UIAlertAction(title: "Later", style: .default) { (action) in
            
                    alertVC.dismiss(animated: true, completion: nil)
                }
            alertVC.addAction(laterAction)
        
        }
        alertVC.addAction(okAction)
        
        
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
 
    
    func showNetworkConnectionAlert()
    {
        let alertVC = UIAlertController(title: "Unable to connect with server", message: "Please check your internet and try again later.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "TRY AGAIN", style: .default) { (action) in
            if self.tryAgainClosure != nil {
                self.tryAgainClosure!(action.title!)
            }
        }
        
        alertVC.addAction(okAction)
        VIEWMANAGER.topMostController().present(alertVC, animated: true, completion: nil)
        
    }
    
    func onTryAgainClicked(apiCall : @escaping (String) -> Void)
    {
        tryAgainClosure = apiCall
    }
}
