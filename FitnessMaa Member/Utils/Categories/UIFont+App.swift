//
//  UIFont+App.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 05/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    class func appRegularFont(size:CGFloat) -> UIFont
    {
        return UIFont(name: "OpenSans", size: size)!
    }
    
    class func appMediumFont(size:CGFloat) -> UIFont
    {
        return UIFont(name: "OpenSans-Semibold", size: size)!
    }
    class func appBoldFont(size:CGFloat) -> UIFont
    {
        return UIFont(name: "OpenSans-Bold", size: size)!
    }
}
