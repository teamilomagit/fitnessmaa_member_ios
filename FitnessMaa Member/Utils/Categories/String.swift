//
//  String.swift
//  FitnessMaaModule
//
//  Created by Pawan Ramteke on 02/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import Foundation

extension String {
    func floatValue() -> Float {
        let number = NumberFormatter().number(from: self)
        return (number?.floatValue)!
    }
    
    func trim() -> String
    {
        return self.trimmingCharacters(in:CharacterSet.whitespaces)
    }
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

}
