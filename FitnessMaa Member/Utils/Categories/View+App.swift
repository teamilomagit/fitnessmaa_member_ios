//
//  View+App.swift
//  FitnessMaaModule
//
//  Created by Pawan Ramteke on 04/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    
    func addGradientColor(color1:UIColor,color2:UIColor)
    {
        let gradient = CAGradientLayer()
        gradient.frame = self.frame
        gradient.colors = [color1.cgColor, color2.cgColor]
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    
        func roundCorners(corners: UIRectCorner, radius: CGFloat) {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        
    }
}
