//
//  UIColor+App.swift
//  FitnessMaaModule
//
//  Created by Pawan Ramteke on 04/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import Foundation
import UIKit
extension UIColor
{
    public class var appDarkThemeColor: UIColor
    {
        return UIColor(red:0.96, green:0.00, blue:0.12, alpha:1.0)
    }
    public class var appLightThemeColor: UIColor
    {
        return UIColor(red: 0.88, green: 0.04, blue: 0.33, alpha: 1.0)
    }
    public class var navigationStripeColor: UIColor
    {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    public class var controllerBGColor : UIColor
    {
        return UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.0)
    }
    
}
