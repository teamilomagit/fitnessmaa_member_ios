//
//  ForgetPassValidationView.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 09/06/19.
//  Copyright © 2019 Ashwini Rokade. All rights reserved.
//

import UIKit

class ForgetPassValidationView: UIView {

    @IBOutlet weak var btnSubmit: AppCustomButton!
    @IBOutlet weak var txtFieldEmail: AppTextField!
    var btnSubmitClickClosure : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
        
        btnSubmit.addTarget(self, action: #selector(btnSubmitClicked), for: .touchUpInside)
    }

    @IBAction func btnBackgroundClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func btnSubmitClicked(){
        
        if btnSubmitClickClosure != nil{
            btnSubmitClickClosure!()
        }
    }
    
    func onSubmitBtnClicked(closure : @escaping ()->()){
        btnSubmitClickClosure = closure
    }
    
    
    
    
    
}
