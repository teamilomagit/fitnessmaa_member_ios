//
//  CustomTextField.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit


class CustomTextField: UITextField,UITextFieldDelegate {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup() {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",attributes: [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font : UIFont.appRegularFont(size: 20)])
        
        self.font = UIFont.appRegularFont(size: 20)
        self.layer.borderWidth = 0.5
        self.delegate = self

        self.layer.borderColor = UIColor.gray.cgColor

    }
    
    func setTextFieldTag(tag:TextFieldTag)
    {
        self.tag = tag.rawValue
        if tag == .TAG_MOBILE{
            self.keyboardType = .phonePad
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
            if self.tag == TextFieldTag.TAG_MOBILE.rawValue
            {
                return updatedText.count <= 10
            }
        }
        return true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height/2
    }

}
