//
//  VideoPlayerView.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 11/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class VideoPlayerView: UIView {

    init(frame: CGRect,videoURL : String) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black
        self.alpha = 0
        let player = YTPlayerView(frame: CGRect(x: 0, y: self.frame.midY - 100, width: self.frame.size.width, height: 250))
        self.addSubview(player)
        let playvarsDic = [ "controls" : 0, "playsinline" : 0, "autohide" : 0, "showinfo" : 0, "autoplay": 1, "origin": "https://www.youtube.com" , "modestbranding" : 1 ] as [String : Any]
        player.load(withVideoId: videoURL.components(separatedBy: "watch?v=")[1], playerVars: playvarsDic)
        
        let btnClose = UIButton(frame: CGRect(x: 20, y: 30, width: 40, height: 40))
        btnClose.setImage(UIImage(named: "ic_close"), for: .normal)
        btnClose.layer.cornerRadius = 20
        btnClose.addTarget(self, action: #selector(ImageViewerController.btnCloseClicked), for: .touchUpInside)
        self.addSubview(btnClose)
        
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
    }
    
    @objc func btnCloseClicked()
    {
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
}


