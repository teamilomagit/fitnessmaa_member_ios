//
//  DropDownListView.swift
//  BTS
//
//  Created by Pawan Ramteke on 01/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DropDownListView: UIView,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    var doneClosure : ((String)->())?
    var doneClosureModel : ((AnyObject)->())?


    var baseView : UIView!
    var tblView : UITableView!

    var tblDataArr = NSArray()
    var arrSearch = NSMutableArray()
    var isFiltered = false
    
    var activityLoader: UIActivityIndicatorView!

    let BASE_HEIGHT = (SCREEN_HEIGHT * 0.6)
    init(frame: CGRect,title: String, data:[String]) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.alpha = 0
        
      //  tblDataArr = data as NSArray
        
        let btnBG = UIButton(frame: self.frame)
        btnBG.addTarget(self, action: #selector(DropDownListView.btnCloseClicked), for: .touchUpInside)
        self.addSubview(btnBG)
        
        baseView = UIView(frame: CGRect(x: 10, y: self.frame.midY - BASE_HEIGHT/2, width: self.frame.size.width - 20, height: BASE_HEIGHT))
        baseView.backgroundColor = .white
        baseView.layer.cornerRadius = 10
        baseView.layer.masksToBounds = true
        self.addSubview(baseView)
        
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: baseView.frame.size.width, height: 50))
        lblTitle.backgroundColor = UIColor.appDarkThemeColor
        baseView.addSubview(lblTitle)
        lblTitle.font = UIFont.appBoldFont(size: 20)
        lblTitle.textAlignment = .center
        lblTitle.text = title
        lblTitle.textColor = .white
        
        let txtFieldSearch = UITextField(frame: CGRect(x: 10, y: lblTitle.frame.maxY + 10, width: baseView.frame.size.width - 20, height: 40))
        txtFieldSearch.font = UIFont.appRegularFont(size: 18)
        txtFieldSearch.placeholder = "Search.."
        txtFieldSearch.borderStyle = .roundedRect
        txtFieldSearch.delegate = self
        baseView.addSubview(txtFieldSearch)

        
        tblView = UITableView(frame: CGRect(x: 0, y: txtFieldSearch.frame.maxY+10, width: baseView.frame.size.width, height: baseView.frame.size.height - txtFieldSearch.frame.maxY-10))
        tblView.dataSource = self
        tblView.delegate = self
        baseView.addSubview(tblView)
        
        
        
        activityLoader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityLoader.center = tblView.center
        self.addSubview(activityLoader)
        activityLoader.startAnimating()
        
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1;
        }
    }
    
    
    func updateList(list: NSArray,type: String){
        
        tblDataArr = list
        tblView.reloadData()
        activityLoader.stopAnimating()
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? arrSearch.count : tblDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "CellId")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "CellId")
        }
        
        var name = ""
        if tblDataArr[indexPath.row] is CountryModel {
            let model = tblDataArr[indexPath.row] as! CountryModel
            name = model.name
        }
         else if tblDataArr[indexPath.row] is StateModel {
            let model = tblDataArr[indexPath.row] as! StateModel
            name = model.name
        }
        else if tblDataArr[indexPath.row] is CityModel {
            let model = tblDataArr[indexPath.row] as! CityModel
            name = model.name
        }
        
        cell?.textLabel?.text = name

        
        cell?.textLabel?.font = UIFont.appRegularFont(size: 18)
       // cell?.textLabel?.text = isFiltered ? arrSearch[indexPath.row] as? String :  tblDataArr[indexPath.row] as? String
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if doneClosure != nil {
            doneClosure!((isFiltered ? arrSearch[indexPath.row] as? String :  tblDataArr[indexPath.row] as? String)!)
        }
        
        if doneClosureModel != nil {
            doneClosureModel!((isFiltered ? arrSearch[indexPath.row] as AnyObject  :  tblDataArr[indexPath.row] as AnyObject ))
        }
        btnCloseClicked()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let nsString = textField.text! as NSString
        let resultingString = nsString.replacingCharacters(in: range, with: string)
        applyFilter(text: resultingString)
        return true
    }
    
    func applyFilter(text:String)
    {
        if text.count == 0
        {
            isFiltered = false
            tblView.reloadData()
            return
        }
        else
        {
            isFiltered = true
        }
        
        let terms = text.components(separatedBy: CharacterSet.whitespaces)
        let subpredicates = NSMutableArray()
        
        for term in terms
        {
            if term.count == 0
            {
                continue
            }
            let p = NSPredicate(format:"SELF.name contains[c] %@",text)
            subpredicates.add(p)
        }
        
        let filter = NSCompoundPredicate(orPredicateWithSubpredicates: subpredicates as! [NSPredicate])
        let arr = tblDataArr.filtered(using: filter)
        arrSearch.removeAllObjects()
        arrSearch.addObjects(from: arr)
        tblView.reloadData()
    }
    
    
    @objc func btnCloseClicked()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
    func onDoneClicked(back : @escaping (AnyObject) -> Void)
    {
        doneClosureModel = back
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
