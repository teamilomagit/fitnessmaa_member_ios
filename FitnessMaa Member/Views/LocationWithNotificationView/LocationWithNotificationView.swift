//
//  LocationWithNotificationView.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 28/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class LocationWithNotificationView: UIView {
    
    var notificationClosure : (() -> ())?
    var locationClosure : (() -> ())?
    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblYourLocation: UILabel!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var view: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        //view.backgroundColor = UIColor.appDarkThemeColor
         btnNotification.addTarget(self, action: #selector(btnNotificationClicked), for: .touchUpInside)
        btnLocation.addTarget(self, action: #selector(btnLocationClicked), for: .touchUpInside)
        lblCityName.font = UIFont.appBoldFont(size: 12)
        lblYourLocation.font = UIFont.appBoldFont(size: 12)
        notifucationBadge()
    }
    
    func onNotificationClicked(closure : @escaping ()->()) {
        notificationClosure = closure
    }
    
    func onLocationClicked(closure :@escaping ()->()){
        locationClosure = closure
    }
    
    @objc func btnLocationClicked(){
        if locationClosure != nil{
            locationClosure!()
        }
    }
    
    @objc func btnNotificationClicked() {
        if notificationClosure != nil {
            notificationClosure!()
        }
    }
    func notifucationBadge(){
        let viewBdg = UILabel(frame: CGRect(x: 15, y: 5, width: 10, height: 10))
        viewBdg.layer.borderColor = UIColor.lightGray.cgColor
        viewBdg.layer.borderWidth = 0.5
        viewBdg.layer.cornerRadius = viewBdg.bounds.size.height / 2
        viewBdg.textAlignment = .center
        viewBdg.layer.masksToBounds = true
        viewBdg.backgroundColor = .white
        btnNotification.addSubview(viewBdg)
    }
    

}
