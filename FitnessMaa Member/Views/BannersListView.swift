//
//  BannersListView.swift
//  Odito
//
//  Created by Pawan Ramteke on 30/01/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
import ScaledVisibleCellsCollectionView

class BannersListView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //var arrImages  =  ["img1","img2","img3","img4","img5","img6"]

    
    var SelectClosure : ((Int)->())?
    var _collectionView : UICollectionView!
    private var pageControl : UIPageControl!
    private var index = 1
    var timer : Timer!
    var bannerData = NSArray() {
        didSet {
            if bannerData.count > 0 {
                pageControl.isHidden = false
                startTimer()
                pageControl.numberOfPages = bannerData.count
            }
            _collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        
        let layout = ASJCollectionViewFillLayout()
        layout.direction = .horizontal
        layout.itemSpacing = 0
        layout.itemLength = SCREEN_WIDTH
        layout.stretchesLastItems = false
        _collectionView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        _collectionView.dataSource = self
        _collectionView.delegate = self
        _collectionView.backgroundColor = UIColor.clear
        _collectionView.isPagingEnabled = true
        _collectionView.showsHorizontalScrollIndicator = false
        self.addSubview(_collectionView)
        
        _collectionView.enableAutoLayout()
        _collectionView.leadingMargin(pixels: 0)
        _collectionView.trailingMargin(pixels: 0)
        _collectionView.topMargin(pixels: 0)
        _collectionView.fixedHeight(pixels: 200)

        _collectionView.register(BannerCollectionCell.self, forCellWithReuseIdentifier: "CellID")
        
        _collectionView.setScaledDesginParam(scaledPattern: .HorizontalCenter, maxScale: 1.0, minScale: 0.5, maxAlpha: 1.0, minAlpha: 0.5)

        
        pageControl = UIPageControl()
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = UIColor.appDarkThemeColor
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.isHidden = true
        self.addSubview(pageControl)
        pageControl.enableAutoLayout()
        pageControl.centerX()
        pageControl.belowToView(view: _collectionView, pixels: 10)
        pageControl.fixedHeight(pixels: 20)
        pageControl.bottomMargin(pixels: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return bannerData.count
    }
    
    func collectionBGView()->UIActivityIndicatorView
    {
        let loader = UIActivityIndicatorView(frame: self.bounds)
        loader.color = UIColor.appDarkThemeColor
        loader.startAnimating()
        return loader
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! BannerCollectionCell
        let model = bannerData[indexPath.row] as! TopOffer
        cell.imgView.layer.cornerRadius = 10
        let imgurl = MEDIA_URL + model.image
        
        cell.imgView.sd_setImage(with: URL(string:imgurl), placeholderImage: UIImage(named:"placeholder_img"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if SelectClosure != nil {
            SelectClosure!(indexPath.row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        _collectionView.scaledVisibleCells()
    }
    
    func startTimer()
    {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector:#selector(timerEvent) , userInfo: nil, repeats: true)
    }
    
    @objc func timerEvent()
    {
        if index < bannerData.count {
            _collectionView.setContentOffset(CGPoint(x:SCREEN_WIDTH * CGFloat(index),y:0), animated: true)
            index = index + 1
        }
        else {
            index = 1
            _collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
        }
        pageControl.currentPage = index - 1
    }
    
    func onSelectImage(closure : @escaping (Int)->())
    {
        SelectClosure = closure
    }
}

class BannerCollectionCell: UICollectionViewCell {
    
    var imgView : UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let baseView = MaterialView(frame: self.frame)
        self.addSubview(baseView)
        baseView.enableAutoLayout()
        baseView.leadingMargin(pixels: 10)
        baseView.trailingMargin(pixels: 10)
        baseView.topMargin(pixels: 0)
        baseView.bottomMargin(pixels: 0)
        
        imgView = UIImageView()
        imgView.image = UIImage(named: "img_institute_banner")
        imgView.contentMode = .scaleToFill
        imgView.layer.cornerRadius = 10
        imgView.layer.masksToBounds = true
        baseView.addSubview(imgView)
        imgView.enableAutoLayout()
        imgView.leadingMargin(pixels: 0)
        imgView.trailingMargin(pixels: 0)
        imgView.topMargin(pixels: 0)
        imgView.bottomMargin(pixels: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
