//
//  CustomAppButton.swift
//  FitnessMaa
//
//  Created by iLoma on 04/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//
import UIKit
import Foundation
extension UIButton
{
//    func applyGradient(colors: [CGColor])
//    {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = colors
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
//        gradientLayer.frame = self.bounds
//        self.layer.addSublayer(gradientLayer)
//    }
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
       // self.layer.addSublayer(gradient)
    }
}
