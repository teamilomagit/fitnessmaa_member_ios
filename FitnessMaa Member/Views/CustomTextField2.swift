//
//  AppTextField.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 07/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

enum TextFieldTag3 : Int {
    case TAG_MOBILE = 100
    case TAG_DATE_PICKER = 101
    case TAG_ACTION_SHEET = 102
    case TAG_DROP_DOWN   = 103
}

class CustomTextField2: UITextField,UITextFieldDelegate {
    var actionSheetData : NSArray!
    var dropDownData : [String]!

    @IBInspectable var leftImage : String = ""  {
        didSet{
            self.leftViewMode = .always
            
            let btn = UIButton(frame: CGRect(x: 0, y:0, width: 40, height: 50))
            btn.setImage(UIImage(named: leftImage)?.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.tintColor = UIColor.darkGray
            btn.contentMode = .center
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.leftView = btn
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.delegate = self
        self.borderStyle = .none
        self.font = UIFont.appRegularFont(size: 16)
        if leftImage == "" {
            self.leftViewMode = .always
            self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 50))
        }
    }
    
    func setTextFieldTag(tag:TextFieldTag3)
    {
        self.tag = tag.rawValue
        if tag == .TAG_MOBILE {
            self.keyboardType = .phonePad
        }
        if tag == .TAG_ACTION_SHEET || tag == .TAG_DROP_DOWN {
            self.rightViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            imgView.image = UIImage(named: "ic_down_arrow")
            imgView.contentMode = .scaleAspectFit
            self.rightView = imgView
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
            if self.tag == TextFieldTag.TAG_MOBILE.rawValue
            {
                return updatedText.count <= 10
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == TextFieldTag.TAG_DATE_PICKER.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
            showDatePicker()
            return false
        }
        if textField.tag == TextFieldTag.TAG_ACTION_SHEET.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
            showActionSheet()
            return false
        }
        
        if textField.tag == TextFieldTag.TAG_DROP_DOWN.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
         //   showDropDown()
            return false
        }
        return true
    }
    
    
    func showDatePicker()
    {
        let datePicker = DatePickerView(frame: UIScreen.main.bounds)
        VIEWMANAGER.topMostController().view.addSubview(datePicker)
//        datePicker.onDoneClicked { (date) in
//            self.text = self.formattedDate(selDate: date)
//        }
    }
    
    func formattedDate(selDate:Date!) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy/MM/dd"
        var dateToPass: String? = nil
        
        dateToPass = dateFormat.string(from: selDate)
        
        return dateToPass!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
    func showActionSheet()
    {
        if actionSheetData == nil {
            return
        }
        
        VIEWMANAGER.showActionSheet(actionSheetData) { (title) in
            self.text = title
        }
    }
    
//    func showDropDown()
//    {
//        if dropDownData == nil {
//            return
//        }
//        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: self.placeholder!,data:dropDownData)
//        UIApplication.shared.keyWindow?.addSubview(dropDown)
//        dropDown.onDoneClicked { (data) in
//            self.text = data
//        }
//    }

    
}
