//
//  AppCustomButton.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 12/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class AppCustomButton2: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont.appMediumFont(size: 18)
        self.setTitleColor(UIColor.white, for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        self.applyGradient(colours: [UIColor.appDarkThemeColor, UIColor.appLightThemeColor])
        self.setTitle(self.titleLabel?.text, for: .normal)
    }
    
    

}
