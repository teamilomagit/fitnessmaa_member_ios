//
//  AddCentreView.swift
//  FitnessMaa Member
//
//  Created by iLoma on 28/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class AddCentreView: UIView {
    
    var successClosure : ((Any)->())?

    @IBOutlet weak var txtFieldCentreId: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var btnConnectCentre: AppCustomButton2!
    var cModel : CentreModel!
    var arrData = NSMutableArray()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
            
        }
       txtFieldCentreId.text = "1065"
       txtFieldEmail.text = "amolhirkane@gmail.com"
       txtFieldPassword.text = "abcd1234"

        let model = CentreModel(fromDictionary: [:])
        model.address = "Flat No. 301,Armaam Exotica, KT Nagar Nagpur, Maharashtra"
        model.centreName = "Test Centre"
        model.centre = "1065"
        print(model)
        self.arrData.add(model)
    }
    
    
    
    
    func onAddCentreSuccess(closure : @escaping (Any)->())
    {
        successClosure = closure
    }
    
    
    
    
    
    
    
    @IBAction func btnConnectCentreClicked(_ sender: Any) {
        removePopup()
        if self.successClosure != nil {
            self.successClosure!(arrData)
        }
    }
    
    @IBAction func btnClosePopup(_ sender: Any) {
        removePopup()
      
    }
    
    func validate()->Bool
    {
        if  txtFieldCentreId.text?.trim().count == 0{
            self.makeToast("Please enter Centre Id")
            return false
        }
        if txtFieldEmail.text?.count == 0 {
            self.makeToast("Please enter Email")
            return false
        }
        if txtFieldPassword.text?.count == 0 {
            self.makeToast("Please enter Password")
            return false
        }
        
        return true
    }
    
    func removePopup(){
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
        
    }
}
