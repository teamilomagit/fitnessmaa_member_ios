//
//  FrontTabBarView.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class FrontTabBarView: MaterialView {

    var frontTabClosure : ((Int)->())?
    
    @IBOutlet weak var viewDiscover: UIView!
    @IBOutlet weak var imgDiscover: UIImageView!
    @IBOutlet weak var lblDiscover: UILabel!
    
    @IBOutlet weak var viewFeeds: UIView!
    @IBOutlet weak var imgFeeds: UIImageView!
    @IBOutlet weak var lblFeeds: UILabel!

    @IBOutlet weak var viewOffers: UIView!
    @IBOutlet weak var imgOffers: UIImageView!
    @IBOutlet weak var lblOffers: UILabel!

    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfile: UILabel!
    
    @IBOutlet weak var viewHealthyDiet: UIView!
    @IBOutlet weak var imgDiet: UIImageView!
    @IBOutlet weak var lblDiet: UILabel!
    
    @IBOutlet weak var separatorWdthConstr: NSLayoutConstraint!
    @IBOutlet weak var separatorLeadingConstr: NSLayoutConstraint!
    
    let scale : CGFloat = 1.4
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 0
        separatorWdthConstr.constant = SCREEN_WIDTH / 3
        lblDiscover.textColor = UIColor.appDarkThemeColor

        imgDiscover.image = UIImage(named: "tab_discover")?.withRenderingMode(.alwaysTemplate)
        imgDiscover.tintColor = UIColor.appDarkThemeColor
        
        imgFeeds.image = UIImage(named: "tab_feed")?.withRenderingMode(.alwaysTemplate)
        imgFeeds.tintColor = UIColor.darkGray
        
        imgOffers.image = UIImage(named: "tab_offers")?.withRenderingMode(.alwaysTemplate)
        imgOffers.tintColor = UIColor.darkGray

        imgProfile.image = UIImage(named: "tab_profile_selected")?.withRenderingMode(.alwaysTemplate)
        imgProfile.tintColor = UIColor.darkGray
        
        imgDiet.image = UIImage(named: "tab_recipes_selected")?.withRenderingMode(.alwaysTemplate)
        imgDiet.tintColor = UIColor.darkGray
    }
    
    @IBAction func btnDiscoverClicked(_ sender: UIButton) {
        reset()
        imgDiscover.tintColor = UIColor.appDarkThemeColor
        lblDiscover.textColor = UIColor.appDarkThemeColor
        if frontTabClosure != nil {
            frontTabClosure!(sender.tag)
        }
    }
    
    @IBAction func btnFeedsClicked(_ sender: UIButton) {
        reset()
        imgFeeds.tintColor = UIColor.appDarkThemeColor
        lblFeeds.textColor = UIColor.appDarkThemeColor
        if frontTabClosure != nil {
            frontTabClosure!(sender.tag)
        }
    }
    
    @IBAction func btnOffersClicked(_ sender: UIButton) {
        reset()
        imgOffers.tintColor = UIColor.appDarkThemeColor
        lblOffers.textColor = UIColor.appDarkThemeColor
        if frontTabClosure != nil {
            frontTabClosure!(sender.tag)
        }
    }
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        reset()
        imgProfile.tintColor = UIColor.appDarkThemeColor
        lblProfile.textColor = UIColor.appDarkThemeColor
        if frontTabClosure != nil {
            frontTabClosure!(sender.tag)
        }
    }
    
    @IBAction func btnDietClicked(_ sender: UIButton) {
        reset()
        imgDiet.tintColor = UIColor.appDarkThemeColor
        lblDiet.textColor = UIColor.appDarkThemeColor
        if frontTabClosure != nil {
           frontTabClosure!(sender.tag)
        }
    }
    
    
    func reset() {
        imgDiscover.tintColor = .darkGray
        imgFeeds.tintColor = .darkGray
        imgOffers.tintColor = .darkGray
        imgProfile.tintColor = .darkGray
        imgDiet.tintColor = .darkGray
        
        lblDiscover.textColor = .darkGray
        lblFeeds.textColor = .darkGray
        lblOffers.textColor = .darkGray
        lblProfile.textColor = .darkGray
        lblDiet.textColor = .darkGray
    }
    
    func changeSeparatorLocations(leading : CGFloat) {
        separatorLeadingConstr.constant = leading
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func onFrontTabClicked(closure : @escaping (Int)->()) {
        frontTabClosure = closure
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addGradientColor(color1: UIColor.appDarkThemeColor, color2: UIColor.appLightThemeColor)
    }
}
