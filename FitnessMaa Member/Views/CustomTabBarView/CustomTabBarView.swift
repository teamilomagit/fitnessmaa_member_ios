//
//  CustomTabBarView.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class CustomTabBarView: UIView {

    var tabClosure : ((Int)->())?
    
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var imgPayment: UIImageView!
    
    @IBOutlet weak var viewAttendance: UIView!
    @IBOutlet weak var imgAttendance: UIImageView!
    
    @IBOutlet weak var viewPackages: UIView!
    @IBOutlet weak var imgPackages: UIImageView!
    
    @IBOutlet weak var separatorWdthConstr: NSLayoutConstraint!
    @IBOutlet weak var separatorLeadingConstr: NSLayoutConstraint!
   // mbmbmb
    
    let scale : CGFloat = 1.4
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorWdthConstr.constant = SCREEN_WIDTH / 3
        viewPayment.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        imgPayment.transform = CGAffineTransform(scaleX: scale, y: scale)
    }
    
    @IBAction func btnPaymentClicked(_ sender: UIButton) {
        reset()
        viewPayment.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        UIView.animate(withDuration: 0.1) {
            self.imgPayment.transform = CGAffineTransform(scaleX: self.scale, y: self.scale)
        }
        changeSeparatorLocation(leading: 0)
        if tabClosure != nil {
            tabClosure!(sender.tag)
        }
    }
    
    @IBAction func btnAttendanceClicked(_ sender: UIButton) {
        reset()
        viewAttendance.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        UIView.animate(withDuration: 0.1) {
            self.imgAttendance.transform = CGAffineTransform(scaleX: self.scale, y: self.scale)
        }
        changeSeparatorLocation(leading: SCREEN_WIDTH / 3)
        if tabClosure != nil {
            tabClosure!(sender.tag)
        }
    }
    
    @IBAction func btnPackagesClicked(_ sender: UIButton) {
        reset()
        viewPackages.backgroundColor = UIColor.black.withAlphaComponent(0.2)
         UIView.animate(withDuration: 0.1) {
            self.imgPackages.transform = CGAffineTransform(scaleX: self.scale, y: self.scale)
         }
        changeSeparatorLocation(leading: (SCREEN_WIDTH / 3) * 2)
        if tabClosure != nil {
            tabClosure!(sender.tag)
        }
    }
    
    func reset() {
        viewPayment.backgroundColor = .clear
        viewAttendance.backgroundColor = .clear
        viewPackages.backgroundColor = .clear
        
      //  imgPayment.transform = CGAffineTransform(scaleX: 1, y: 1)
        imgPayment.transform = CGAffineTransform.identity
        imgAttendance.transform = CGAffineTransform.identity
        imgPackages.transform = CGAffineTransform.identity
    }
    
    func changeSeparatorLocation(leading : CGFloat) {
        separatorLeadingConstr.constant = leading
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        }
    }
    
    func onTabClicked(closure : @escaping (Int)->()) {
        tabClosure = closure
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addGradientColor(color1: UIColor.appDarkThemeColor, color2: UIColor.appLightThemeColor)
    }
}
