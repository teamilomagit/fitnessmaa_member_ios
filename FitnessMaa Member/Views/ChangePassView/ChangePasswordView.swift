//
//  ChangePasswordView.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 09/06/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ChangePasswordView: UIView {

    @IBOutlet weak var txtFieldNewPass: AppTextField!
    @IBOutlet weak var txtFieldPass: AppTextField!
    @IBOutlet weak var txtFieldConfirmPass: AppTextField!
    @IBOutlet weak var btnSubmit: AppCustomButton!
    var iconClick = true
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
        setShowHidePassImgInPassword()
        setShowHidePassImgInNewPassword()
        setShowHidePassImgInConfirmPassword()

    }
    
    func setShowHidePassImgInPassword(){
        txtFieldPass.rightViewMode = .unlessEditing
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        
        button1.frame = CGRect(x: CGFloat(txtFieldPass.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        button1.contentMode = .scaleToFill
        button1.addTarget(self, action: #selector(self.ShowHide), for: .touchUpInside)
        txtFieldPass.rightView = button1
        txtFieldPass.rightViewMode = .always
        button1.tintColor = .black
    }
    
    func setShowHidePassImgInNewPassword(){
         txtFieldNewPass.rightViewMode = .unlessEditing
        let button2 = UIButton(type: .custom)
        button2.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button2.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        
        button2.frame = CGRect(x: CGFloat(txtFieldNewPass.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        
        button2.contentMode = .scaleToFill
        button2.addTarget(self, action: #selector(self.ShowHideNewPass), for: .touchUpInside)
       txtFieldNewPass.rightView = button2
        txtFieldNewPass.rightViewMode = .always
        button2.tintColor = .black
    }
    
     func setShowHidePassImgInConfirmPassword(){
         txtFieldConfirmPass.rightViewMode = .unlessEditing
        let button3 = UIButton(type: .custom)
        button3.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button3.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        
        button3.frame = CGRect(x: CGFloat(txtFieldConfirmPass.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        
        button3.contentMode = .scaleToFill
        button3.addTarget(self, action: #selector(self.ShowHideConfirmPass), for: .touchUpInside)
        txtFieldConfirmPass.rightView = button3
        txtFieldConfirmPass.rightViewMode = .always
        button3.tintColor = .black
    }
    
    @IBAction func ShowHide(_ sender: UIButton) {
        if(iconClick == true) {
            txtFieldPass.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black
        } else {
            txtFieldPass.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black

        }
        iconClick = !iconClick
    }
    
    
    @IBAction func ShowHideNewPass(_ sender: UIButton) {
        if(iconClick == true) {
            txtFieldNewPass.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black
        } else {
            txtFieldNewPass.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black
            
        }
        iconClick = !iconClick
    }
    
    @IBAction func ShowHideConfirmPass(_ sender: UIButton) {
        if(iconClick == true) {
            txtFieldConfirmPass.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black
        } else {
            txtFieldConfirmPass.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password")?.withRenderingMode(.alwaysTemplate), for: .normal)
            sender.tintColor = .black
            
        }
        iconClick = !iconClick
    }
    
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if isValidate(){
            changePasswordAPI()
        }
    }
    
    @IBAction func btnBgClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    
    
    func isValidate() -> Bool {
        
        if txtFieldPass.text!.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter password")
            return false
        }
        
        if txtFieldNewPass.text!.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter new password")
            return false
        }
        if txtFieldConfirmPass.text!.trim().count == 0 || txtFieldConfirmPass.text! != txtFieldNewPass.text!  {
            VIEWMANAGER.showToast("Please enter valid confirm password")
            return false
        }
        return true
    }
    
    func changePasswordAPI() {
        
        let param = [
            "user_id": VIEWMANAGER.currentUser?.id!,
            "current_password":txtFieldPass.text!,
            "new_password":txtFieldNewPass.text!
            
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: CHANGE_PASSWORS_API, params: param as [String : AnyObject], completion: { (msg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(msg as! String)
            app_del.setLoginAsRoot()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
}
