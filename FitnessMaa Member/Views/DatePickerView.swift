//
//  DatePickerView.swift
//  BTS
//
//  Created by Pawan Ramteke on 01/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit

class DatePickerView: UIView,UIPickerViewDelegate, UIPickerViewDataSource{
    
    var doneClosure : ((String,String)->())?
    var doneClosureTap : ((Date)->())?

    var baseView : UIView!
    
    var datePicker : UIDatePicker!
    
    var pickerView : UIPickerView!
    let months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
    let years = (2010...2100).map { Int($0) }

    var selMonth : String!
    var selYear : Int!
    
    var month = String() {
        didSet {
            self.selMonth = month
            pickerView.selectRow(months.index(of: selMonth)!, inComponent: 0, animated: false)
        }
    }
    
    var year = Int() {
        didSet {
            self.selYear = year
            pickerView.selectRow(years.index(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var onDateSelected: ((_ month: Int, _ year: Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.alpha = 0;
        
        baseView = UIView(frame: CGRect(x: 0, y: self.frame.size.height, width: self.frame.size.width, height: 300))
        baseView.backgroundColor = .white
        self.addSubview(baseView)
        
//        datePicker = UIDatePicker(frame:CGRect(x: 0, y: 20, width: baseView.frame.size.width, height: 200))
//        datePicker.backgroundColor = UIColor.white
//        datePicker.datePickerMode = .date
//        baseView.addSubview(datePicker)
        
        pickerView = UIPickerView(frame: CGRect(x: 0, y: 20, width: baseView.frame.size.width, height: 200))
        pickerView.dataSource = self
        pickerView.delegate = self
        baseView.addSubview(pickerView)

       // commonSetup()
        
        let btnCancel = UIButton(frame: CGRect(x: 10, y:pickerView.frame.maxY + 10, width:  baseView.frame.size.width/2 - 15, height: 40))
        btnCancel.backgroundColor = .red
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.white, for: .normal)
        btnCancel.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnCancel.addTarget(self, action: #selector(DatePickerView.btnCancelClicked(sender:)), for: .touchUpInside)
        baseView.addSubview(btnCancel)
       
        let btnDone = UIButton(frame: CGRect(x: btnCancel.frame.maxX + 10, y:btnCancel.frame.minY, width: btnCancel.frame.size.width, height: 40))
        btnDone.backgroundColor = UIColor.appDarkThemeColor
        btnDone.setTitle("Done", for: .normal)
        btnDone.setTitleColor(.white, for: .normal)
        btnDone.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btnDone.addTarget(self, action: #selector(DatePickerView.btnDoneClicked(sender:)), for: .touchUpInside)
        baseView.addSubview(btnDone)
        
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1;
            self.baseView.frame.origin.y = self.frame.size.height - self.baseView.frame.size.height
        }
    }
    
//    func commonSetup() {
//        // population years
//        var years: [Int] = []
//        if years.count == 0 {
//            var year = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.year, from: NSDate() as Date)
//            for _ in 1...15 {
//                years.append(year)
//                year += 1
//            }
//        }
//        self.years = years
//
//        // population months with localized names
//        var months: [String] = []
//        var month = 0
//        for _ in 1...12 {
//            months.append(DateFormatter().monthSymbols[month].capitalized)
//            month += 1
//        }
//        self.months = months
//
//        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
//        pickerView.selectRow(currentMonth - 1, inComponent: 0, animated: false)
//    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = months[pickerView.selectedRow(inComponent: 0)]
        let year = years[pickerView.selectedRow(inComponent: 1)]
//        if let block = onDateSelected {
//            block(month, year)
//        }
//
        self.selMonth = month
        self.selYear = year
    }
    
    
    @objc func btnDoneClicked(sender:UIButton)
    {
//        let selDate: Date? = datePicker.date
//        let dateFormat = DateFormatter()
//        dateFormat.dateFormat = "yyyy-MM-dd"
//        var dateString: String? = nil
//        if let aDate = selDate {
//            dateString = dateFormat.string(from: aDate)
//        }
        
        if doneClosure != nil {
            doneClosure!(selMonth,"\(selYear!)")
        }
        if doneClosureTap != nil {
            doneClosureTap!(datePicker.date)
        }
        
        
        hideView()
    }
    
    @objc func btnCancelClicked(sender:UIButton)
    {
       hideView()
    }
    
    
    
    func onDoneClicked(back : @escaping (String,String) -> Void)
    {
        doneClosure = back
    }
    func onDoneClickedTap(back : @escaping (Date) -> Void)
    {
        doneClosureTap = back
    }
    
    func hideView()
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0;
            self.baseView.frame.origin.y = self.frame.size.height
        }) { (completion) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension Date {
    
   static func getFormattedDate(strDate: String , currentFomat:String, expectedFromat: String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = currentFomat
        let date : Date = dateFormatterGet.date(from: strDate)!
        dateFormatterGet.dateFormat = expectedFromat
        return dateFormatterGet.string(from: date)
    
    //uses of this function
    //self.getFormattedDate(strDate: "20-March-2019", currentFomat: "dd-MMM-yyyy", expectedFromat: "yyyy-MM-dd")

    }
    static func convertDateFormater(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM yyyy"
        return  dateFormatter.string(from: date!)
        
    }
}
