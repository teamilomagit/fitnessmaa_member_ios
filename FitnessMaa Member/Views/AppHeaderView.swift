//
//  AppHeaderView.swift
//  FitnessMaa
//
//  Created by Pawan Ramteke on 06/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class AppHeaderView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 4
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.layer.cornerRadius = 10;
        self.backgroundColor = UIColor.white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // self.frame.size.height = SCREEN_HEIGHT * 0.35
        self.addGradientColor(color1: UIColor.appDarkThemeColor, color2: UIColor.appLightThemeColor)
    }

}
