//
//  VerifyDetailsView.swift
//  FitnessMaa
//
//  Created by iLoma on 08/05/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class VerifyDetailsView: UIView {
    var proceedClosure : (()->())?


    override func awakeFromNib() {
        super.awakeFromNib()
        self.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.alpha = 1
        }
    }
    
    @IBAction func btnEditClicekd(_ sender: Any?) {
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    @IBAction func btnProceedClicked(_ sender: Any) {
        if self.proceedClosure != nil {
            self.proceedClosure!()
        }
        btnEditClicekd(nil)
    }
    func onProceedButtonCliked(closure:@escaping ()->())
    {
        proceedClosure = closure
    }
}
