//
//  FeedsFilterView.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
class FeedsFilterView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    var _collectionView : UICollectionView!
    var collectionHtConstr : NSLayoutConstraint!
    var selIndexs = [Int]()
    var totalSelected = 0
    
    typealias StringBlock = (String?) -> Void
    var filterBlock : StringBlock?
    
    func onFilterChange(closure:@escaping StringBlock){
        self.filterBlock = closure

    }

    var feedOptions = NSMutableArray() //NSArray() {
//        didSet {
//            _collectionView.reloadData()
//        }
   // }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        _collectionView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        _collectionView.dataSource = self
        _collectionView.delegate = self
        _collectionView.backgroundColor = UIColor.clear
        _collectionView.showsHorizontalScrollIndicator = false
        self.addSubview(_collectionView)
        
        _collectionView.enableAutoLayout()
        _collectionView.leadingMargin(pixels: 0)
        _collectionView.trailingMargin(pixels: 0)
        _collectionView.topMargin(pixels: 0)
        _collectionView.bottomMargin(pixels: 0)
        
        collectionHtConstr = NSLayoutConstraint(item: _collectionView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 40)
        self.addConstraint(collectionHtConstr)
        
        _collectionView.register(FeedOptionsCell.self, forCellWithReuseIdentifier: "CellID")
        
      //  _collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        collectionHtConstr.constant = _collectionView.contentSize.height
//    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return feedOptions.count
    }
    
    
    func updateFilter(tagArray : NSArray){
    
        _collectionView.reloadData()
        
        if feedOptions.count == 0 {
            let optionAll = OptionsModel.init(fromDictionary: ["tag":"All","isSelected":true])
            optionAll.isSelected = true
            feedOptions.add(optionAll)
            
            feedOptions.addObjects(from: tagArray as! [Any])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as! FeedOptionsCell
        
        let model = feedOptions[indexPath.row] as! OptionsModel ;

        cell.lblTitle.text = model.tag //feedOptions[indexPath.row];
        cell.lblTitle.textColor = model.isSelected  ? UIColor.appDarkThemeColor : .white
        cell.backgroundColor = model.isSelected ? UIColor.white : .clear
        cell.layer.borderWidth = 0.7
        cell.layer.borderColor = selIndexs.contains(indexPath.row) ? UIColor.clear.cgColor : UIColor.white.cgColor
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let model = feedOptions[indexPath.row] as! OptionsModel ;
        
          model.isSelected = model.tag == "All" ? true : !model.isSelected
        
        setResetSelection(model: model)
        _collectionView.reloadData()

    }
    
    func setResetSelection(model: OptionsModel){
        
        if model.tag == "All" {
            
            for case let tempModel as OptionsModel in feedOptions {
                // do something with button
                if tempModel.tag != "All"{
                    tempModel.isSelected = false
                }
            }
            totalSelected = 0

        }
        else{
            
             model.isSelected ? (totalSelected += 1) : (totalSelected -= 1)

            
            if totalSelected > 0{
                let tempModel = feedOptions[0] as! OptionsModel ;
                tempModel.isSelected = false

            }
            else{
                let tempModel = feedOptions[0] as! OptionsModel ;
                tempModel.isSelected = true
            }
            
        }
        
        if filterBlock != nil{
            
            var strTag = ""
            for case let tempModel as OptionsModel in feedOptions{
                
                if tempModel.isSelected {
                    strTag.append(tempModel.tag + ",")
                }
            }
            filterBlock!(strTag)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 10, 0, 10)
    }
    
    deinit {
        _collectionView.removeObserver(self, forKeyPath: "contentSize")
    }
    
}


class FeedOptionsCell: UICollectionViewCell {
    
    var lblTitle : UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 10
        lblTitle = UILabel()
        lblTitle.font = UIFont.appMediumFont(size: 14)
        lblTitle.textAlignment = .center
        self.addSubview(lblTitle)
        lblTitle.enableAutoLayout()
        lblTitle.leadingMargin(pixels: 0)
        lblTitle.trailingMargin(pixels: 0)
        lblTitle.topMargin(pixels: 0)
        lblTitle.bottomMargin(pixels: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
