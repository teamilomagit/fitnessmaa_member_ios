//
//  BMICalculatorView.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 11/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class BMICalculatorView: UIView,UITextFieldDelegate {

    @IBOutlet weak var baseView: MaterialView!
    
    @IBOutlet weak var txtFieldKGs: UITextField!
    @IBOutlet weak var txtFieldPounds: UITextField!
    @IBOutlet weak var txtFieldFeet: UITextField!
    @IBOutlet weak var txtFieldCm: UITextField!
    @IBOutlet weak var viewBMIHtConstr: NSLayoutConstraint!
    
    @IBOutlet weak var lblBmiStatus: UILabel!
    @IBOutlet weak var lblBMI: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtFieldKGs.delegate = self
        txtFieldPounds.delegate = self
        txtFieldFeet.delegate = self
        txtFieldCm.delegate = self
        
        viewBMIHtConstr.constant = 0
        self.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
            if textField == txtFieldKGs {
                if updatedText.trim().count > 0 {
                    let pound = Float(updatedText)! * 2.20462
                    txtFieldPounds.text = String(format: "%.2f", pound)
                }
                else {
                    txtFieldPounds.text = ""
                }
                
                calculateBMI(weight: updatedText, height: txtFieldCm.text!)
            }
            else if textField == txtFieldPounds {
                if updatedText.trim().count > 0 {
                    let kg = Float(updatedText)! / 2.20462
                    txtFieldKGs.text = String(format: "%.2f", kg)
                }
                else {
                    txtFieldKGs.text = ""
                }
                calculateBMI(weight: txtFieldKGs.text!, height: txtFieldCm.text!)
            }
            
            else if textField == txtFieldFeet {
                if updatedText.trim().count > 0 {
                    let cm = Float(updatedText)! * 30.4800
                    txtFieldCm.text = String(format: "%.2f", cm)
                }
                else {
                    txtFieldCm.text = ""
                }
                
                calculateBMI(weight: txtFieldKGs.text!, height: txtFieldCm.text!)
            }
                
            else if textField == txtFieldCm {
                if updatedText.trim().count > 0 {
                    let feet = Float(updatedText)! / 30.4800
                    txtFieldFeet.text = String(format: "%.2f", feet)
                }
                else {
                    txtFieldFeet.text = ""
                }
                calculateBMI(weight: txtFieldKGs.text!, height: updatedText)
            }
        }
        
        return true
    }
    
    func calculateBMI(weight:String,height:String) {
        if weight.count > 0 && height.count > 0 {
            let heightValue = Float(height)! / 100.0
            let weightValue = Float(weight)!
            
            let BMI = weightValue / (heightValue * heightValue)
            
            lblBMI.text = "BMI - \(String(format: "%.2f", BMI))"
            
            viewBMIHtConstr.constant = 193
            
            checkBMIStatus(bmi: BMI)
        }
    }
    
    func checkBMIStatus(bmi : Float) {
        
        if (bmi <= 15.0) {
            lblBmiStatus.text = "Very severely under weight";
        } else if bmi > 15.0 && bmi <= 16.0 {
            lblBmiStatus.text = "Severely under weight";
        } else if bmi > 16.0 && bmi <= 18.5 {
            lblBmiStatus.text = "Under weight";
        } else if bmi > 18.5 && bmi <= 25.0 {
            lblBmiStatus.text = "Normal";
        } else if bmi > 25.0 && bmi <= 30.0 {
            lblBmiStatus.text = "Over Weight";
        } else if bmi > 30.0 && bmi <= 35 {
            lblBmiStatus.text = "Obesity class I";
        } else if bmi > 35 && bmi <= 40 {
            lblBmiStatus.text = "Obesity class II";
        } else {
            lblBmiStatus.text = "Obesity class III";
        }
    }
    

    @IBAction func btnBGClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
}
