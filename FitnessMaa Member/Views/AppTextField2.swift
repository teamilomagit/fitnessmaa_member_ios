//
//  AppTextField2.swift
//  FitnessMaa
//
//  Created by iLoma on 14/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//
import UIKit
import Foundation
private var kAssociationKeyMaxLength: Int = 0

enum TextFieldTag2 : Int {
    case TAG_MOBILE = 100
    case TAG_DATE_PICKER = 101
    case TAG_ACTION_SHEET = 102
    case TAG_DROP_DOWN   = 103
}
class AppTextField2: UITextField,UITextFieldDelegate {
    var actionSheetData : NSArray!
    var dropDownData : NSArray!
    var doneClosure : ((String)->())?
    var dropdownFiledClickedCloser: (()->())?

    @IBInspectable var rightImage : String = ""  {
        didSet{
            self.rightViewMode = .always
            
            let btn = UIButton(frame: CGRect(x: 0, y:0, width: 40, height: 50))
            btn.setImage(UIImage(named: rightImage)?.withRenderingMode(.alwaysTemplate), for: .normal)
            btn.tintColor = UIColor.darkGray
            btn.contentMode = .center
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.leftView = btn
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.delegate = self
        self.borderStyle = .none
        if rightImage == "" {
            self.leftViewMode = .always
            self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 50))
        }
    }
    
    func setTextFieldTag(tag:TextFieldTag)
    {
        self.tag = tag.rawValue
        if tag == .TAG_MOBILE {
            self.keyboardType = .phonePad
        }
        if tag == .TAG_ACTION_SHEET || tag == .TAG_DROP_DOWN {
            self.rightViewMode = .always
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            imgView.image = UIImage(named: "ic_drop_down")
            imgView.contentMode = .scaleAspectFit
            self.rightView = imgView
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,let textRange = Range(range, in: text)
        {
            let updatedText = text.replacingCharacters(in: textRange,with: string)
            
            if self.tag == TextFieldTag.TAG_MOBILE.rawValue
            {
                return updatedText.count <= 10
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == TextFieldTag.TAG_DATE_PICKER.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
            showDatePicker()
            return false
        }
        if textField.tag == TextFieldTag.TAG_ACTION_SHEET.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
            showActionSheet()
            return false
        }
        
        if textField.tag == TextFieldTag.TAG_DROP_DOWN.rawValue {
            DispatchQueue.main.async {
                self.superview?.endEditing(true)
            }
            showDropDown()
            return false
        }
        return true
    }
    
    
    func showDatePicker()
    {
        
        let datePicker = DatePickerView(frame: UIScreen.main.bounds)
        VIEWMANAGER.topMostController().view.addSubview(datePicker)

        datePicker.onDoneClickedTap { (date) in
            self.text = self.formattedDate(selDate: date)

        }
    }
    
    func formattedDate(selDate:Date!) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy/MM/dd"
        var dateToPass: String? = nil
        
        dateToPass = dateFormat.string(from: selDate)
        
        return dateToPass!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height/2
    }
    
    func showActionSheet()
    {
    
        if actionSheetData == nil {
            return
        }
        
        VIEWMANAGER.showActionSheet(actionSheetData) { (title) in
            self.text = title
        }
    }
    
    func showDropDown()
    {
        
        
        if dropdownFiledClickedCloser != nil{
            dropdownFiledClickedCloser!()
        }
        
        if dropDownData == nil {
            return
        }
        
        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: self.placeholder!,data:dropDownData as! [String])
        UIApplication.shared.keyWindow?.addSubview(dropDown)
        dropDown.onDoneClicked { (data) in
            self.text = data as! String
            if self.doneClosure != nil {
                self.doneClosure!(data as! String)
            }
        }
    }
    
    func onDropDownDataSelection(back : @escaping (String) -> Void)
    {
        doneClosure = back
    }
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
    
    
    func onDropDownFieldClicked(closer: @escaping ()->()){
        
        self.dropdownFiledClickedCloser = closer
    }
    
   
}
