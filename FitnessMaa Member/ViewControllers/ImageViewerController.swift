//
//  ImageViewerController.swift
//  BTS
//
//  Created by Pawan Ramteke on 27/12/18.
//  Copyright © 2018 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
class ImageViewerController: UIViewController,UIScrollViewDelegate {

    let ZOOM_STEP : CGFloat = 2
    var imageScrollView : UIScrollView!
    var imgView : UIImageView!
    var imgURL : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black
        
        imageScrollView = UIScrollView(frame: view.bounds)
        imageScrollView.bouncesZoom = true
        imageScrollView.delegate = self
        imageScrollView.clipsToBounds = true
        
        view.addSubview(imageScrollView)
        
        imgView = UIImageView(frame: imageScrollView.bounds)
        imgView.isUserInteractionEnabled = true
        imageScrollView.addSubview(imgView)
        imgView.contentMode = .scaleAspectFit
        
        let loader = UIActivityIndicatorView()
        loader.activityIndicatorViewStyle = .whiteLarge
        view.addSubview(loader)
        loader.enableAutoLayout()
        loader.centerX()
        loader.centerY()
        loader.fixedWidth(pixels: 20)
        loader.fixedHeight(pixels: 20)
        
        loader.startAnimating()
        
        SDImageCache.shared.removeImage(forKey: imgURL, withCompletion: nil)
        
        weak var weakImgView: UIImageView? = imgView
        imgView.sd_setImage(with: URL(string: imgURL), completed: { image, error, cacheType, imageURL in
            if image == nil {
                self.imgView.image = UIImage(named: "img_broke")
                self.imgView.contentMode = .center
            } else {
                weakImgView?.image = image
            }
            loader.stopAnimating()
        })
        
        imageScrollView.contentSize = CGSize(width: imgView.bounds.size.width, height: imgView.bounds.size.height)
        imageScrollView.decelerationRate = UIScrollViewDecelerationRateFast
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(ImageViewerController.handleDoubleTap(_:)))
        let twoFingerTap = UITapGestureRecognizer(target: self, action: #selector(ImageViewerController.handleTwoFingerTap(_:)))
        
        doubleTap.numberOfTapsRequired = 2
        twoFingerTap.numberOfTouchesRequired = 2
        
        imgView.addGestureRecognizer(doubleTap)
        imgView.addGestureRecognizer(twoFingerTap)
        
        let minimumScale: Float = 1.0
        imageScrollView.maximumZoomScale = 4.0
        imageScrollView.minimumZoomScale = CGFloat(minimumScale)
        imageScrollView.zoomScale = CGFloat(minimumScale)
        imageScrollView.contentSize = CGSize(width: imgView.frame.size.width, height: imgView.frame.size.height)
        
        
        let btnClose = UIButton(frame: CGRect(x: 10, y: 20, width: 50, height: 50))
        btnClose.setImage(UIImage(named: "ic_close"), for: .normal)
        btnClose.addTarget(self, action: #selector(ImageViewerController.btnCloseClicked), for: .touchUpInside)
        view.addSubview(btnClose)

        
    }
    
    func scrollViewDidZoom(_ aScrollView: UIScrollView) {
        let offsetX: CGFloat = (imageScrollView.bounds.size.width > imageScrollView.contentSize.width) ? (imageScrollView.bounds.size.width - imageScrollView.contentSize.width) * 0.5 : 0.0
        let offsetY: CGFloat = (imageScrollView.bounds.size.height > imageScrollView.contentSize.height) ? (imageScrollView.bounds.size.height - imageScrollView.contentSize.height) * 0.5 : 0.0
        imgView.center = CGPoint(x: imageScrollView.contentSize.width * 0.5 + offsetX, y: imageScrollView.contentSize.height * 0.5 + offsetY)
    }
    
//    override func viewDidUnload() {
//        imageScrollView = nil
//        imgView = nil
//    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgView
    }

    
    @objc func handleTwoFingerTap(_ gestureRecognizer:UIGestureRecognizer)
    {
        let newScale = Float(imageScrollView.zoomScale / ZOOM_STEP)
        let zoomR: CGRect = zoomRect(forScale: CGFloat(newScale), withCenter: gestureRecognizer.location(in: gestureRecognizer.view))
        imageScrollView.zoom(to: zoomR, animated: true)

    }
    
    @objc func handleDoubleTap(_ gestureRecognizer:UIGestureRecognizer)
    {
        var newScale = Float(imageScrollView.zoomScale * ZOOM_STEP)
        
        if CGFloat(newScale) > imageScrollView.maximumZoomScale {
            newScale = Float(imageScrollView.minimumZoomScale)
            var zoomR: CGRect = zoomRect(forScale: CGFloat(newScale), withCenter: gestureRecognizer.location(in: gestureRecognizer.view))
            
            imageScrollView.zoom(to: zoomR, animated: true)
        } else {
            
            newScale = Float(imageScrollView.maximumZoomScale)
            let zoomR: CGRect = zoomRect(forScale: CGFloat(newScale), withCenter: gestureRecognizer.location(in: gestureRecognizer.view))
            
            imageScrollView.zoom(to: zoomR, animated: true)
        }
    }
    
    func zoomRect(forScale scale: CGFloat, withCenter center: CGPoint) -> CGRect {
        
        var zoomR = CGRect()
        
        // the zoom rect is in the content view's coordinates.
        //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
        //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
        zoomR.size.height = CGFloat(imageScrollView.frame.size.height / scale)
        zoomR.size.width = CGFloat(imageScrollView.frame.size.width / scale)
        
        // choose an origin so as to get the right center.
        zoomR.origin.x = center.x - (zoomR.size.width / 2.0)
        zoomR.origin.y = center.y - (zoomR.size.height / 2.0)
        
        return zoomR
    }

    
    @objc func btnCloseClicked()
    {
        self.dismiss(animated: true, completion: nil)
        
    }

    
}
