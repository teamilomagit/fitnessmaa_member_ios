//
//  PaymentViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class PaymentViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    var pageNo = 1
    var loadStarted : Bool = false

    @IBOutlet weak var lblTotalPayment: UILabel!
    @IBOutlet weak var tblViewList: UITableView!
    var refresh : UIRefreshControl!
    var arrList = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewList.register(UINib(nibName: "PaymentHistoryCell", bundle: nil), forCellReuseIdentifier: "PaymentHistoryCell")
        
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewList.addSubview(refresh)
        
        getMemberPaymentHistory(pageNo: pageNo)
    }
    
    @objc func pullToRefresh() {
        pageNo = 1
        self.getMemberPaymentHistory(pageNo: pageNo,showLoader: false,isRefresh: true)
    }
    
    func getMemberPaymentHistory(pageNo:Int,showLoader : Bool = true,isRefresh:Bool = false) {
        let param = [
            "page" : pageNo,
            "limit" : LIMIT,
            "gym_id" : VIEWMANAGER.currentUser!.gymId,
            "member_id":VIEWMANAGER.currentUser!.id,
            ] as [String : Any]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: MEMBER_PAYMENT_LIST_URL, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()

            self.refresh.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = responseData as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            self.arrList.addObjects(from: arr as! [Any])
            
            var total : Float = 0.0
            for case let model as PaymentListModel in self.arrList {
                total = total + Float(model.paidAmount!)!
            }
            self.lblTotalPayment.text = "Total Payment: \(total)"
            self.tblViewList.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        let model = arrList[indexPath.row] as! PaymentListModel
        cell.lblPaymentDate.text = "Paid On \(VIEWMANAGER.formattedDate(strDate: model.paymentDate!)) at \(VIEWMANAGER.formattedTime(strDate: model.paymentDate!))"
        cell.lblPaidAmount.text = model.paidAmount
        cell.lblCurrencySymbol.text = CURRENCY_SYMBOL
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblViewList.contentOffset.y >= (tblViewList.contentSize.height - tblViewList.frame.size.height) {
            if loadStarted {
                self.getMemberPaymentHistory(pageNo: pageNo,showLoader: false)
                loadStarted = false
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
