//
//  DrawerViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
class DrawerViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!

    var arrTitles : [String]!
    var arrImages : [String]!

    let screenWidth = SCREEN_WIDTH * 0.8

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        arrTitles = ["Profile","Calculate BMI","Exercise Plan","Feedback","Logout"]
        arrImages = ["ic_drawer_profile","ic_menu_excercise_plan","ic_menu_excercise_plan","ic_drawer_terms","ic_drawer_logout"]

        tblView.estimatedRowHeight = 60
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.bounces = false
        tblView.register(UINib(nibName: "DrawerCell", bundle: nil), forCellReuseIdentifier: "DrawerCell")
        
        tblView.tableHeaderView = tableHeaderView()

    }
    func tableHeaderView() -> UIView
    {
        let viewHeader = UIView(frame: CGRect(x: 0, y: -20, width: screenWidth, height: 230))
        viewHeader.layer.cornerRadius = 0
        let imgViewLogo = UIImageView(frame: CGRect(x: viewHeader.frame.midX - 100, y: 20, width: 200, height: 40))
        imgViewLogo.contentMode = .scaleAspectFit
        imgViewLogo.image = UIImage(named: "ic_header_logo")
        viewHeader.addSubview(imgViewLogo)
        
        let imgViewDP = UIImageView(frame: CGRect(x: viewHeader.frame.midX - 50, y: imgViewLogo.frame.maxY + 5, width: 80, height: 80))
        imgViewDP.layer.cornerRadius = 40
        imgViewDP.layer.borderWidth = 2.0;
        imgViewDP.layer.borderColor = UIColor.white.cgColor
        imgViewDP.layer.masksToBounds = true
      //  imgViewDP.sd_setImage(with: URL(string: "\(MEDIA_BASE_URL)\(VIEWMANAGER.currentUser!.gym.imageUrl!.replacingOccurrences(of: " ", with: "%20"))"), placeholderImage: UIImage(named: "ic_placeholder"))
        viewHeader.addSubview(imgViewDP)
        
        let lblGymName = UILabel(frame: CGRect(x: 10, y: imgViewDP.frame.maxY , width: viewHeader.frame.size.width - 20, height: 30))
        lblGymName.font = UIFont.appMediumFont(size: 18)
     //   lblGymName.text = VIEWMANAGER.currentUser?.gym.gymName
        lblGymName.textColor = .white
        lblGymName.textAlignment = .center
        viewHeader.addSubview(lblGymName)
        
        let lblGymId = UILabel(frame: CGRect(x: 10, y: lblGymName.frame.maxY , width: viewHeader.frame.size.width - 20, height: 20))
        lblGymId.font = UIFont.appRegularFont(size: 17)
        lblGymId.text = "(Gym ID: \(VIEWMANAGER.currentUser!.gymId!))"
        lblGymId.textColor = .white
        lblGymId.textAlignment = .center
        viewHeader.addSubview(lblGymId)
        
      //  viewHeader.frame.size.height = lblGymId.frame.maxY + 10
        
        viewHeader.addGradientColor(color1: UIColor.appDarkThemeColor, color2: UIColor.appLightThemeColor)
        
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawerCell") as! DrawerCell
        cell.lblTitle.text = arrTitles[indexPath.row]
        cell.imgView.image = UIImage(named: arrImages[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let controller = self.parent as! KYDrawerController
        controller.setDrawerState(.closed, animated: true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: indexPath.row)
    }

}
