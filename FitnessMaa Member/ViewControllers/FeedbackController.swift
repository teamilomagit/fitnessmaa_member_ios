//
//  FeedbackController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 20/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class FeedbackController: BaseViewController {
    @IBOutlet weak var txtFieldName: AppTextField!
    @IBOutlet weak var txtViewFeedback: KMPlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationWithTitle(title: "Feedback")
        txtViewFeedback.layer.cornerRadius = 10
        txtViewFeedback.layer.borderColor = UIColor.gray.cgColor
        txtViewFeedback.layer.borderWidth = 1
        txtFieldName.text = VIEWMANAGER.currentUser?.fullName!
    }
    


    @IBAction func btnSubmitClicked(_ sender: Any) {
        if validate(){
            feedbackService()
        }
      
    }
    func validate()->Bool
    {
        if  txtFieldName.text?.trim().count == 0{
            self.view.makeToast("Please enter full name")
            return false
        }
        if txtViewFeedback.text?.count == 0 {
            self.view.makeToast("Please type your suggestions or complaint")
            return false
        }
        
        return true
    }
    
    func feedbackService() {
        
        let param = [
            "user_id" : "4",
            "fullname" : txtFieldName.text!,
            "app_type":"MEMBER",
            "comments":txtViewFeedback.text!
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: SEND_FEEDBACK_API, params: param as [String : AnyObject], completion: { (response) in
            CustomAlertView.showAlert(withTitle: "SUCCESS", messsage: "Thank you for your feedback. Our team will review and work on it.", completion: {
                self.navigationController?.popViewController(animated: true)
                
            })
            VIEWMANAGER.hideActivityIndicator()
                    }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
}
