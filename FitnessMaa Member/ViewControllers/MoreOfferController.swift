//
//  MoreOfferController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 27/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class MoreOfferController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tblViewMoreOffer: UITableView!
    var arrMoreOffer = NSMutableArray()
    var refresh = UIRefreshControl()
    var pageNo = 1
    var loadStarted : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewMoreOffer.register(UINib(nibName: "TodaysOffersCell", bundle: nil), forCellReuseIdentifier: "TodaysOffersCell")
        //tblViewMoreOffer.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        addNavigationWithTitle(title: "More Offers")
        refresh.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewMoreOffer.addSubview(refresh)
        
        getOffersService()
        
    }
    @objc func pullToRefresh() {
        //pageNo = 1
        self.getOffersService()
    }
    func getOffersService() {
        
        let param = [
            "city_id":"2"
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_OFFERS_API, params: param as [String : AnyObject], completion: { (response) in
            self.arrMoreOffer.removeAllObjects()
            let model = response as! OffersModel
          
            for str in model.cityOffers{
                self.arrMoreOffer.add(str)
            }

            self.tblViewMoreOffer.reloadData()
            
            self.refresh.endRefreshing()
//            if isRefresh {
//                self.arrMoreOffer.removeAllObjects()
//            }
//            let arr = model.cityOffers! as NSArray
//            if arr.count == LIMIT {
//                self.pageNo = self.pageNo + 1
//                self.loadStarted = true
//            }
//            else{
//                self.loadStarted = false
//            }
//            self.arrMoreOffer.addObjects(from: arr as! [Any])
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            self.refresh.endRefreshing()
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMoreOffer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodaysOffersCell") as! TodaysOffersCell
        cell.selectionStyle = .none
        let model = arrMoreOffer[indexPath.row] as! CityOffer
        cell.imgViewTodaysOffer.sd_setImage(with: URL(string:MEDIA_URL + model.image), placeholderImage: UIImage(named:"placeholder_img"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
//    deinit {
//        tblViewMoreOffer.removeObserver(self, forKeyPath: "contentSize")
//    }
}
