//
//  ExercisePlanController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 18/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ExercisePlanController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tbleViewExercisePlan: UITableView!
    var arrList = NSMutableArray()
    
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool = false
    var strMemberId : String!
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationWithTitle(title: "Exercise Plan")
        setupTableView()
        getExerciseListService()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.tintColor = UIColor.black
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        tbleViewExercisePlan.addSubview(refreshControl)
    }
    @objc func refreshList() {
        getExerciseListService()
        
    }
    
    func setupTableView()
    {
        tbleViewExercisePlan.estimatedRowHeight = 70
        tbleViewExercisePlan.rowHeight = UITableViewAutomaticDimension
        
        let nib = UINib(nibName: "ExerciseListCell", bundle: nil)
        tbleViewExercisePlan.register(nib, forCellReuseIdentifier: "ExerciseListCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView = arrList.count > 0 ? nil : VIEWMANAGER.tableBGView()
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseListCell") as! ExerciseListCell
        let model = arrList[indexPath.row] as! ExerciseListModel
        cell.indexPath = indexPath
        cell.lblName.text = model.planName
        cell.lblWeeks.text = "\(model.noOfWeeks!)\(" Weeks")"
        cell.lblDate.text = (Date.convertDateFormater(date: model.createdAt!)) + " - " + (Date.convertDateFormater(date: model.updatedAt!))
        tableView.backgroundView = arrList.count > 0 ? nil : VIEWMANAGER.tableBGView()
    
        return cell
        
    }
    
    func getExerciseListService()
    {
        let param = [
            "member_id" : VIEWMANAGER.currentUser?.id ?? ""
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_EXERCISE_LIST, params: param as [String : AnyObject], completion: { (responseData) in
            self.arrList = responseData as! NSMutableArray
            //self.setupData()
            self.tbleViewExercisePlan.reloadData()
            VIEWMANAGER.hideActivityIndicator()
            self.refreshControl.endRefreshing()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
            self.refreshControl.endRefreshing()
            
        }
    }
    
}
