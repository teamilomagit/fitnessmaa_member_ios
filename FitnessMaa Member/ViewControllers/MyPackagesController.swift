//
//  MyPackagesController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class MyPackagesController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    var pageNo = 1
    var loadStarted : Bool = false
    
    var refresh : UIRefreshControl!
    var arrList = NSMutableArray()

    @IBOutlet weak var tblViewList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        tblViewList.register(UINib(nibName: "PackageHistoryCell", bundle: nil), forCellReuseIdentifier: "PackageHistoryCell")
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewList.addSubview(refresh)

        getMemberPackageHistory(pageNo: pageNo)
        
    }
    
    @objc func pullToRefresh() {
        pageNo = 1
        self.getMemberPackageHistory(pageNo: pageNo,showLoader: false, isRefresh: true)
    }
    
    func getMemberPackageHistory(pageNo:Int,showLoader : Bool = true,isRefresh:Bool = false) {
        let param = [
            "page" : pageNo,
            "limit" : LIMIT,
            "gym_id" : VIEWMANAGER.currentUser!.gymId,
            "member_id":VIEWMANAGER.currentUser!.id,
            ] as [String : Any]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: MEMBER_PACKAGE_LIST_URL, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.refresh.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = responseData as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            self.arrList.addObjects(from: arr as! [Any])
            self.tblViewList.backgroundView =  self.arrList.count == 0 ? self.tblBGView() : nil
            self.tblViewList.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    override func tblBGView() -> UILabel {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        lblBG.font = UIFont.appRegularFont(size: 18)
        lblBG.text = "No data found"
        lblBG.textAlignment = .center
        return lblBG
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PackageHistoryCell") as! PackageHistoryCell
        let model = arrList[indexPath.row] as! PackageListModel
        
        cell.lblPackageName.text = model.packageName
        cell.lblDaysLeft.text = "\(model.noOfDays!) DAYS LEFT"
        cell.lblDuration.text = "\(model.duration!) Months"
        cell.lblStartDate.text = formattedDate(strDate: model.startDate)
        cell.lblEndDate.text = formattedDate(strDate: model.endDate)
        cell.lblPackageAmt.text = "\(CURRENCY_SYMBOL)\(model.amount!)"
        cell.lblTotal.text = "\(CURRENCY_SYMBOL)\(model.finalAmount!)"
        
        return cell
    }
    
    func formattedDate(strDate:String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: strDate)
        
        if date != nil {
            dateFormat.dateFormat = "dd MMM yyyy"
            return dateFormat.string(from: date!)
        }
        return ""
    }
}
