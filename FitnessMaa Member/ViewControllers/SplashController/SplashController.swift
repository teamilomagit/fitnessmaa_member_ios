//
//  SplashController.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 11/06/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class SplashController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        generalSettingApi()
    }
    

    
    func generalSettingApi() {
        let params =  [String : AnyObject]()
        RemoteAPI().callPOSTApi(apiName: GENERAL_SETTING_API, params: params as [String : AnyObject], completion: { (response) in
            let generalSettingModel = response as! GeneralSettingModel
            Preferences.saveGeneralSettingModelData(generalSettingModel: (generalSettingModel.toDictionary()as? NSDictionary)!)
            
            if Preferences.getLoginResponse() != nil {
                let dict =  Preferences.getLoginResponse()
                VIEWMANAGER.currentUser = UserModel(fromDictionary: dict as! [String : Any])                        }
            app_del.setDashboarAsRoot()
        }) { (errMsg) in
            
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.generalSettingApi()
                })
                
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
           
            
        }
    }

}
