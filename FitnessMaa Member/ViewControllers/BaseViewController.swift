//
//  BaseViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController,UIGestureRecognizerDelegate {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.controllerBGColor
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true;
    }
    
    @IBAction func drawerClicked(_ sender: Any) {
        var parentVC = self.parent
        while parentVC != nil {
            if let drawerVC = parentVC as? KYDrawerController
            {
                drawerVC.setDrawerState(.opened, animated: true)
            }
            parentVC = parentVC?.parent
        }
    }
    
    
    func btnNotificationShow(){
        let btnNotification = UIButton(frame: CGRect(x: view.bounds.maxX - 50, y: 20, width: 50, height: 50))
        btnNotification.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        btnNotification.setImage(UIImage(named: "ic_notification"), for: .normal)
        btnNotification.setTitleColor(UIColor.black, for: .normal)
        btnNotification.addTarget(self, action: #selector(self.btnNotificationClicked), for: .touchUpInside)
        self.view.addSubview(btnNotification)
    }
    
   
    
    @IBAction func btnNotificationClicked(_ sender: UIButton) {
        let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        self.navigationController?.pushViewController(notificaitonVC, animated: true)
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
  
    func addNavigationWithTitle(title : String){
        
        let imgBack = UIImage(named: "ic_back") as UIImage?
        
        
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 35, width: UIScreen.main.bounds.width, height: 35))
        titleLabel.text = "\(title)"
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.appBoldFont(size: 18)
        self.view.addSubview(titleLabel)
        
        
        let backBtn = UIButton(frame: CGRect(x: 15, y: 0, width: 30, height: 30))
        backBtn.setImage(imgBack, for: .normal)
        backBtn.layoutMargins.left = 15
        backBtn.layoutMargins.top = 15
        self.view.addSubview(backBtn)
        backBtn.center.y = titleLabel.center.y
        backBtn.addTarget(self, action: #selector(btnBackeClicked), for: .touchUpInside)
        
    }
    
    func addCitySelectionWithNotification(){
        
        let imgBack = UIImage(named: "ic_logo_icon") as UIImage?
        let img_Arrow = UIImage(named: "ic_drop_down-1") as UIImage?
        let img_location = UIImage(named: "ic_location-1") as UIImage?
   
        let cityName = UILabel(frame: CGRect(x: 50, y: 55, width: UIScreen.main.bounds.width, height: 35))
        cityName.text = "Jaitala, Nagpur"
        cityName.textColor = UIColor.white
        cityName.textAlignment = .left
        cityName.font = UIFont.appBoldFont(size: 14)
        self.view.addSubview(cityName)

        
        let arrow = UIButton(frame: CGRect(x: 150, y: 55, width: 30, height: 30))
        arrow.setImage(img_Arrow, for: .normal)
        arrow.layoutMargins.left = 15
        arrow.layoutMargins.top = 15
        self.view.addSubview(arrow)
        arrow.center.y = cityName.center.y
        
        let bgView = UIView(frame: CGRect(x: 15, y: 10, width: 0, height: 0))
        bgView.backgroundColor = UIColor.blue
        self.view.addSubview(bgView)
        
        let titleLabel = UILabel(frame: CGRect(x: 50, y: 35, width: UIScreen.main.bounds.width, height: 35))
        titleLabel.text = "Your Location"
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .left
        titleLabel.font = UIFont.appBoldFont(size: 14)
        self.view.addSubview(titleLabel)
       
        
        let logoBtn = UIButton(frame: CGRect(x: 15, y: 0, width: 30, height: 30))
        logoBtn.setImage(imgBack, for: .normal)
        logoBtn.layoutMargins.left = 15
        logoBtn.layoutMargins.top = 15
        self.view.addSubview(logoBtn)
        logoBtn.center.y = titleLabel.center.y
        
        let location = UIButton(frame: CGRect(x:30, y: 35, width: 30, height: 30))
        location.setImage(img_location, for: .normal)
        location.layoutMargins.left = 15
        location.layoutMargins.top = 15
        self.view.addSubview(location)
        
        
        let btnNotification = UIButton(frame: CGRect(x: view.bounds.maxX - 50, y: 20, width: 50, height: 50))
        btnNotification.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        btnNotification.setImage(UIImage(named: "ic_notification"), for: .normal)
        btnNotification.setTitleColor(UIColor.black, for: .normal)
        btnNotification.addTarget(self, action: #selector(self.btnNotificationClicked), for: .touchUpInside)
        self.view.addSubview(btnNotification)
       
        
    }
    
    func tblBGView() -> UILabel {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        lblBG.font = UIFont.appRegularFont(size: 18)
        lblBG.text = "No data found"
        lblBG.textAlignment = .center
        return lblBG
    }
    
    @objc func btnBackeClicked()
    {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

}
