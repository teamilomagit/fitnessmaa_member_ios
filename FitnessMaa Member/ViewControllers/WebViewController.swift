//
//  WebViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class WebViewController: BaseViewController,UIWebViewDelegate {

    var url : String!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        webView.loadRequest(URLRequest(url: URL(string: url)!))
    }

    func webViewDidStartLoad(_ webView: UIWebView) {
        loader.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loader.stopAnimating()
    }
    
    @IBAction func btnBGClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
