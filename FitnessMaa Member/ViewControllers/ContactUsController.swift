//
//  ContactUsController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 30/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ContactUsController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var viewBG: AppHeaderView!
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblviewContactAngSupport: UITableView!
    var headerHeight : CGFloat = 80.0
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool = false
    var arrList = NSMutableArray()
    let screenWidth = SCREEN_WIDTH
    
    let arrTitle = ["Email Id","Mobile Number"]
    let arrContent = ["support@fitnessmaa.com","+918208545489"]
    let arrImage = ["ic_email","ic_mobile"]
    
    var model  :  GeneralSettingModel!

    @IBOutlet weak var viewBGHtConstr: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.font = UIFont.appMediumFont(size: 16)
        lblTitle.textColor = .white
        tblviewContactAngSupport.delegate = self
        tblviewContactAngSupport.dataSource = self
        self.view.backgroundColor = UIColor.controllerBGColor
        setupTableView()
        addNavigationWithTitle(title: "Contact & Support")
        
        if Preferences.getGeneralSettingModelData() != nil{
            model = Preferences.getGeneralSettingModelData()!
        }
    }
    
    func setupTableView()
    {
        tblviewContactAngSupport.estimatedRowHeight = 70
        tblviewContactAngSupport.rowHeight = UITableViewAutomaticDimension
        tblviewContactAngSupport.register(UINib(nibName: "ContactSupportCell", bundle: nil), forCellReuseIdentifier: "ContactSupportCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactSupportCell") as! ContactSupportCell
        cell.indexPath = indexPath
        cell.lblTitle.text = arrTitle[indexPath.row]
      //  cell.btnContent.setTitle(arrContent[indexPath.row], for: .normal)
        cell.imgViewDP.image = UIImage(named: arrImage[indexPath.row])
        cell.btnContent.setTitle(indexPath.row == 0 ? model.supportEmail : model.supportMobile, for: .normal)
        cell.onDataSelection { (indexPath) in
            
            if indexPath.row == 0 {
                let email = self.model.supportEmail
                if let url = URL(string: "mailto:\(email)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
            else if indexPath.row == 1 {
                let url = URL(string: "telprompt://\(self.model.supportMobile)")
                if url != nil{
                    UIApplication.shared.open(url!)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
