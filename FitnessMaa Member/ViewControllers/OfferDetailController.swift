//
//  OfferDetailController.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 30/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class OfferDetailController: BaseViewController {

    @IBOutlet weak var lblOfferTitle: UILabel!
    
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var lblOfferDescription: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var lblTermsDescription: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblInterestedpeople: UILabel!
    @IBOutlet weak var imgOffer: UIImageView!
    @IBOutlet weak var btnInterested: AppCustomButton!
    @IBOutlet weak var lblWebsite: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblMoreDetail: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTerms.font = UIFont.appMediumFont(size: 16)
        lblAddress.font = UIFont.appRegularFont(size: 14)
        lblWebsite.font = UIFont.appRegularFont(size: 14)
        lblMobileNo.font = UIFont.appRegularFont(size: 14)
        lblOfferTitle.font = UIFont.appMediumFont(size: 16)
        lblMoreDetail.font = UIFont.appMediumFont(size: 16)
        lblOfferDescription.font = UIFont.appRegularFont(size: 14)
        lblTermsDescription.font = UIFont.appRegularFont(size: 14)
        btnInterested.titleLabel?.font = UIFont.appMediumFont(size: 16)
        mainView.isHidden = true
        getOfferDetailApi()
        }
    

    
    @IBAction func btnInterestedClicked(_ sender: Any) {
        getOfferInterestedpeopleApi()
       
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // Api
    func getOfferDetailApi() {
        
        let param = [
            "offer_id":"2"
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_OFFER_DETAIL_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! OfferDetailModel
            self.mainView.isHidden = false
            self.setOfferDetailData(model: model)
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getOfferDetailApi()
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
            
        }
    }
    
    
    func getOfferInterestedpeopleApi() {
        
        let param = [
            "offer_id":"2"
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_OFFER_INTERESTED_API, params: param as [String : AnyObject], completion: { (response,message) in
            let model = response as! OfferDetailModel
            self.mainView.isHidden = false
            VIEWMANAGER.hideActivityIndicator()
            self.setOfferDetailData(model: model)
            CustomAlertView.showAlert(withTitle: "Thank You", messsage: message, completion: {
              //  self.navigationController?.popViewController(animated: true)
                
            })
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setOfferDetailData(model : OfferDetailModel){
        if  model.title != nil{
           lblOfferTitle.text = model.title
        }
        if model.details != nil{
            lblOfferDescription.text = model.details
        }
        if model.termsOfUse != nil{
            lblTermsDescription.text = (model.termsOfUse as! String)
        }
        if model.address != nil{
            lblAddress.text = (model.address as! String)
        }
        if model.contactNo != nil {
            lblMobileNo.text = model.contactNo as! String
        }
        if model.moreInfoLink != nil {
             lblWebsite.text = model.moreInfoLink
        }
        
        if  model.image != nil{
            imgOffer.sd_setImage(with: URL(string: MEDIA_URL + model.image), placeholderImage: UIImage(named:"placeholder_img"))
        }
        if model.totalInterestedPeople != nil{
             lblInterestedpeople.text = "\(model.totalInterestedPeople!) People are interested"
        }
  
    }

}
