//
//  NutritionistProfileController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 31/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import HCSStarRatingView

class NutritionistProfileController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgViewDpBG: UIImageView!
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEducation: UILabel!
    @IBOutlet weak var lblExperienceYear: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDiscountPrice: UILabel!
    @IBOutlet weak var lblAvailable: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var viewStarRatingSmall: UIView!
    @IBOutlet weak var lblClinicName: UILabel!
    @IBOutlet weak var lblDiscaountPercent: UILabel!
    @IBOutlet weak var lblClinicAddress: UILabel!
    @IBOutlet weak var lblAvailableDay: UILabel!
    @IBOutlet weak var lblAvailableTime: UILabel!
    @IBOutlet weak var lblServices: UILabel!
    @IBOutlet weak var viewStarRatingBig: HCSStarRatingView!
    
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnMobile: UIButton!
    @IBOutlet weak var starBgView: UIView!
    
    @IBOutlet weak var btnSubmit: AppCustomButton2!
    
    var strPostedUserId : String!
    var model : NutritionistProfileModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.isHidden = true
        imgViewDP.layer.cornerRadius = imgViewDP.bounds.height/2
        imgViewDP.layer.masksToBounds = true
        lblRating.layer.cornerRadius = lblRating.bounds.height/2
        lblRating.layer.masksToBounds = true
       
        

        setFont()
        getNutritionistProfileApi()
        
        if VIEWMANAGER.currentUser?.id == nil{
            btnSubmit.isHidden = true
            viewStarRatingBig.isHidden = true
            starBgView.isHidden = true
             lblRating.text = "0"
        }else{
            btnSubmit.isHidden = false
            viewStarRatingBig.isHidden = false
            starBgView.isHidden = false
        }
        
    }
 
    @IBAction func btnSubmitClicked(_ sender: Any) {
        getNutritionistRatingApi()
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMobileClicked(_ sender: Any) {

        let url = URL(string: "telprompt://\(model.contactNo)")
        if url != nil{
            UIApplication.shared.open(url!)
        }
    }
    
    
    @IBAction func btnEmailClicked(_ sender: Any) {
        if let url = URL(string: "mailto:\(model.email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    func setFont(){
        btnMobile.titleLabel?.font = UIFont.appRegularFont(size: 16)
        lblClinicName.font = UIFont.appRegularFont(size: 16)
        lblDiscountPrice.font = UIFont.appRegularFont(size: 16)
        lblProfession.font = UIFont.appRegularFont(size: 16)
        lblPrice.font = UIFont.appRegularFont(size: 16)
        lblName.font = UIFont.appMediumFont(size: 16)
        lblAbout.font = UIFont.appRegularFont(size: 16)
        lblServices.font = UIFont.appRegularFont(size: 16)
        lblEducation.font = UIFont.appRegularFont(size: 16)
        lblAvailable.font = UIFont.appRegularFont(size: 16)
        lblAvailableTime.font = UIFont.appRegularFont(size: 16)
        lblExperienceYear.font = UIFont.appRegularFont(size: 16)
        btnEmail.titleLabel?.font = UIFont.appRegularFont(size: 16)
        
    }
    
    // Api
    func getNutritionistProfileApi() {
        
        let param = [
            "nutritionist_id": strPostedUserId,
            "user_id":VIEWMANAGER.currentUser?.id
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_NUTRITIONIST_PROFILE_API, params: param as [String : AnyObject], completion: { (response) in
            let modelProfile = response as! NutritionistProfileModel
            self.model = modelProfile
            self.mainView.isHidden = false
            self.setProfileData()
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getNutritionistProfileApi()
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
            
        }
    }
    
    func getNutritionistRatingApi() {
        
        let param = [
            "nutritionist_id": strPostedUserId,
            "user_id":VIEWMANAGER.currentUser?.id,
            "rating": viewStarRatingBig.value
           
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: ADD_RETTING_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! NutritionistProfileModel
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
   

    
    func setProfileData(){
        
         lblPrice.text = model.currency_symbol + " " + model.fees
         let attributedString = NSMutableAttributedString(string: lblPrice.text!)
        attributedString.addAttribute(NSAttributedStringKey.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.lightGray, range: NSMakeRange(0, attributedString.length))
        lblPrice.attributedText = attributedString
        
        lblName.text = model.name
        lblAbout.text = model.aboutUs
        lblEducation.text = model.qualification
        lblExperienceYear.text = model.yearOfExp + " Year Experience"
        lblProfession.text = model.qualification
        lblClinicName.text = model.clinicName
         btnMobile.setTitle(" "+model.contactNo, for: .normal)
        lblServices.text = getServicesFormatted(services: model.services)
        lblAvailableTime.text = model.timings
        lblDiscaountPercent.text = "(" + (model.discountInPercent) + " % FitnessMaa Discount)"
        lblDiscountPrice.text =   model.currency_symbol + " " + calculateDiscountprise(feesStr: model.fees, discountStr: model.discountInPercent)
        
         imgViewDP.sd_setImage(with: URL(string: "\(MEDIA_BASE_URL2)\(model.image!)"), placeholderImage: UIImage(named: "ic_user_placeholder"))
        let rating = String(format: "%.f", Float(model.avg_rating)!)
        lblRating.text = rating
        if model.my_rating != nil{
        guard let bigRatingVal = NumberFormatter().number(from: model.my_rating) else { return }
        viewStarRatingBig.value = CGFloat(bigRatingVal)
        }

        btnEmail.setTitle(" "+model.email, for: .normal)
        
        if model.hideEmail == "true" {
            btnEmail.isHidden = true
        }else{
            btnEmail.isHidden = false
        }
        if model.hideContactNo == "true" {
            btnMobile.isHidden = true
            lblAvailable.isHidden = true
        }else{
            btnMobile.isHidden = false
             lblAvailable.isHidden = false
        }
        
     
    }
    
    func calculateDiscountprise(feesStr : String , discountStr : String) -> String{
        
        var discount : Float
        var fee : Float
        var result : Float
        discount  = Float(discountStr)!
        fee = Float(feesStr)!
        
        result = fee * discount/100
        fee = fee - result
        
        return String(fee)
        
    }
    
    func getServicesFormatted(services : String) -> String {
    
       let txt = ",\(services)"
        return txt.replacingOccurrences(of: ",", with: "\n★ ")

    }
    
}
