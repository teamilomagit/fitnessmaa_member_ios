//
//  ForgetPassController.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 09/06/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ForgetPassController: UIViewController {

  
    @IBOutlet weak var txtFieldTempPass: AppTextField!
    
    @IBOutlet weak var txtFieldConfirmPass: AppTextField!
    @IBOutlet weak var txtFieldNewPass: AppTextField!
    var email : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if isValidate(){
            resetPasswordAPI()
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    func isValidate() -> Bool {
        
        if txtFieldTempPass.text!.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter temporary password")
            return false
        }
        
        if txtFieldNewPass.text!.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter new password")
            return false
        }
        if txtFieldConfirmPass.text!.trim().count == 0  ||
            (txtFieldConfirmPass!.text !=  txtFieldNewPass.text!){
            VIEWMANAGER.showToast("Please enter valid confirm password")
            return false
        }
        return true
    }
    
    func resetPasswordAPI() {
        
        let param = [
            "email" : email,
            "temp_password":txtFieldTempPass.text!,
            "new_password": txtFieldNewPass.text!

        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: RESET_PASS_API, params: param as [String : AnyObject], completion: { (response , msg) in
            VIEWMANAGER.hideActivityIndicator()
            
            let memberSignupVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "ForgetPassController") as! ForgetPassController
            self.navigationController?.pushViewController(memberSignupVC, animated: true)
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                   self.resetPasswordAPI()
                })
            }else{
                 VIEWMANAGER.showToast(errMsg!)
            }
            
           
        }
    }
    
}
