//
//  LoginViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {

    @IBOutlet weak var txtFieldEmail: CustomTextField!
    @IBOutlet weak var txtFieldPassword: CustomTextField!
    @IBOutlet weak var btnLogin: AppCustomButton!
    
    @IBOutlet weak var btnBack: UIButton!
    var iconClick = true
    
    var backBtnShow : Bool!

    override func viewDidLoad() {
        super.viewDidLoad()

        btnLogin.layer.cornerRadius = 25
        btnLogin.layer.masksToBounds = true
        
        //txtFieldMobileNo.setTextFieldTag(tag: .TAG_MOBILE)
        txtFieldEmail.text = "amol4@gmail.com"
        txtFieldPassword.text = "abcd1234"
        
        if backBtnShow == false {
            btnBack.isHidden = true
        }
        
        txtFieldPassword.rightViewMode = .unlessEditing
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "show_password"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button.frame = CGRect(x: CGFloat(txtFieldPassword.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        button.tintColor = UIColor.red
        button.contentMode = .scaleToFill
        button.addTarget(self, action: #selector(self.ShowHide), for: .touchUpInside)
        txtFieldPassword.rightView = button
        txtFieldPassword.rightViewMode = .always
    }
    
    
    @IBAction func ShowHide(_ sender: UIButton) {
        if(iconClick == true) {
            txtFieldPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password"), for: .normal)
        } else {
            txtFieldPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password"), for: .normal)
        }
        iconClick = !iconClick
    }
    
    func performLoginAPI() {
        
        let param = [
            "email" : txtFieldEmail.text!,
            "password":txtFieldPassword.text!,
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: LOGIN_NORMAL_USER_URL, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.currentUser = response as? UserModel
            DispatchQueue.main.async {
                VIEWMANAGER.showToast("Member login successfully!")
            }
            app_del.setDashboarAsRoot()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    

    @IBAction func btnLoginClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            performLoginAPI()
        }
    }
    
    func validate() -> Bool {
        if  txtFieldEmail.text?.trim().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter mobile number"))
             return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email id"))
            return false
        }
        if txtFieldPassword.text?.trim().count == 0 {
            VIEWMANAGER.showToast(localize(string: "Please enter password"))
            return false
        }
        return true
    }

    override func viewDidLayoutSubviews() {
        self.btnLogin.applyGradient(colours: [UIColor.appDarkThemeColor, UIColor.appLightThemeColor])
        self.btnLogin.setTitle("Login", for: .normal)
        
    }
    @IBAction func btnRegisterClicked(_ sender: Any) {
        let memberSignupVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "MemberSignUpController") as! MemberSignUpController
        self.navigationController?.pushViewController(memberSignupVC, animated: true)
        
    }
    
    
    @IBAction func btnForgetPassClicked(_ sender: Any) {
        
        showForgetPasswordView()
    }
    
        func showForgetPasswordView()
        {
            let forgetPassView = Bundle.main.loadNibNamed("ForgetPassValidationView", owner: self, options: [:])?.first as! ForgetPassValidationView
            forgetPassView.frame = self.view.bounds
            self.view.addSubview(forgetPassView)
            forgetPassView.onSubmitBtnClicked {
                //  email validation Api call
                if self.validateEmail(emailstr: forgetPassView.txtFieldEmail.text!){
                    self.forgotPasswordAPI(emailStr: forgetPassView.txtFieldEmail.text!)
                }
            }
            
        }
    
    
    func validateEmail(emailstr : String) -> Bool {
        
        if emailstr.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter email")
            return false
        }
        
        if !emailstr.isValidEmail() {
            VIEWMANAGER.showToast(localize(string: "Please enter valid email id"))
            return false
        }
        return true
    }
    
    
    
    func forgotPasswordAPI(emailStr : String) {
        
        let param = [
            "email" : emailStr
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: FORGOT_PASS_API, params: param as [String : AnyObject], completion: { (response,msg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(msg)
            let forgotVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "ForgetPassController") as! ForgetPassController
            forgotVC.email = emailStr
            self.navigationController?.pushViewController(forgotVC, animated: true)
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
