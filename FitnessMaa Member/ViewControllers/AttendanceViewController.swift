//
//  AttendanceViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class AttendanceViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    var pageNo = 1
    var loadStarted : Bool = false
    
    var refresh : UIRefreshControl!
    var arrList = NSMutableArray()

    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var lblStartDayMonth: UILabel!
    @IBOutlet weak var btnStartDate: UIButton!
    
    @IBOutlet weak var lblTotalDays: UILabel!
    @IBOutlet weak var tblViewList: UITableView!
    var strDate : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDateView()
        
        tblViewList.register(UINib(nibName: "AttendanceListCell", bundle: nil), forCellReuseIdentifier: "AttendanceListCell")
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewList.addSubview(refresh)
        self.lblTotalDays.isHidden = true

        formattedDate(selDate: Date()) { (dateToPass, dateToShow) in
            let arr = dateToShow.components(separatedBy: "-")
            if arr.count == 2{
                self.lblStartDayMonth.text = arr[0]
                self.btnStartDate.setTitle(arr[1], for: .normal)
            }
            strDate = dateToPass
            self.getMemberAttendanceHistory(pageNo: pageNo, date: dateToPass)
        }
    }
    
    @objc func pullToRefresh() {
        pageNo = 1
        self.getMemberAttendanceHistory(pageNo: pageNo, date: strDate, showLoader: false, isRefresh: true)
    }
    
    func getMemberAttendanceHistory(pageNo:Int,date:String,showLoader : Bool = true,isRefresh:Bool = false) {
        let param = [
            "page" : pageNo,
            "limit" : LIMIT,
            "gym_id" : VIEWMANAGER.currentUser!.gymId,
            "member_id":VIEWMANAGER.currentUser!.id,
            "attendance_date":date
            ] as [String : Any]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: MEMBER_ATTENDANCE_LIST_URL, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.refresh.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = responseData as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            self.arrList.addObjects(from: arr as! [Any])
            
            if self.arrList.count == 0 {
                self.lblTotalDays.isHidden = true
                self.tblViewList.backgroundView = self.tblBGView()
            }
            else {
                self.lblTotalDays.isHidden = false
                self.lblTotalDays.text = "Total: \(self.arrList.count) days"
                self.tblViewList.backgroundView = nil
            }
            self.tblViewList.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
//    func tblBGView() -> UILabel {
//        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
//        lblBG.font = UIFont.appRegularFont(size: 18)
//        lblBG.text = "No data found"
//        lblBG.textAlignment = .center
//        return lblBG
//    }
    
    func setupDateView() {
        viewDate.layer.borderWidth = 1.0
        viewDate.layer.borderColor = UIColor.white.cgColor
        viewDate.layer.cornerRadius = 15
        
        lblStartDayMonth.font = UIFont.appRegularFont(size: 14)
        lblStartDayMonth.textColor = UIColor.appDarkThemeColor
        
        btnStartDate.titleLabel?.font = UIFont.appRegularFont(size: 14)
        btnStartDate.setTitleColor(.white, for: .normal)
        btnStartDate.setImage(UIImage(named:"ic_drop_down"), for: .normal)
        btnStartDate.semanticContentAttribute = .forceRightToLeft
        btnStartDate.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceListCell") as! AttendanceListCell
        let model = arrList[indexPath.row] as! AttendanceModel
        cell.lblName.text = attendanceFormattedDate(strDate: model.inTime)
        cell.lblMobile.text = VIEWMANAGER.currentUser?.mobile
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let feedsVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "FeedsViewController") as! FeedsViewController
        self.navigationController?.pushViewController(feedsVC, animated: true)
    }
    
    func attendanceFormattedDate(strDate:String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: strDate)

        if date != nil {
            dateFormat.dateFormat = "dd MMM yyyy"
            return dateFormat.string(from: date!)
        }
        return ""
    }

    func formattedDate(selDate:Date!,completion:((String,String) ->())){
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM"
        var dateToPass: String? = nil
        var dateToShow: String? = nil
        
        dateToPass = dateFormat.string(from: selDate)
        
        dateFormat.dateFormat = "MMM-yyyy"
        dateToShow = dateFormat.string(from: selDate)
        
        completion(dateToPass!, dateToShow!)
    }
    
    @IBAction func btnDateClicked(_ sender: Any) {
        
        let datePicker = DatePickerView(frame: UIScreen.main.bounds)
        UIApplication.shared.keyWindow?.addSubview(datePicker)
        datePicker.month = lblStartDayMonth.text!
        datePicker.year = Int(btnStartDate.titleLabel!.text!)!
        datePicker.onDoneClicked { (month,year) in
            
            self.strDate = "\(month)-\(year)"
            self.arrList.removeAllObjects()
            self.pageNo = 1
            self.getMemberAttendanceHistory(pageNo: self.pageNo, date: self.strDate, showLoader: true, isRefresh: false)
            self.lblStartDayMonth.text = month
            self.btnStartDate.setTitle(year, for: .normal)

         }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tblViewList.contentOffset.y >= (tblViewList.contentSize.height - tblViewList.frame.size.height) {
            if loadStarted {
                self.getMemberAttendanceHistory(pageNo: pageNo, date: strDate,showLoader: false)
                loadStarted = false
            }
        }
    }
    

}
