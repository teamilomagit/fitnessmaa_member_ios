//
//  MemberSignUpController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 21/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class MemberSignUpController: UIViewController,UITextFieldDelegate {
  
    @IBOutlet weak var txtFieldFullName: AppTextField2!
    @IBOutlet weak var txtFieldEmail: AppTextField2!
    @IBOutlet weak var txtFieldMobile: AppTextField2!
    @IBOutlet weak var txtFieldCountry: AppTextField2!
    @IBOutlet weak var txtFieldState: AppTextField2!
    @IBOutlet weak var txtFieldCity: AppTextField2!
    @IBOutlet weak var txtFieldRefferdBy: AppTextField2!
    @IBOutlet weak var txtFieldPassword: AppTextField2!
    @IBOutlet weak var txtFieldConfirmPassword: AppTextField2!
    @IBOutlet weak var btnCheckbox: UIButton!
    
    @IBOutlet weak var btnSubmit: AppCustomButton!
    
    var countryData : [CountryModel]!
    var selCountryId : String!
    
    var stateData : [StateModel]!
    var selStateId : String!
    
    var cityData : [CityModel]!
    var selCityId : String!
    var btnTag    : Int = 0
    var iconClickFirst = true
    var iconClickSecond = true


    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtFieldCountry.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldState.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldCity.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldMobile.setTextFieldTag(tag: .TAG_MOBILE)
        btnTag = btnCheckbox.tag
        btnTag = 1
        getCountries()
        txtFieldPassword.rightViewMode = .unlessEditing
        let button1 = UIButton(type: .custom)
        button1.setImage(UIImage(named: "show_password"), for: .normal)
        button1.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button1.frame = CGRect(x: CGFloat(txtFieldPassword.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        button1.tintColor = UIColor.red
        button1.contentMode = .scaleToFill
        button1.addTarget(self, action: #selector(self.ShowHidePass), for: .touchUpInside)
        txtFieldPassword.rightView = button1
        txtFieldPassword.rightViewMode = .always
        
        txtFieldConfirmPassword.rightViewMode = .unlessEditing
        let button2 = UIButton(type: .custom)
        button2.setImage(UIImage(named: "show_password"), for: .normal)
        button2.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        button2.frame = CGRect(x: CGFloat(txtFieldConfirmPassword.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        button2.tintColor = UIColor.red
        button2.contentMode = .scaleToFill
        button2.addTarget(self, action: #selector(self.ShowHideConPass), for: .touchUpInside)
        txtFieldConfirmPassword.rightView = button2
        txtFieldConfirmPassword.rightViewMode = .always
        
        let btnPopup = UIButton(type: .custom)
        btnPopup.setImage(UIImage(named: "ic_exclamation"), for: .normal)
        btnPopup.imageEdgeInsets = UIEdgeInsetsMake(0, -16, 0, 0)
        btnPopup.frame = CGRect(x: CGFloat(txtFieldRefferdBy.frame.size.width - 25), y: CGFloat(5), width: CGFloat(30), height: CGFloat(30))
        btnPopup.tintColor = UIColor.red
        btnPopup.contentMode = .scaleToFill
        btnPopup.addTarget(self, action: #selector(self.showPopup), for: .touchUpInside)
        txtFieldRefferdBy.rightView = btnPopup
        txtFieldRefferdBy.rightViewMode = .always
        
        txtFieldCountry.onDropDownDataSelection { (country) in
            let arrFilter = self.countryData.filter { $0.name == country }
            let model = arrFilter[0]
            self.selCountryId = model.id
            self.stateData = nil
            self.selStateId = nil
            self.txtFieldState.text = ""
            self.getStates(countryId: self.selCountryId)
            
            
            //  let filterCurrency = self.arrCurrencies.filter { $0.id == model.id }
            
        }
        
        txtFieldState.onDropDownDataSelection { (state) in
            let arrFilter = self.stateData.filter { $0.name == state }
            let model = arrFilter[0]
            self.selStateId = model.id
            self.getCities(cityId: self.selStateId)
            self.cityData = nil
            self.selCityId = nil
            self.txtFieldCity.text = ""
        }
        txtFieldCity.onDropDownDataSelection { (city) in
            let arrFilter = self.cityData.filter { $0.name == city }
            let model = arrFilter[0]
            self.selCityId = model.id
        }
        
     
        
        btnCheckbox.addTarget(self, action: #selector(btnCheckboxClicked), for: .touchUpInside)

    }
    
    @IBAction func ShowHidePass(_ sender: UIButton) {
        if(iconClickFirst == true) {
            txtFieldPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password"), for: .normal)
        } else {
            txtFieldPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password"), for: .normal)
        }
        iconClickFirst = !iconClickFirst
    }
 

    @IBAction func ShowHideConPass(_ sender: UIButton) {
        if(iconClickSecond == true) {
            txtFieldConfirmPassword.isSecureTextEntry = false
            sender.setImage(UIImage(named: "hide_password"), for: .normal)
        } else {
            txtFieldConfirmPassword.isSecureTextEntry = true
            sender.setImage(UIImage(named: "show_password"), for: .normal)
        }
        iconClickSecond = !iconClickSecond
    }
    
  @IBAction func showPopup(_ sender: UIButton){
    CustomAlertView.showAlert(withTitle: "!", messsage: "") {
        
    }
        
    }
    
    func getCountries() {
        let param = [String : AnyObject]()
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_COUNTRIES_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.countryData = responseData as? [CountryModel]
            let arrCountry = self.countryData.map { $0.name }
            self.txtFieldCountry.dropDownData = arrCountry as NSArray
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    func getStates(countryId:String) {
        let param = ["id" : countryId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_STATE_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.stateData = responseData as? [StateModel]
            let arrState = self.stateData.map { $0.name }
            self.txtFieldState.dropDownData = arrState as NSArray
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    func getCities(cityId:String) {
        let param = ["id" : cityId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_CITIES_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.cityData = responseData as? [CityModel]
            let arrCity = self.cityData.map { $0.name }
            self.txtFieldCity.dropDownData = arrCity as NSArray
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
    
    @objc func btnCheckboxClicked(_ sender:UIButton){
        if btnTag == 1 {
            btnCheckbox.setImage(UIImage(named: "ic_check_selected"), for: .normal)
            btnTag = 0
        }else{
            btnCheckbox.setImage(UIImage(named: "ic_check_normal"), for: .normal)
            btnTag = 1
        }
    }
    
    func registrationService() {
        
        let param = [
            "full_name": txtFieldFullName.text!,
            "email": txtFieldEmail.text!,
            "mobile": txtFieldMobile.text!,
            "country_id": selCountryId,
            "state_id" : selStateId,
            "city_id" : selCityId,
            "country": txtFieldCountry.text!,
            "state": txtFieldState.text!,
            "city": txtFieldCity.text!,
            "reffered_by": txtFieldRefferdBy.text!,
            "password": txtFieldPassword.text!,
            
            ] as [String : AnyObject]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:REGISTRATION_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            CustomAlertView.showAlert(withTitle: "Success", messsage: responseData as! String, completion: {
                self.navigationController?.popViewController(animated: true)
            })
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let selectLocationVC = storyboard.instantiateViewController(withIdentifier: "SelectLatLongController") as! SelectLatLongController
//        self.navigationController?.pushViewController(selectLocationVC, animated: true)
//        selectLocationVC.onSelectLocation { (lat, lng) in
//            textField.text = "\(lat), \(lng)"
//        }
//        return false
//    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        self.view.endEditing(true)
        if validate() {
            registrationService()
        }
    }
    
    func validate() -> Bool
    {
        
        if txtFieldFullName.text?.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter full name")
            return false
        }
        if txtFieldEmail.text?.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter email id")
            return false
        }
        if !txtFieldEmail.text!.isValidEmail() {
            VIEWMANAGER.showToast("Please enter valid email id")
            return false
        }
        if txtFieldMobile.text?.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter mobile number")
            return false
        }
        
        if selCountryId == nil {
            VIEWMANAGER.showToast("Please select country")
            return false
        }
        if selStateId == nil {
            VIEWMANAGER.showToast("Please select state")
            return false
        }
        if selCityId == nil {
            VIEWMANAGER.showToast("Please select city")
            return false
        }
     
        if txtFieldPassword.text?.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter password")
            return false
        }
        if txtFieldConfirmPassword.text?.trim().count == 0 {
            VIEWMANAGER.showToast("Please enter confirm password")
            return false
        }
        if txtFieldPassword.text != txtFieldConfirmPassword.text{
            VIEWMANAGER.showToast("Your password abd confirm password shouid be same.")
            
        }
        if btnTag == 1{
            VIEWMANAGER.showToast("Please select Terms and Condition.")
            return false
            
        }
        return true
    }
    
//    func showVerifyDetailView()
//    {
//        let viewVerifyDetails = Bundle.main.loadNibNamed("VerifyDetailsView", owner: self, options: [:])?.first as! VerifyDetailsView
//        viewVerifyDetails.frame = self.view.bounds
//        self.view.addSubview(viewVerifyDetails)
//        viewVerifyDetails.onProceedButtonCliked {
//            self.registrationService()
//        }
//    }
    
    override func viewDidLayoutSubviews() {
        self.txtFieldFullName.attributedPlaceholder =
            NSAttributedString(string: "Full Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
       
        self.txtFieldEmail.attributedPlaceholder =
            NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldMobile.attributedPlaceholder =
            NSAttributedString(string: "Mobile", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldCountry.attributedPlaceholder =
            NSAttributedString(string: "Country", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldState.attributedPlaceholder =
            NSAttributedString(string: "State", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldCity.attributedPlaceholder =
            NSAttributedString(string: "City", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldRefferdBy.attributedPlaceholder =
            NSAttributedString(string: "Reffered by", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
   
        self.txtFieldPassword.attributedPlaceholder =
            NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        self.txtFieldConfirmPassword.attributedPlaceholder =
            NSAttributedString(string: "Confirm Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
    }
    @IBAction func btnTermsAndConditionClicked(_ sender: Any) {
        let TAndCVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "TermsAndCondition") as! TermsAndCondition
        self.navigationController?.pushViewController(TAndCVC, animated: true)
        
    }
}
