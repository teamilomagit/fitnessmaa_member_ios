//
//  ProfileViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 11/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController ,UITextFieldDelegate{

    @IBOutlet weak var baseScroll: UIScrollView!
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var txtFieldName: AppTextField2!
    @IBOutlet weak var txtFieldMobile: AppTextField2!
    
    @IBOutlet weak var txtFieldEmail: AppTextField2!
    @IBOutlet weak var txtFieldGender: AppTextField2!
    
    @IBOutlet weak var txtfieldCountry: AppTextField2!
    
    @IBOutlet weak var txtFieldCity: AppTextField2!
    @IBOutlet weak var txtfielsState: AppTextField2!
    @IBOutlet weak var txtFieldDob: AppTextField!
    var model : UserModel!
    
    var profilePath : String!

    
    var countryData : [CountryModel]!
    var selCountryId : String!
    
    var stateData : [StateModel]!
    var selStateId : String!
    
    var cityData : [CityModel]!
    var selCityId : String!
    
     var dropDownData : [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  baseScroll.isHidden = true
       
        getUserProfileDate()
        txtfieldCountry.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtfielsState.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldCity.setTextFieldTag(tag: .TAG_DROP_DOWN)
        txtFieldDob.setTextFieldTag(tag: .TAG_DATE_PICKER)
        txtFieldGender.setTextFieldTag(tag: .TAG_ACTION_SHEET)
        
        txtFieldGender.actionSheetData = ["Male","Female","Transgender"]
        
      selCityId = VIEWMANAGER.currentUser?.cityId
        selStateId = VIEWMANAGER.currentUser?.stateId
        selCountryId = VIEWMANAGER.currentUser?.countryId
        
        txtfieldCountry.onDropDownFieldClicked {
            self.getCountries()
        }
        txtfielsState.onDropDownFieldClicked {
            if self.selCountryId != nil{
                self.getStates(countryId: self.selCountryId)
            }else{
                VIEWMANAGER.showToast("Please Select Country")
            }
            
        }
        txtFieldCity.onDropDownFieldClicked {
            if self.selCountryId != nil || self.selStateId != nil{
            self.getCities(stateId: self.selStateId)
            }else{
                 VIEWMANAGER.showToast("Please Select Country & State")
            }
        }
        
     
    }
    

    
    
    @IBAction func btnProfileClicked(_ sender: Any) {
        
        VIEWMANAGER.showActionSheet(["Camera","Gallary"]) { (selStr) in
            if selStr == "Camera" {
                self.showImagePicker(sourceType: .camera)
            }
            else {
                self.showImagePicker(sourceType: .photoLibrary)
            }
        }
    }
    
    
    func showImagePicker(sourceType:UIImagePickerController.SourceType) {
        VIEWMANAGER.showImagePicker(sourceType: .photoLibrary, block: { (img, imgPath) in
            self.imgViewDP.image = img
            self.profilePath = imgPath
            
            if self.profilePath != nil
            {
                self.uploadMemberProfilePic()
            }
            
        })
    }
    
    
    func uploadMemberProfilePic() {
        let param = [
            "file":profilePath,
            "user_id":VIEWMANAGER.currentUser?.id,
            "app_type":"MEMBER"
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().uploadImageToServer(apiName: UPDATE_PROFILE_PHTO_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.showToast("Profile picture updated successfully")
        }) { (errMsg) in

        }
    }
    
    func updateProfileDetailsService() {
        let param = [
            "user_id" : VIEWMANAGER.currentUser!.id,
            "full_name": txtFieldName.text!,
            "email": txtFieldEmail.text!,
            "mobile": txtFieldMobile.text!,
            "gender": txtFieldGender.text!,
            "dob": txtFieldDob.text!,
            "country_id": selCountryId,
            "state_id": selStateId,
            "city_id": selCityId

            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: UPDATE_PROFILE_API, params: param as [String : AnyObject], completion: { (response) in
            VIEWMANAGER.hideActivityIndicator()
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: REFRESH_MEMBER_LIST_NOTIFICATION), object: nil)
            self.navigationController?.popViewController(animated: true)
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        updateProfileDetailsService()
    }
    
 
    func getUserProfileDate() {
        let param = [
            "user_id" : VIEWMANAGER.currentUser!.id
        ]

        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: MEMBER_PROFILE_URL, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.baseScroll.isHidden = false
            self.model = responseData as? UserModel
            self.setProfileData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }

    func setProfileData() {
         imgViewDP.sd_setImage(with: URL(string: "\(MEDIA_BASE_URL2)\(model.image!)"), placeholderImage: UIImage(named: "ic_user_placeholder"))
        txtFieldName.text = model.fullName
        txtFieldMobile.text = model.mobile
        txtFieldEmail.text = model.email
        if model.gender != nil{
        txtFieldGender.text = model.gender as? String
        }
        if model.dob != nil{
            txtFieldDob.text = formattedDate(strDate: (model.dob as? String)!)
        }
        txtFieldCity.text = model.city_name
        txtfielsState.text = model.state_name
        txtfieldCountry.text = model.country_name

    }
    
    func formattedDate(strDate : String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.date(from: strDate)
        if date != nil {
            formatter.dateFormat = "dd-MMM-yyyy"
            return formatter.string(from: date!)
        }
        return ""
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    func getCountries() {
        let param = [String : AnyObject]()
        RemoteAPI().callPOSTApi(apiName:GET_COUNTRIES_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.countryData = responseData as? [CountryModel]
            //let arrCountry = self.countryData.map { $0.name }
            
            //self.dropDownData = arrCountry as? [String]
            self.showDropDown(title: "Select Country",dropDowndata:  self.countryData , type: "country")
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
    func getStates(countryId:String) {
        let param = ["id" : countryId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_STATE_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.stateData = responseData as? [StateModel]
            let arrState = self.stateData.map { $0.name }
            self.dropDownData = arrState as? [String]
            self.showDropDown(title: "Select State",dropDowndata: self.stateData, type: "state")
            // self.lblState.dropDownData = arrState as? [String]
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    func getCities(stateId:String) {
        let param = ["id" : stateId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_CITIES_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.cityData = responseData as? [CityModel]
            let arrCity = self.cityData.map { $0.name }
            self.showDropDown(title: "Select City",dropDowndata:  self.cityData , type: "city")
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
    func showDropDown(title : String , dropDowndata : [AnyObject]!,  type: String)
    {
        if dropDowndata == nil {
            return
        }
        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: title,data:NSArray() as! [String])
        UIApplication.shared.keyWindow?.addSubview(dropDown)
        dropDown.onDoneClicked { (data) in
            
            if data is CountryModel {
                let model = data as! CountryModel
                self.txtfieldCountry.text = model.name
                self.selCountryId = model.id
               
                
            }
            else if data is StateModel {
                let model = data as! StateModel
                self.txtfielsState.text = model.name
                self.selStateId = model.id
    
            }
            else if data is CityModel {
                let model = data as! CityModel
                self.txtFieldCity.text = model.name
                self.selCityId = model.id
                
            }
            
        }
        
        dropDown.updateList(list: dropDowndata! as NSArray, type: type)
    }
  

}
