//
//  FeedsViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import RZTransitions
class FeedsViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {

    let FEED_TYPE_BANNER = "BANNER"
    let FEED_TYPE_YOUTUBE = "YOUTUBE"
    let FEED_TYPE_WEBLINK = "WEBLINK"
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tblViewList: UITableView!
    @IBOutlet weak var tagView: FeedsFilterView!
    
   // @IBOutlet weak var locationView: LocationWithNotificationView!
    
    var arrList = NSMutableArray()
    var pageNo = 1
    var loadStarted : Bool = false
    var refreshControl = UIRefreshControl()
    var tagType = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tagType = "All"
        
        
      //  addCitySelectionWithNotification()
        tblViewList.register(UINib(nibName: "FeedsTableCell", bundle: nil), forCellReuseIdentifier: "FeedsTableCell")
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewList.addSubview(refreshControl)
        getLatestFeedAPI(pageNo: pageNo)
        setupNavigationBar()
        
        self.tagView.onFilterChange { (filter) in
            self.tagType = filter!
            self.getLatestFeedAPI(pageNo: self.pageNo)
        }
    }
    
    func setupNavigationBar() {
        let navBar = Bundle.main.loadNibNamed("LocationWithNotificationView", owner: self, options: nil)?[0] as! LocationWithNotificationView
        navView.addSubview(navBar)
        navBar.enableAutoLayout()
        navBar.leadingMargin(pixels: 0)
        navBar.trailingMargin(pixels: 0)
        navBar.topMargin(pixels: 0)
        navBar.bottomMargin(pixels: 0)
        
        if Preferences.getGeneralSettingModelData() != nil{
            let model  :  GeneralSettingModel
            model = Preferences.getGeneralSettingModelData()!
            navBar.lblCityName.text = model.defualtCityName
        }
        else{
            if Preferences.getUserCurrentCity() != nil{
                let userLocationModel = Preferences.getUserCurrentCity()
                navBar.lblCityName.text = userLocationModel?.cityName
            }
        }
        
        navBar.onNotificationClicked {
            
            let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificaitonVC, animated: true)
        }
        navBar.onLocationClicked {
            
            let cityVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "CitySelectionController") as! CitySelectionController
            self.navigationController?.present(cityVC, animated: true, completion: nil)
            cityVC.onLocationSaveClicked {str in
                navBar.lblCityName.text = str
            }
        }
        
    }
    
    @objc func pullToRefresh() {
        pageNo = 1
        self.getLatestFeedAPI(pageNo: pageNo,showLoader: false, isRefresh: true)
    }
    
    func getLatestFeedAPI(pageNo:Int,showLoader : Bool = true,isRefresh:Bool = false) {
      
        let param = [
            "user_id":VIEWMANAGER.currentUser?.id,
            "tags":tagType,
            "country_id":VIEWMANAGER.currentUser?.countryId,
            "page_no":pageNo,
            "limit":10

            ] as [String : Any]
        if showLoader{
             VIEWMANAGER.showActivityIndicator()
        }else{
            VIEWMANAGER.hideActivityIndicator()
        }
 
        RemoteAPI().callPOSTApi(apiName: GET_LATEST_FEED_API, params: param as [String : AnyObject], completion: { (feeds,tags,message) in
            
            VIEWMANAGER.hideActivityIndicator()
            
            self.refreshControl.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = feeds as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            
            self.tagView.updateFilter(tagArray: tags as! NSArray)
            
            self.arrList.addObjects(from: arr as! [Any])
            self.tblViewList.backgroundView =  self.arrList.count == 0 ? self.tblBGView() : nil
            self.tblViewList.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            self.refreshControl.endRefreshing()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getLatestFeedAPI(pageNo: pageNo)
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedsTableCell") as! FeedsTableCell
        cell.indexPath = indexPath
        let model = arrList[indexPath.row] as! FeedsModel
        cell.lblTitle.text = model.title
        cell.imgViewBanner.sd_setImage(with: URL(string: MEDIA_URL + model.imageUrl!)!, placeholderImage: UIImage())
        cell.btnPlayVideo.isHidden = model.type != FEED_TYPE_YOUTUBE
        
        cell.btnLike.tintColor = model.liked_by_me != nil ? UIColor.blue : UIColor.gray

        cell.onPlayVideoClicked { (indexPath) in
            let model = self.arrList[indexPath.row] as! FeedsModel
            self.viewFeedIncrementApi(model: model)
            if model.url != nil{
               self.playVideo(videoURL: model.url)
            }
            
        }
       
       
        
        
        if model.posted_by_user_type == "SocialPage"{
            cell.lblPostUserName.text = model.social_page_name
            
            if model.social_page_image != nil{
                cell.PostUserPic.sd_setImage(with: URL(string: MEDIA_URL + model.social_page_image!)!, placeholderImage: UIImage(named: "ic_user_placeholder"))
            }
        }else if model.posted_by_user_type == "Nutritionist"{
            cell.lblPostUserName.text = model.nutritionist_name
            
            if model.nutritionist_image != nil{
                cell.PostUserPic.sd_setImage(with: URL(string: MEDIA_URL + model.nutritionist_image!)!, placeholderImage: UIImage(named: "ic_user_placeholder"))
            }
        }
       
        
        
        cell.onLikeBtnClicked { (indexPath) in
            if VIEWMANAGER.currentUser != nil{
            
            let model = self.arrList[indexPath.row] as! FeedsModel
            if model.liked_by_me == nil {
                self.likeFeedApi(model: model)
                model.liked_by_me = VIEWMANAGER.currentUser?.id
                var count = Int(model.no_of_likes)!
                count = count + 1
                model.no_of_likes = "\(count)"
                self.arrList.replaceObject(at: indexPath.row, with: model)
                tableView.reloadRows(at: [indexPath], with: .none)
            }else{
                self.UNlikeFeedApi(model: model)
                model.liked_by_me = nil
                var count = Int(model.no_of_likes)!
                count = count - 1
                model.no_of_likes = "\(count)"
                self.arrList.replaceObject(at: indexPath.row, with: model)
                tableView.reloadRows(at: [indexPath], with: .none)
                }
            } else{
                self.askToLoginAlert()
            }
      
        }
        
        cell.onShareBtnClicked { (indexPath) in
             self.shareAppUrl()
        }
        cell.onViewProfileBtnClicked { (indexPath) in
            let model = self.arrList[indexPath.row] as! FeedsModel
            // Push Profile Controller
            if model.posted_by_user_type == "SocialPage"{
                
                let socialPageVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "SocialPageController") as! SocialPageController
                socialPageVC.strPostedById = model.posted_by_user_id
                self.navigationController?.pushViewController(socialPageVC, animated: true)
                
            } else  if model.posted_by_user_type == "Nutritionist"{
                
                let nutriProfileVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NutritionistProfileController") as! NutritionistProfileController
                nutriProfileVC.strPostedUserId = model.posted_by_user_id
                self.navigationController?.pushViewController(nutriProfileVC, animated: true)
            }
            
            
            
        }
        
        cell.btnView.setTitle("(" + formatNumber(Int(model.no_of_views)!) + ")", for: .normal)
        cell.btnLike.setTitle("(" + model.no_of_likes + ")", for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrList[indexPath.row] as! FeedsModel
        if model.type == FEED_TYPE_YOUTUBE {
            if model.url != nil {
                playVideo(videoURL: model.url)
            }
            else {
                VIEWMANAGER.showToast("Invalid URL")
            }
        }
        
        else if model.type ==  FEED_TYPE_WEBLINK {
            if model.url != nil{
               pushToWebViewController(url: model.url)
            }
        }
        else if model.type ==  FEED_TYPE_BANNER {
            showImageViewerController(url: MEDIA_URL + model.imageUrl)
        }
    }
    
    func pushToWebViewController(url : String) {
        let webVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.url = url
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func showImageViewerController(url : String) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = url
        imgViewerVC.modalTransitionStyle = .crossDissolve
       // self.present(imgViewerVC, animated: true, completion: nil)
        
        self.transitioningDelegate = RZTransitionsManager.shared()
        let nextViewController = UIViewController()
        nextViewController.transitioningDelegate = RZTransitionsManager.shared()
        self.present(imgViewerVC, animated:true) {}
    }
    
    func playVideo(videoURL : String)
    {
        let arr = videoURL.components(separatedBy: "watch?v=")
        if arr.count == 2 {
            let videoView = VideoPlayerView(frame: UIScreen.main.bounds, videoURL: videoURL)
            UIApplication.shared.keyWindow?.addSubview(videoView)
        }
        else{
            VIEWMANAGER.showToast("Invalid video url")
        }
    }
    
    
    // Like Api
    func likeFeedApi(model : FeedsModel) {
        
        let param = [
            "feed_id" : model.id,
            "user_id" : 4
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: GET_FEED_LIKES_API, params: param as [String : AnyObject], completion: { (response,message) in
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    // UNLIKE API
    
    func UNlikeFeedApi(model : FeedsModel) {
        
        let param = [
            "feed_id" : model.id,
            "user_id" : 4
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: GET_FEED_UNLIKE_API, params: param as [String : AnyObject], completion: { (response,message) in
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    // view feed increment api
    func viewFeedIncrementApi(model : FeedsModel) {

        let param = [
            "feed_id" : model.id,
            "user_id" : VIEWMANAGER.currentUser!.id
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: GET_FEED_INCREMENT_VIEW_API, params: param as [String : AnyObject], completion: { (response) in
            
            VIEWMANAGER.hideActivityIndicator()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func shareAppUrl(){
        
        let shareAll = ["Download FitnessMaa(Technology & Marketing Partner For Fitness Centres) Link:\n \(APP_STORE_URL)"]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
   
    func formatNumber(_ n: Int) -> String {

        let num = abs(Double(n))
        let sign = (n < 0) ? "-" : ""

        switch num {

        case 1_000_000_000...:
            var formatted = num / 1_000_000_000
            formatted = formatted.truncate(places: 1)
            return "\(sign)\(formatted)B"

        case 1_000_000...:
            var formatted = num / 1_000_000
            formatted = formatted.truncate(places: 1)
            return "\(sign)\(formatted)M"

        case 1_000...:
            var formatted = num / 1_000
            formatted = formatted.truncate(places: 1)
            return "\(sign)\(formatted)K"

        case 0...:
            return "\(n)"

        default:
            return "\(sign)\(n)"

        }

    }
    
    
    func askToLoginAlert(){
        CustomAlertView.showAlertwithLogin(withTitle: "Login Alert", messsage: "You are not login, Please login first", completion: {
            let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        })
    }
    
}

extension Double {
    func truncate(places: Int) -> Double {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}


