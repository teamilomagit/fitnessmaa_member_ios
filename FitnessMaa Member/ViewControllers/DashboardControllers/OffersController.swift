//
//  OffersController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 24/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import SDWebImage
import ASJCollectionViewFillLayout

class OffersController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
   
    
    @IBOutlet weak var BannerView: BannersListView!
    @IBOutlet weak var tblViewTodayOffer: UITableView!
    @IBOutlet weak var collectionViewOtherOffer: UICollectionView!
    @IBOutlet weak var baseScroll: UIScrollView!
    @IBOutlet weak var tblViewHtConstr: NSLayoutConstraint!
    @IBOutlet weak var btnTodaysOffer: UIButton!
    @IBOutlet weak var btnOthersOffer: UIButton!
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tblHeaderView: UIView!
    @IBOutlet weak var collectionHeaderView: UIView!
    
    var arrBanners = NSArray()
    var pageControl : UIPageControl!
    var currentOffset : CGFloat!
    let arrTopOffers = NSMutableArray()
    var arrCityOffer = NSMutableArray()
    var arrNearByOffer = NSMutableArray()


    let arrProducts = NSMutableArray()
    var arrDealsOfTheDay = NSArray()
    var refreshControl : UIRefreshControl!
    var currentIdx : Int!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

   // var arrImages = ["img1","img2","img3","img4","img5","img6"]
    override func viewDidLoad() {
        super.viewDidLoad()
        //btnNotificationShow()
        getOffersService()
        setupCollectionView()

        tblViewTodayOffer.estimatedRowHeight = 250
        tblViewTodayOffer.rowHeight = UITableViewAutomaticDimension
        BannerView.layer.cornerRadius = 10
        BannerView.layer.borderWidth = 0.1
        BannerView.layer.borderColor = UIColor.lightGray.cgColor
        //BannerView.clipsToBounds = true
        btnOthersOffer.layer.masksToBounds = true
        btnOthersOffer.layer.cornerRadius = 10
        btnTodaysOffer.layer.masksToBounds = true
        btnTodaysOffer.layer.cornerRadius = 10

        tblViewTodayOffer.register(UINib(nibName: "TodaysOffersCell", bundle: nil), forCellReuseIdentifier: "TodaysOffersCell")
        tblViewTodayOffer.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        collectionViewOtherOffer.register(UINib(nibName: "OtherOffersCollectionCell", bundle: nil), forCellWithReuseIdentifier: "OtherOffersCollectionCell")

        setupNavigationBar()
      tblHeaderView.isHidden = true
        collectionHeaderView.isHidden = true
        BannerView.onSelectImage { (Int) in
            let offerDetailVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "OfferDetailController") as! OfferDetailController
            self.navigationController?.pushViewController(offerDetailVC, animated: true)
        }

    }
    
    func setupNavigationBar() {
        let navBar = Bundle.main.loadNibNamed("LocationWithNotificationView", owner: self, options: nil)?[0] as! LocationWithNotificationView
        navView.addSubview(navBar)
        navBar.enableAutoLayout()
        navBar.leadingMargin(pixels: 0)
        navBar.trailingMargin(pixels: 0)
        navBar.topMargin(pixels: 0)
        navBar.bottomMargin(pixels: 0)
        
        if Preferences.getGeneralSettingModelData() != nil{
            let model  :  GeneralSettingModel
            model = Preferences.getGeneralSettingModelData()!
            navBar.lblCityName.text = model.defualtCityName
        }
        else{
            if Preferences.getUserCurrentCity() != nil{
                let userLocationModel = Preferences.getUserCurrentCity()
                navBar.lblCityName.text = userLocationModel?.cityName
            }
        }
        
        
        navBar.onNotificationClicked {
            
            let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificaitonVC, animated: true)
        }
        navBar.onLocationClicked {
            
            let cityVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "CitySelectionController") as! CitySelectionController
            self.navigationController?.present(cityVC, animated: true, completion: nil)
            cityVC.onLocationSaveClicked {str in
                navBar.lblCityName.text = str
            }
        }
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHtConstr.constant = tblViewTodayOffer.contentSize.height
    }
    
    func getOffersService() {
        
        let param = [
            "city_id":"2"
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_OFFERS_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! OffersModel
            
            if model.cityOffers != nil{
                self.tblHeaderView.isHidden = false
            }
            if model.nearbyOffers != nil{
                self.collectionHeaderView.isHidden = false
            }
            for str in model.topOffers{
                self.arrTopOffers.add(str)
            }
            for str in model.cityOffers{
                self.arrCityOffer.add(str)
                
            }
            for str in model.nearbyOffers{
                self.arrNearByOffer.add(str)
            }
            
            VIEWMANAGER.hideActivityIndicator()
            self.BannerView.bannerData = self.arrTopOffers as NSArray
            self.collectionViewOtherOffer.reloadData()
            self.tblViewTodayOffer.reloadData()
            VIEWMANAGER.hideActivityIndicator()
           
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getOffersService()
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCityOffer.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodaysOffersCell") as! TodaysOffersCell
        let model = arrCityOffer[indexPath.row] as! CityOffer
         cell.imgViewTodaysOffer.sd_setImage(with: URL(string:MEDIA_URL + model.image), placeholderImage: UIImage(named:"placeholder_img"))
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let offerDetailVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "OfferDetailController") as! OfferDetailController
        self.navigationController?.pushViewController(offerDetailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    
    func setupCollectionView()
    {
        let layout = ASJCollectionViewFillLayout()
        //layout.numberOfItemsInSide = noOfItems
        layout.itemSpacing = 10
        layout.stretchesLastItems = false
        layout.itemLength = SCREEN_WIDTH * 0.7
        layout.direction = .horizontal
        collectionViewOtherOffer.setCollectionViewLayout(layout, animated: true)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNearByOffer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : OtherOffersCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherOffersCollectionCell", for: indexPath) as! OtherOffersCollectionCell
                let model = self.arrNearByOffer[indexPath.row] as! NearbyOffer
                cell.lblTitleOther.text = model.title
                cell.imgViewOthersOffer.sd_setImage(with: URL(string: MEDIA_URL + model.image), placeholderImage: UIImage(named:"placeholder_img"))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
    deinit {
        tblViewTodayOffer.removeObserver(self, forKeyPath: "contentSize")
    }

    @IBAction func btnTodaysOfferClicked(_ sender: Any) {
        let moreOfferVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "MoreOfferController") as! MoreOfferController
        self.navigationController?.pushViewController(moreOfferVC, animated: true)
    }
    @IBAction func btnOthersOfferClicked(_ sender: Any) {
        let moreOfferVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "MoreOfferController") as! MoreOfferController
        self.navigationController?.pushViewController(moreOfferVC, animated: true)
    }
}
