//
//  DietController.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 31/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class DietController: BaseViewController , UITableViewDelegate , UITableViewDataSource {
    
    let FEED_TYPE_BANNER = "BANNER"
    let FEED_TYPE_YOUTUBE = "YOUTUBE"
    let FEED_TYPE_WEBLINK = "WEBLINK"
    
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var dietFilterView: FeedsFilterView!
    @IBOutlet weak var tblDiet: UITableView!
    
    var arrList = NSMutableArray()
    var pageNo = 1
    var loadStarted : Bool = false
    var refreshControl = UIRefreshControl()
 //   var arrRecipe = NSMutableArray()
     var tagType = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tagType = "All"

        tblDiet.register(UINib(nibName: "FeedsTableCell", bundle: nil), forCellReuseIdentifier: "FeedsTableCell")
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblDiet.addSubview(refreshControl)
        getLatestLatestRecipeAPI(pageNo: pageNo)
        setupNavigationBar()
        
        self.dietFilterView.onFilterChange { (filter) in
            self.tagType = filter!
            self.getLatestLatestRecipeAPI(pageNo: self.pageNo)
        }
    }
    
    
    
    func setupNavigationBar() {
        let navBar = Bundle.main.loadNibNamed("LocationWithNotificationView", owner: self, options: nil)?[0] as! LocationWithNotificationView
        navView.addSubview(navBar)
        navBar.enableAutoLayout()
        navBar.leadingMargin(pixels: 0)
        navBar.trailingMargin(pixels: 0)
        navBar.topMargin(pixels: 0)
        navBar.bottomMargin(pixels: 0)
        
        if Preferences.getGeneralSettingModelData() != nil{
            let model  :  GeneralSettingModel
            model = Preferences.getGeneralSettingModelData()!
            navBar.lblCityName.text = model.defualtCityName
        }
        else{
            if Preferences.getUserCurrentCity() != nil{
                let userLocationModel = Preferences.getUserCurrentCity()
                navBar.lblCityName.text = userLocationModel?.cityName
            }
        }
        
        navBar.onNotificationClicked {
            
            let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificaitonVC, animated: true)
        }
        navBar.onLocationClicked {
            
            let cityVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "CitySelectionController") as! CitySelectionController
            self.navigationController?.present(cityVC, animated: true, completion: nil)
            cityVC.onLocationSaveClicked {str in
                navBar.lblCityName.text = str
            }
        }
    }
    
    @objc func pullToRefresh() {
        pageNo = 1
        self.getLatestLatestRecipeAPI(pageNo: pageNo,showLoader: false, isRefresh: true)
    }
 
    
    func getLatestLatestRecipeAPI(pageNo:Int,showLoader : Bool = true,isRefresh:Bool = false) {

        let param = [
            "user_id":4,
            "tags":tagType,
            "country_id":101
            
            ] as [String : Any]
        if showLoader{
            VIEWMANAGER.showActivityIndicator()
        }else{
            VIEWMANAGER.hideActivityIndicator()
        }
        
        RemoteAPI().callPOSTApi(apiName: GET_LATEST_RECIPE_API, params: param as [String : AnyObject], completion: { (feeds,tags,message) in
            
            VIEWMANAGER.hideActivityIndicator()
            
            self.refreshControl.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = feeds as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            
            self.dietFilterView.updateFilter(tagArray: tags as! NSArray)
            
            self.arrList.addObjects(from: arr as! [Any])
            self.tblDiet.backgroundView =  self.arrList.count == 0 ? self.tblBGView() : nil
            self.tblDiet.reloadData()
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            self.refreshControl.endRefreshing()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getLatestLatestRecipeAPI(pageNo: pageNo)
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedsTableCell") as! FeedsTableCell
        cell.indexPath = indexPath
        let model = arrList[indexPath.row] as! Recipe
        cell.lblTitle.text = model.title
        cell.imgViewBanner.sd_setImage(with: URL(string:model.imageUrl!)!, placeholderImage: UIImage())
        
        cell.btnLike.tintColor = model.liked_by_me != nil ? UIColor.appDarkThemeColor : UIColor.gray
        
        cell.onPlayVideoClicked { (indexPath) in
            let model = self.arrList[indexPath.row] as! Recipe
            self.viewRecipeIncrementApi(model: model)
            var count = Int(model.noOfViews)!
            count = count + 1
            model.noOfViews = "\(count)"
            self.arrList.replaceObject(at: indexPath.row, with: model)
            tableView.reloadRows(at: [indexPath], with: .none)
            if model.videoUrl != nil{
                self.playVideo(videoURL: model.videoUrl)
            }
            
        }
        
        cell.onLikeBtnClicked { (indexPath) in
            
            if VIEWMANAGER.currentUser != nil{
            
            if model.liked_by_me == nil{
                let model = self.arrList[indexPath.row] as! Recipe
                if model.liked_by_me == nil {
                    self.likeRecipeApi(model: model)
                    model.liked_by_me = VIEWMANAGER.currentUser?.id
                    var count = Int(model.noOfLikes)!
                    count = count + 1
                    model.noOfLikes = "\(count)"
                    self.arrList.replaceObject(at: indexPath.row, with: model)
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                
            }else{
                
                self.unlikeRecipeApi(model: model)
                model.liked_by_me = nil
                var count = Int(model.noOfLikes)!
                count = count - 1
                model.noOfLikes = "\(count)"
                self.arrList.replaceObject(at: indexPath.row, with: model)
                tableView.reloadRows(at: [indexPath], with: .none)
            }
           
            } else{
                self.askToLoginAlert()
            }

        }
        
        cell.onShareBtnClicked { (indexPath) in
            self.shareAppUrl()
        }
        
        cell.onViewProfileBtnClicked { (indexPath) in
            // Push Profile Controller
            VIEWMANAGER.showToast("profile")
        }
        
        
       // self.viewFeedCountApi(model: model)
  
        cell.btnView.setTitle("(" + model.noOfViews + ")", for: .normal)
        cell.btnLike.setTitle("(" + model.noOfLikes + ")", for: .normal)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrList[indexPath.row] as! Recipe
//        if model.type == FEED_TYPE_YOUTUBE {
//
//        }
//
//        else if model.type ==  FEED_TYPE_WEBLINK {
//            if model.url != nil{
//                pushToWebViewController(url: model.url)
//            }
//        }
//        else if model.type ==  FEED_TYPE_BANNER {
//            showImageViewerController(url: MEDIA_URL + model.imageUrl)
//        }
        
        playVideo(videoURL: model.videoUrl)
    }
    
    func pushToWebViewController(url : String) {
        let webVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        webVC.url = url
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func showImageViewerController(url : String) {
        let imgViewerVC = ImageViewerController()
        imgViewerVC.imgURL = url
        imgViewerVC.modalTransitionStyle = .crossDissolve
        self.present(imgViewerVC, animated: true, completion: nil)
    }
    
    func playVideo(videoURL : String)
    {
        let arr = videoURL.components(separatedBy: "watch?v=")
        if arr.count == 2 {
            let videoView = VideoPlayerView(frame: UIScreen.main.bounds, videoURL: videoURL)
            UIApplication.shared.keyWindow?.addSubview(videoView)
        }
        else{
            VIEWMANAGER.showToast("Invalid video url")
        }
    }
    
    
    // Like Api
    func likeRecipeApi(model : Recipe) {
        
        let param = [
            "recipe_id" : model.id,
            "user_id": 4
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: LIKE_RECIPE_API, params: param as [String : AnyObject], completion: { (response) in
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    // Like Api
    func unlikeRecipeApi(model : Recipe) {
        
        let param = [
            "recipe_id" : model.id,
            "user_id": 4
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: UNLIKE_RECIPE_API, params: param as [String : AnyObject], completion: { (response,message) in
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    // view recipe increment api
    func viewRecipeIncrementApi(model : Recipe) {
        
        let param = [
           "recipe_id":model.id,
           "user_id":VIEWMANAGER.currentUser?.id
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: GET_RECIPE_INCREMENT_VIEW_API, params: param as [String : AnyObject], completion: { (response,message) in
            VIEWMANAGER.showToast(message)
        }) { (errMsg) in
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func shareAppUrl(){
        
        let shareAll = ["Download FitnessMaa(Technology & Marketing Partner For Fitness Centres) Link:\n \(APP_STORE_URL)"]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func askToLoginAlert(){
        CustomAlertView.showAlertwithLogin(withTitle: "Login Alert", messsage: "You are not login, Please login first ", completion: {
            let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated: true)
        })
    }
  
}
