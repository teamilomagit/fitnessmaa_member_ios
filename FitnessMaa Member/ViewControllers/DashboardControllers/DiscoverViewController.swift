//
//  DiscoverViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit
import ASJCollectionViewFillLayout
class DiscoverViewController: BaseViewController , UICollectionViewDelegate,UICollectionViewDataSource {
    
    

    @IBOutlet weak var bannerListView: BannersListView!
    @IBOutlet weak var navView: UIView!
     let arrTopOffers = NSMutableArray()
    
    var arrTitles : [String]!
    var arrImages : [String]!
    
    @IBOutlet weak var DashCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        bannerListView.layer.cornerRadius = 10
        bannerListView.layer.borderWidth = 0.1
        bannerListView.layer.borderColor = UIColor.lightGray.cgColor
        
        arrTitles = ["Gyms","Zumba","Dance","Aerobic","Karate","Yoga"]
        arrImages = ["ic_gym","ic_zumba","ic_dance","ic_aerobic","ic_karate","ic_yoga"]
        
        bannerListView.onSelectImage { (Int) in
            let offerDetailVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "OfferDetailController") as! OfferDetailController
            self.navigationController?.pushViewController(offerDetailVC, animated: true)
        }
        
        
        let layout = ASJCollectionViewFillLayout()
        layout.numberOfItemsInSide = 3
        layout.direction = .vertical;
        layout.itemLength = 100;
        layout.stretchesLastItems = false
        
        DashCollectionView.setCollectionViewLayout(layout, animated: true)
        DashCollectionView.register(UINib(nibName: "DiscoverCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverCollectionViewCell")
        
        
        getBannerService()
        
    }
    
    func setupNavigationBar() {
        let navBar = Bundle.main.loadNibNamed("LocationWithNotificationView", owner: self, options: nil)?[0] as! LocationWithNotificationView
        navView.addSubview(navBar)
        navBar.enableAutoLayout()
        navBar.leadingMargin(pixels: 0)
        navBar.trailingMargin(pixels: 0)
        navBar.topMargin(pixels: 0)
        navBar.bottomMargin(pixels: 0)
        
    
        if Preferences.getGeneralSettingModelData() != nil{
        let model  :  GeneralSettingModel
        model = Preferences.getGeneralSettingModelData()!
        navBar.lblCityName.text = model.defualtCityName
        }
        else{
        if Preferences.getUserCurrentCity() != nil{
            let userLocationModel = Preferences.getUserCurrentCity()
            navBar.lblCityName.text = userLocationModel?.cityName
            }
        }
       
        navBar.onNotificationClicked {
            let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificaitonVC, animated: true)
        }
        navBar.onLocationClicked {
            
            let cityVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "CitySelectionController") as! CitySelectionController
            self.navigationController?.present(cityVC, animated: true, completion: nil)
            cityVC.onLocationSaveClicked {str in
                navBar.lblCityName.text = str
            }
        }
    }
    
    func getBannerService() {
        
        let param = [
            "city_id":"2"
        ]
        RemoteAPI().callPOSTApi(apiName: GET_OFFERS_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! OffersModel
            
          
            for str in model.topOffers{
                self.arrTopOffers.add(str)
            }
           
           self.bannerListView.bannerData = self.arrTopOffers as NSArray
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : DiscoverCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverCollectionViewCell", for: indexPath) as! DiscoverCollectionViewCell

        cell.lblTitle.text = arrTitles[indexPath.row]
        cell.lblTitle.font = UIFont.appMediumFont(size: 16)
        cell.imgType.image = UIImage(named: arrImages[indexPath.row])

        return cell
    }
}
