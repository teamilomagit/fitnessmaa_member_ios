//
//  ReferalViewController.swift
//  FitnessMaa Member
//
//  Created by Sanjana on 7/2/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class ReferalViewController: UIViewController {

   
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var referLbl: UILabel!
   
    @IBOutlet weak var codeLbl: UILabel!
    
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var viewBckBtnShare: MaterialView!
    override func viewDidLoad() {
        super.viewDidLoad()
    lblTitle.font = UIFont.appBoldFont(size: 17)
    referLbl.font = UIFont.appMediumFont(size: 15)
    codeLbl.layer.masksToBounds = true
    codeLbl.layer.cornerRadius = 8
    codeLbl.text = VIEWMANAGER.currentUser?.refarralCode!
       viewBckBtnShare.layer.cornerRadius = viewBckBtnShare.bounds.height/2
      
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnShare(_ sender: UIButton) {
        let text = "FitnessMaa - Technology & Marketing partner for fitness centers. Receive daily nutrition and health tips, yummy healthy recipes, offers in your city, upcoming events and track your payments, attendance and membership details with FitnessMaa connected centers) Link:\n \(APP_STORE_URL)"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
