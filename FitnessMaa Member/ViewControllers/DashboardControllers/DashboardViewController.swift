//
//  DashboardViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 10/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class DashboardViewController: UITabBarController,UITabBarControllerDelegate {

    var discoverVC : DiscoverViewController!
    var feedsVC : FeedsViewController!
    var offersVC : OffersController!
    var myProfileVC : MyProfileViewController!
    var dietVC : DietController!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupCustomTabbar()
        setupFrontCustomTabbar()
        setupTabbarControllers()
        NotificationCenter.default.addObserver(self, selector: #selector(drawerSelection(notification:)), name: NSNotification.Name(rawValue: DRAWER_SELECTION_NOTIFICATION), object: nil)
        
        checkAppUpdateApi()
    }
    
    func setupTabbarControllers() {
        discoverVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "DiscoverViewController") as? DiscoverViewController
        feedsVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "FeedsViewController") as? FeedsViewController
        offersVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "OffersController") as? OffersController
        myProfileVC = PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
        
        dietVC =  PAWAN_STORYBOARD.instantiateViewController(withIdentifier: "DietController") as? DietController
        
        self.viewControllers = [discoverVC,feedsVC,offersVC,dietVC, myProfileVC]
    }
    
        func setupFrontCustomTabbar() {
            let tabbar = Bundle.main.loadNibNamed("FrontTabBarView", owner: self, options: nil)?[0] as! FrontTabBarView
            self.view.addSubview(tabbar)
            tabbar.enableAutoLayout()
            tabbar.leadingMargin(pixels: 0)
            tabbar.trailingMargin(pixels: 0)
            tabbar.bottomMargin(pixels: 0)
            tabbar.fixedHeight(pixels: 70)
            tabbar.onFrontTabClicked { (tag) in
                self.selectedIndex = tag - 1

            }

        }
    
    @objc func drawerSelection(notification:NSNotification) {
        let selIndex = notification.object as! NSInteger
        switch selIndex {
        case 0:
            pushToProfileViewController()
            break
        case 1:
            showBMICalculatorView()
            break
        case 2:
            pushToExerciseViewController()
            break
        case 3:
            pushToFeedbackController()
            break
        case 4:
            performLogout()
            break
        default:
            break
        }
    }
    
    func pushToProfileViewController() {
        let profileVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    func pushToExerciseViewController() {
        let profileVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ExercisePlanController") as! ExercisePlanController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func pushToFeedbackController(){
        let feedbackVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "FeedbackController") as! FeedbackController
        self.navigationController?.pushViewController(feedbackVC, animated: true)
    }
    func showBMICalculatorView() {
        let bmiCalView = Bundle.main.loadNibNamed("BMICalculatorView", owner: self, options: nil)?[0] as! BMICalculatorView
        self.view.addSubview(bmiCalView)
        
        bmiCalView.enableAutoLayout()
        bmiCalView.leadingMargin(pixels: 0)
        bmiCalView.trailingMargin(pixels: 0)
        bmiCalView.topMargin(pixels: 0)
        bmiCalView.bottomMargin(pixels: 0)
        
    }
    
    func performLogout(){
        CustomAlertView.showAlertWithYesNo(title: "", messsage: "Are you sure you want to logout?") {
            app_del.setLoginAsRoot()
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func checkAppUpdateApi() {
        
        let param = [
            "device_type": "iOS"
   
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: CHECK_APP_UPDATE_API, params: param as [String : AnyObject], completion: { (response) in
            let updateModel = response as! AppUpdateModel
            VIEWMANAGER.hideActivityIndicator()
            self.updatepopupStatus(updateModel: updateModel)
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.checkAppUpdateApi()
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
        }
    }
    
    func updatepopupStatus(updateModel : AppUpdateModel){
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let current = appVersion
        let appStore = updateModel.versionName
        let versionCompare = current!.compare(appStore!, options: .numeric)
        
         if versionCompare == .orderedAscending {
            
            if updateModel.showPopup != nil && updateModel.showPopup {
                VIEWMANAGER.showUpdateAlert(isOptional: updateModel.isOptional)
            }
        }
    }
    
   

}


