//
//  MyProfileViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 25/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class MyProfileViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var viewNoData: MaterialView!
    var cellToReturn = UITableViewCell()

    @IBOutlet weak var navView: UIView!
    @IBOutlet  var tblViewMenu: UITableView!
    @IBOutlet weak var tblViewHtConstr: NSLayoutConstraint!
    @IBOutlet weak var tblViewCentreList: UITableView!
    @IBOutlet weak var tblviewCentreHtConstr: NSLayoutConstraint!
    @IBOutlet weak var btnAdCen: UIButton!
    @IBOutlet weak var bottomTableTopConstr: NSLayoutConstraint!
    @IBOutlet weak var baseView: UIView!
    
    @IBOutlet weak var viewNoDateHtContr: NSLayoutConstraint!
    var arrMenuTitle = ["My Profile","Feedback & Complaint","Contact us","Change Password", "Share App","Refferal & Rewards"]
    var arrMenuImages = ["ic_my_profile","ic_feedback","ic_contact","ic_setting","ic_share","ic_referal_reward"]
    
    var arrCentreList = NSMutableArray()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getConnectedCenterList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableViewMenu()
        setupTableViewCentre()
        setupNavigationBar()
        if VIEWMANAGER.currentUser?.id != nil{
            arrMenuTitle = ["My Profile","Feedback & Complaint","Contact us","Change Password", "Share App","Refferal & Rewards","Logout"]
            arrMenuImages = ["ic_my_profile","ic_feedback","ic_contact","ic_setting","ic_share","ic_share","ic_logout"]
        }
       
        toggleCenterlistVisibility()

    }
    
    func setupNavigationBar() {
        let navBar = Bundle.main.loadNibNamed("LocationWithNotificationView", owner: self, options: nil)?[0] as! LocationWithNotificationView
        navView.addSubview(navBar)
        navBar.enableAutoLayout()
        navBar.leadingMargin(pixels: 0)
        navBar.trailingMargin(pixels: 0)
        navBar.topMargin(pixels: 0)
        navBar.bottomMargin(pixels: 0)
        
        if Preferences.getGeneralSettingModelData() != nil{
            let model  :  GeneralSettingModel
            model = Preferences.getGeneralSettingModelData()!
            navBar.lblCityName.text = model.defualtCityName
        }
        else{
            if Preferences.getUserCurrentCity() != nil{
                let userLocationModel = Preferences.getUserCurrentCity()
                navBar.lblCityName.text = userLocationModel?.cityName
            }
        }
        
        
        navBar.onNotificationClicked {
            
            let notificaitonVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(notificaitonVC, animated: true)
        }
        navBar.onLocationClicked {
            
            let cityVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "CitySelectionController") as! CitySelectionController
            self.navigationController?.present(cityVC, animated: true, completion: nil)
            cityVC.onLocationSaveClicked {str in
                navBar.lblCityName.text = str
            }
        }
    }
    
    
    func toggleCenterlistVisibility(){
        
        self.tblViewCentreList.reloadData()
        if self.arrCentreList.count > 0 {
            self.viewNoData.isHidden = true
            self.tblViewCentreList.isHidden = false
            let topConstr = NSLayoutConstraint(item: self.tblViewMenu, attribute: .top, relatedBy: .equal, toItem: self.tblViewCentreList, attribute: .bottom, multiplier: 1.0, constant: 150)
            self.baseView?.addConstraint(topConstr)
        }else{
            let topConstr = NSLayoutConstraint(item: self.tblViewMenu, attribute: .top, relatedBy: .equal, toItem: self.viewNoData, attribute: .bottom, multiplier: 1.0, constant: 150)
            self.baseView?.addConstraint(topConstr)
            self.viewNoData.isHidden = false
            self.tblViewCentreList.isHidden = true
        }
    }



    @IBAction func btnAddCentreClicked(_ sender: Any) {
    //  showAddCentreView()
    }
    func showAddCentreView()
    {
        let viewAddCentre = Bundle.main.loadNibNamed("AddCentreView", owner: self, options: [:])?.first as! AddCentreView
        let window = UIApplication.shared.keyWindow!
        viewAddCentre.frame = window.bounds
        // viewAddPkg.packageModel = model
        window.addSubview(viewAddCentre)
        
        viewAddCentre.onAddCentreSuccess { (model) in
            
            self.arrCentreList.add(model)
            
            self.tblViewCentreList.reloadData()
            if self.arrCentreList.count > 0 {
                self.viewNoData.isHidden = true
                self.tblViewCentreList.isHidden = false
                let topConstr = NSLayoutConstraint(item: self.tblViewMenu, attribute: .top, relatedBy: .equal, toItem: self.tblViewCentreList, attribute: .bottom, multiplier: 1.0, constant: 150)
                self.baseView?.addConstraint(topConstr)
            }else{
                let topConstr = NSLayoutConstraint(item: self.tblViewMenu, attribute: .top, relatedBy: .equal, toItem: self.viewNoData, attribute: .bottom, multiplier: 1.0, constant: 150)
                self.baseView?.addConstraint(topConstr)
                self.viewNoData.isHidden = false
                self.tblViewCentreList.isHidden = true
            }
        }
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblViewHtConstr.constant = tblViewMenu.contentSize.height
        tblviewCentreHtConstr.constant = tblViewCentreList.contentSize.height

    }
    
    func setupTableViewMenu()
    {
        tblViewMenu.layer.borderColor = UIColor.gray.cgColor
        tblViewMenu.layer.borderWidth = 1.0
        tblViewMenu.rowHeight = UITableViewAutomaticDimension
         tblViewMenu.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        let nib = UINib(nibName: "MyProfileMenuCell", bundle: nil)
        tblViewMenu.register(nib, forCellReuseIdentifier: "MyProfileMenuCell")
        tblViewCentreList.layer.cornerRadius = 5
        tblViewCentreList.layer.masksToBounds = true
        tblViewCentreList.layer.shadowColor = UIColor.lightGray.cgColor
        tblViewCentreList.layer.shadowOffset =  CGSize(width: -10, height: 10)
        tblViewCentreList.layer.shadowOpacity = 1.0
    }
    func setupTableViewCentre()
    {
        tblViewCentreList.layer.borderColor = UIColor.gray.cgColor
        tblViewCentreList.layer.borderWidth = 1.0
        tblViewCentreList.rowHeight = UITableViewAutomaticDimension
        tblViewCentreList.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        let nib = UINib(nibName: "AddCentreCell", bundle: nil)
        tblViewCentreList.register(nib, forCellReuseIdentifier: "AddCentreCell")
        tblViewMenu.layer.cornerRadius = 5
        tblViewMenu.layer.masksToBounds = true
        tblViewMenu.layer.shadowColor = UIColor.lightGray.cgColor
        tblViewMenu.layer.shadowOffset =  CGSize(width: -10, height: 10)
        tblViewMenu.layer.shadowOpacity = 1.0
     }
 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // var arr = NSMutableArray()
        if tableView == tblViewMenu{
//            for str in arrMenuTitle{
//                arr.add(str)
//            }
            
            return arrMenuTitle.count
            
        }else{
           // arr = arrCentreList
            return arrCentreList.count
       }
//        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView == tblViewMenu) {
           let cell = tableView.dequeueReusableCell(withIdentifier: "MyProfileMenuCell") as! MyProfileMenuCell
            cell.selectionStyle = .none
            cell.indexPath = indexPath
            cell.lblMenu.text! = arrMenuTitle[indexPath.row]
            cell.imgViewMenu.image = UIImage(named: arrMenuImages[indexPath.row])
            cellToReturn = cell
        }
            
        else if (tableView == tblViewCentreList) {
            let  cell = tableView.dequeueReusableCell(withIdentifier: "AddCentreCell") as! AddCentreCell
             let model = arrCentreList[indexPath.row] as! ConnectedCenterModel
            cell.selectionStyle = .none
            cell.lblName.text = model.gym.gymName
            cell.lblAddress.text = model.gym.address
            cell.lblCenterId.text = model.centerId
            cellToReturn =  cell
        }
        return cellToReturn
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var tblHeight = Int()
        if tableView == tblViewMenu{
            tblHeight = 60
        }else{
            tblHeight = 110

        }
        return CGFloat(tblHeight)
    }
    
    
    func getConnectedCenterList() {
        let param = [
            "user_id" : VIEWMANAGER.currentUser?.id
            ] as [String : Any]
        RemoteAPI().callPOSTApi(apiName: GET_CONNECTED_CENTER_API, params: param as [String : AnyObject], completion: { (responseData) in
            let arr = responseData as! NSArray
            self.arrCentreList.removeAllObjects()
            self.arrCentreList.addObjects(from: arr as! [Any])
          //  self.tblViewCentreList.reloadData()
            
            self.toggleCenterlistVisibility()
            
        }) { (errMsg) in
            if errMsg == NO_INTERNET{
                CustomAlertView.showAlertWithRetry(completion: {
                    self.getConnectedCenterList()
                })
            }else{
                VIEWMANAGER.showToast(errMsg!)
            }
        }
    }
    
    
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblViewMenu{
            profileOptionsEvent(indexPath: indexPath)
        }
    }
    
    func profileOptionsEvent(indexPath : IndexPath) {
        
        
        switch indexPath.row {
        case 0:
            if VIEWMANAGER.currentUser?.id != nil{
                pushToProfileViewController()
            }else{
                askToLoginAlert()
            }
            
            break
        case 1:
            if VIEWMANAGER.currentUser?.id != nil{
                pushToFeedbackController()
            }else{
                askToLoginAlert()
            }
            
            break
        case 2:
            pushToContactUsViewController()
            break
            //        case 3:
            //            // referal
            //            break
            
        case 3:
            //change pass
            if VIEWMANAGER.currentUser != nil{
                showChangePasswordView()
            }else{
                askToLoginAlert()
            }
            
            break
            
        case 4:
            // share app
            shareAppUrl()
            break
        case 5:
            if VIEWMANAGER.currentUser != nil{
                 pushToReferalViewController()
            }else{
                askToLoginAlert()
            }
           
            // referal controller
            break
            
        case 6:
            if VIEWMANAGER.currentUser?.id != nil{
                performLogout()
                break
            }else{
                askToLoginAlert()
            }
            break
        default:
            break
        }
    }
    func pushToReferalViewController() {
        let profileVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ReferalViewController") as! ReferalViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func pushToProfileViewController() {
        let profileVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    func pushToContactUsViewController() {
        let contactUsVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "ContactUsController") as! ContactUsController
        self.navigationController?.pushViewController(contactUsVC, animated: true)
    }
    
    func pushToFeedbackController(){
        let feedbackVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "FeedbackController") as! FeedbackController
        self.navigationController?.pushViewController(feedbackVC, animated: true)
    }
    func performLogout(){
        CustomAlertView.showAlertWithYesNo(title: "", messsage: "Are you sure you want to logout?") {
            Preferences.clearLoginResponse()
            VIEWMANAGER.currentUser = nil
            app_del.setDashboarAsRoot()
    
           NotificationCenter.default.removeObserver(self)
        }
    }
    
    func shareAppUrl(){
        
        let shareAll = ["FitnessMaa - Technology & Marketing partner for fitness centers. Receive daily nutrition and health tips, yummy healthy recipes, offers in your city, upcoming events and track your payments, attendance and membership details with FitnessMaa connected centers) Link:\n \(APP_STORE_URL)"]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    func showChangePasswordView()
    {
        let changePassView = Bundle.main.loadNibNamed("ChangePasswordView", owner: self, options: [:])?.first as! ChangePasswordView
        changePassView.frame = self.view.bounds
        self.view.addSubview(changePassView)
        
    }
    
    func askToLoginAlert(){
        CustomAlertView.showAlertwithLogin(withTitle: "Login Alert", messsage: "You are not login, Please login first", completion: {
            let loginVC = MAYUR_STORYBOARD.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            self.navigationController?.pushViewController(loginVC, animated: true)
        })
    }
    
    
    
    deinit {
        tblViewMenu.removeObserver(self, forKeyPath: "contentSize")
        tblViewCentreList.removeObserver(self, forKeyPath: "contentSize")
        
    }
}
