//
//  NotificationViewController.swift
//  FitnessMaa Member
//
//  Created by Pawan Ramteke on 11/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit


class NotificationViewController: BaseViewController,UITableViewDataSource,UITabBarDelegate {
    @IBOutlet weak var tblViewList: UITableView!
    var arrList = NSMutableArray()
    var pageNo = 1
    var loadStarted : Bool = false
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewList.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
       
        getNotificationList(pageNo: pageNo)
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tblViewList.addSubview(refreshControl)
    }
    @objc func pullToRefresh() {
        pageNo = 1
        self.getNotificationList(pageNo: pageNo,showLoader: false, isRefresh: true)
    }
    func getNotificationList(pageNo:Int,showLoader : Bool = true,isRefresh:Bool = false) {
        let param = [
            "page_no" : pageNo,
            "limit" : LIMIT,
            "user_id" : "9",
            "account_creation_datetime":"2019-05-25 09:31:47",
            ] as [String : Any]
        if showLoader {
            VIEWMANAGER.showActivityIndicator()
        }
        RemoteAPI().callPOSTApi(apiName: GET_NOTIFICATION_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.refreshControl.endRefreshing()
            if isRefresh {
                self.arrList.removeAllObjects()
            }
            let arr = responseData as! NSArray
            if arr.count == LIMIT {
                self.pageNo = self.pageNo + 1
                self.loadStarted = true
            }
            else{
                self.loadStarted = false
            }
            self.arrList.addObjects(from: arr as! [Any])
            self.tblViewList.backgroundView =  self.arrList.count == 0 ? self.tblBGView() : nil
            self.tblViewList.reloadData()
        }) { (errMsg) in
            self.refreshControl.endRefreshing()

            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as!  NotificationCell
        let model = arrList[indexPath.row] as! NotificationModel
        cell.lblMessage.text! = model.message
        cell.lblDate.text! =  Date.convertDateFormater(date: model.createdAt!)
        return cell
    }
    
    override func tblBGView() -> UILabel {
        let lblBG = UILabel(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        lblBG.font = UIFont.appRegularFont(size: 18)
        lblBG.text = "No data found"
        lblBG.textAlignment = .center
        return lblBG
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
