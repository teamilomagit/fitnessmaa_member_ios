//
//  TermsAndCondition.swift
//  FitnessMaa
//
//  Created by iLoma on 10/01/19.
//  Copyright © 2019 iLoma. All rights reserved.
//

import UIKit

class TermsAndCondition: BaseViewController , UIWebViewDelegate {
    
    @IBOutlet weak var viewBG: AppHeaderView!
    @IBOutlet weak var imgViewDP: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var webview: UIWebView!
     let activityView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var headerHeight : CGFloat = 80.0
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool = false
    var arrList = NSMutableArray()
    let screenWidth = SCREEN_WIDTH
    @IBOutlet weak var viewBGHtConstr: NSLayoutConstraint!
    @IBOutlet weak var txtView: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        
        lblTitle.font = UIFont.appMediumFont(size: 18)
        lblTitle.textColor = .white
        webview.delegate = self
        self.view.addSubview(activityView)
        activityView.center = self.view.center
        webViewDidStartLoad(webview)
        let url = URL (string: TERMS)
        let requestObj = URLRequest(url: url!)
        webview.loadRequest(requestObj)
        
        webViewDidFinishLoad(webview)
        
        addNavigationWithTitle(title: "Terms & Conditions")
        
        self.view.backgroundColor = UIColor.controllerBGColor

    }

    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 
    func webViewDidStartLoad(_ webView: UIWebView){
        // show indicator
 activityView.startAnimating()
  
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        // hide indicator
        activityView.stopAnimating()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        // hide indicator
        VIEWMANAGER.hideActivityIndicator()
    }
    

}
