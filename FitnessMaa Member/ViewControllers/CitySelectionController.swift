//
//  CitySelectionController.swift
//  FitnessMaa Member
//
//  Created by iLoma on 23/05/19.
//  Copyright © 2019 Pawan Ramteke. All rights reserved.
//

import UIKit

class CitySelectionController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var tbtViewCities: UITableView!
    
    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnState: UIButton!
    @IBOutlet weak var btnCity: UIButton!
    var arrList = NSMutableArray()
    var dropDownData : [String]!
    var arrRecentArray = NSMutableArray()
    
    var valueToPass:String!

    
    
 var locationSendClosure : ((String) -> ())?
    

    
    let LIMIT = 10
    var pageNo = 1
    var loadStarted : Bool = false
    var strMemberId : String!
    var refreshControl = UIRefreshControl()
    
    var countryData : [CountryModel]!
    var selCountryId : String!
    
    var stateData : [StateModel]!
    var selStateId : String!
    
    var cityData : [CityModel]!
    var selCityId : String!
    
    var userLocationModel = UserPreferedLocationModel.init(fromDictionary: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        if Preferences.getRecentSearchLocationList() != nil{
            arrRecentArray = Preferences.getRecentSearchLocationList()
        }
        
        if Preferences.getUserCurrentCity() != nil{
        let model = Preferences.getUserCurrentCity()
            lblCity.text = model?.cityName
            lblState.text = model?.stateName
            lblCountry.text = model?.countryName
        }
        else {
        let generalSettingModel : GeneralSettingModel
        generalSettingModel = Preferences.getGeneralSettingModelData()!
        lblCity.text = generalSettingModel.defualtCityName
        lblState.text = generalSettingModel.defualtStateName
        lblCountry.text = generalSettingModel.defualtCountryName

        }
    }
    
  
    func setupTableView()
    {
        tbtViewCities.estimatedRowHeight = 60
        tbtViewCities.rowHeight = UITableViewAutomaticDimension
        
        let nib = UINib(nibName: "CityListCell", bundle: nil)
        tbtViewCities.register(nib, forCellReuseIdentifier: "CityListCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return arrRecentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "CityListCell") as! CityListCell
        
        cell.selectionStyle = .none
        
        let model = (arrRecentArray[indexPath.row] as! UserPreferedLocationModel)
        cell.lblCityName.text = model.cityName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let indexPath = tableView.indexPathForSelectedRow;

        //valueToPass = (arrRecentArray[indexPath!.row] as! String)
        let model = (arrRecentArray[indexPath.row] as! UserPreferedLocationModel)

        btnSaveClicked(cityName: model.cityName)

        Preferences.saveUserCurrentCity(userPreferedLocationModel: model.toDictionary() as NSDictionary)
        
        self.dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }


    @IBAction func btnSelectCountryClicked(_ sender: Any) {
        getCountries()
    }
    @IBAction func btnSelectStateClicked(_ sender: Any) {

        if selCountryId != nil{
             getStates(countryId: self.selCountryId)
        }else{
            VIEWMANAGER.showToast("Please select country")
        }

        }
    
    
    @IBAction func btnSelectCityClicked(_ sender: Any) {

        if selStateId != nil && selCountryId != nil{
             getCities(stateId: self.selStateId)
        }else{
            VIEWMANAGER.showToast("Please select country and state")
        }
       
        }


    
    @IBAction func btnSearchClicked(_ sender: Any) {
        if (selStateId != nil && selCityId != nil && selCountryId != nil) {
            btnSaveClicked(cityName: lblCity.text!)
            arrRecentArray.add(userLocationModel)
            Preferences.saveRecentSearchLocationList(arrCity: arrRecentArray)
            Preferences.saveUserCurrentCity(userPreferedLocationModel: userLocationModel.toDictionary() as NSDictionary)
            
            Preferences.saveGeneralSettingModelData(generalSettingModel: userLocationModel.toDictionary() as NSDictionary)
            
            self.dismiss(animated: true, completion: nil)
        }else{
            VIEWMANAGER.showToast("Please select location")
        }
        
    }
    
    
    
    func onLocationSaveClicked(closure :@escaping (String)->()){
        locationSendClosure = closure
    }
    
    @objc func btnSaveClicked(cityName : String ){
        if locationSendClosure != nil{
            locationSendClosure!(cityName)
        }
    }
    
    
    
    
    func getCountries() {
        let param = [String : AnyObject]()
  //      VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_COUNTRIES_API, params: param, completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            
            self.countryData = responseData as? [CountryModel]
            let arrCountry = self.countryData.map { $0.name }
 
            self.dropDownData = arrCountry as? [String]
            self.showDropDown(title: "Select Country",dropDowndata:  self.countryData , type: "country")
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
    func getStates(countryId:String) {
        let param = ["id" : countryId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_STATE_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.stateData = responseData as? [StateModel]
            let arrState = self.stateData.map { $0.name }
            self.dropDownData = arrState as? [String]
            self.showDropDown(title: "Select State",dropDowndata: self.stateData, type: "state")
           // self.lblState.dropDownData = arrState as? [String]
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    func getCities(stateId:String) {
        let param = ["id" : stateId ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName:GET_CITIES_API, params: param as [String : AnyObject], completion: { (responseData) in
            VIEWMANAGER.hideActivityIndicator()
            self.cityData = responseData as? [CityModel]
            let arrCity = self.cityData.map { $0.name }
            self.showDropDown(title: "Select City",dropDowndata:  self.cityData , type: "city")
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg)
        }
    }
    
    
//    func showDropDown(title : String , dropDowndata : [String]!)
//    {
//        if dropDownData == nil {
//            return
//        }
//        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: title,data:dropDowndata)
//        UIApplication.shared.keyWindow?.addSubview(dropDown)
//        dropDown.onDoneClicked { (data) in
//            if title == "Select State"{
//                 self.lblState.text = data
//            } else if title == "Select City"{
//                 self.lblCity.text = data
//            }
//            else if title == "Select Country"{
//                self.lblCountry.text = data
//            }
//
//        }
//
//    }
    
    func showDropDown(title : String , dropDowndata : [AnyObject]!,  type: String)
    {
        if dropDownData == nil {
            return
        }
        let dropDown = DropDownListView(frame: UIScreen.main.bounds, title: title,data:NSArray() as! [String])
        UIApplication.shared.keyWindow?.addSubview(dropDown)
        dropDown.onDoneClicked { (data) in
            
            if data is CountryModel {
                let model = data as! CountryModel
                 self.lblCountry.text = model.name
                self.selCountryId = model.id
                self.userLocationModel.countryId = model.id
                self.userLocationModel.countryName = model.name
                
            }
            else if data is StateModel {
                let model = data as! StateModel
                self.lblState.text = model.name
                self.selStateId = model.id
                
                self.userLocationModel.stateId = model.id
                self.userLocationModel.stateName = model.name
            }
            else if data is CityModel {
                let model = data as! CityModel
                self.lblCity.text = model.name
                self.selCityId = model.id
                
                self.userLocationModel.cityId = model.id
                self.userLocationModel.cityName = model.name
            }
            
        }
     
        dropDown.updateList(list: dropDowndata! as NSArray, type: type)
    }
    
}
