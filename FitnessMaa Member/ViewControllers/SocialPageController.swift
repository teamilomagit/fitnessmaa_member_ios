//
//  SocialPageController.swift
//  FitnessMaa Member
//
//  Created by iLoma Technology on 08/06/19.
//  Copyright © 2019 Ashwini Rokade. All rights reserved.
//

import UIKit
import HCSStarRatingView

class SocialPageController: UIViewController {
    
    @IBOutlet weak var btnSubmit: AppCustomButton!
    @IBOutlet weak var imgViewDp: UIImageView!
    
    @IBOutlet weak var lblAvgRating: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var bigRatingView: HCSStarRatingView!
    
    var strPostedById : String!

    @IBOutlet weak var btnLinkdinLink: UIButton!
    @IBOutlet weak var btnInstaLink: UIButton!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var btnLink1: UIButton!
    @IBOutlet weak var btnLink2: UIButton!
    @IBOutlet weak var btnLink3: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.isHidden = true
        getSocialPageApi()
        
        if  VIEWMANAGER.currentUser!.id == nil{
            btnSubmit.isHidden = true
            bigRatingView.isHidden = true
            lblAvgRating.text = "0"
        }
        
        btnLinkdinLink.titleLabel?.font = UIFont.appRegularFont(size: 16)
         btnInstaLink.titleLabel?.font = UIFont.appRegularFont(size: 16)
         btnLink1.titleLabel?.font = UIFont.appRegularFont(size: 16)
         btnLink2.titleLabel?.font = UIFont.appRegularFont(size: 16)
         btnLink3.titleLabel?.font = UIFont.appRegularFont(size: 16)
        lblName.font = UIFont.appMediumFont(size: 16)
        lblAbout.font = UIFont.appRegularFont(size: 16)

    }
  
    
    
    // Api
    func getSocialPageApi() {
        
        let param = [
            "page_id": strPostedById,
            "user_id" : VIEWMANAGER.currentUser?.id
        ]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: GET_SOCIAL_PAGE_DETAIL_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! SocialPageModel
            self.mainView.isHidden = false
            self.setSocialPageData(model: model)
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
    func setSocialPageData(model : SocialPageModel){
        lblName.text = model.fullName
        lblAbout.text = model.aboutInfo
        if model.fbUrl != nil{
            btnLink1.setTitle((model.fbUrl), for: .normal)
       
        }
         if model.twitterUrl != nil{
            btnLink2.setTitle((model.twitterUrl!), for: .normal)
        }
         if model.youtubeUrl != nil{
            btnLink3.setTitle((model.youtubeUrl!), for: .normal)
        }
        if model.instaUrl != nil{
            btnInstaLink.setTitle((model.instaUrl!), for: .normal)
        }else{
            btnInstaLink.isHidden = true
        }
        if model.linkedinUrl != nil{
            btnLinkdinLink.setTitle((model.linkedinUrl!), for: .normal)
        }else{
            btnLinkdinLink.isHidden = true
        }
        
        if model.profileImage != nil{
        
         imgViewDp.sd_setImage(with: URL(string: "\(MEDIA_BASE_URL2)\(model.profileImage!)"), placeholderImage: UIImage(named: "ic_user_placeholder"))
        }
        
        if model.banerImage != nil{
            
            imgBanner.sd_setImage(with: URL(string: "\(MEDIA_BASE_URL2)\(model.banerImage!)"), placeholderImage: UIImage(named: "ic_user_placeholder"))
        }
        
        lblAvgRating.text = model.avg_rating
        
        if model.avg_rating != nil{
            guard let bigRatingVal = NumberFormatter().number(from: model.avg_rating) else { return }
            bigRatingView.value = CGFloat(bigRatingVal)
        }
       
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        getRatingApi()
    }
    
    func getRatingApi() {
        
        let param = [
            "page_id": strPostedById,
            "user_id":VIEWMANAGER.currentUser?.id,
            "rating": bigRatingView.value
            
            ] as [String : Any]
        VIEWMANAGER.showActivityIndicator()
        RemoteAPI().callPOSTApi(apiName: ADD_RETTING_SOCIAL_API, params: param as [String : AnyObject], completion: { (response) in
            let model = response as! SocialPageModel
            VIEWMANAGER.hideActivityIndicator()
            
        }) { (errMsg) in
            VIEWMANAGER.hideActivityIndicator()
            VIEWMANAGER.showToast(errMsg!)
        }
    }
    
}
