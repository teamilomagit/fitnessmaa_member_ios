//
//  ModelParser.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
class ModelParser {
    
    typealias SuccessBlock2Param = (AnyObject,AnyObject) -> Void
    typealias SuccessBlock1Param = (AnyObject) -> Void

    var successClosure2Param : SuccessBlock2Param?
    var successClosure1Param : SuccessBlock1Param?

    
    func onSuccess(closure:@escaping SuccessBlock2Param){
        
        self.successClosure2Param = closure
    }
    
    func onSuccess(closure:@escaping SuccessBlock1Param){
        
        self.successClosure1Param = closure
    }
    
    func modelParserWithData(apiURL:String,responseData:AnyObject)->AnyObject
    {
        if apiURL == LOGIN_URL  {
            return parseUserData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == MEMBER_PROFILE_URL  {
            return parseUserProfileData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == UPDATE_PROFILE_API  {
            return parseUserProfileData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL == MEMBER_PAYMENT_LIST_URL {
            return parsePaymentListData(responseData: responseData as! NSArray)
        }
        if apiURL == MEMBER_ATTENDANCE_LIST_URL {
            return parseAttendanceListData(responseData: responseData as! NSArray)
        }
        if apiURL == MEMBER_PACKAGE_LIST_URL {
            return parsePackageListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_EXERCISE_LIST {
            return parseExerciseListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_COUNTRIES_API {
            return parseCountryListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_STATE_API {
            return parseStateListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_CITIES_API {
            return parseCityListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_OFFERS_API {
            return parseOfferListData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_LATEST_FEED_API {
            return parseLatestFeedData(responseData: responseData as! NSDictionary)
        }
        if apiURL == LOGIN_NORMAL_USER_URL {
            return parseUserData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_NOTIFICATION_API {
            return parseNotificationListData(responseData: responseData as! NSArray)
        }
        if apiURL == GET_FEED_LIKES_API {
            return parseLikeFeedData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_FEED_UNLIKE_API {
            return parseUnlikeFeedData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == LIKE_RECIPE_API {
            return parseLikeRecipeData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == UNLIKE_RECIPE_API {
            return parseUnlikeRecipeData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_OFFER_DETAIL_API {
            return parseOfferDetaildData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_OFFER_INTERESTED_API {
            return parseOfferDetaildData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_FEED_VIEWS_API {
            return parseViewsFeedData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_RECIPE_INCREMENT_VIEW_API {
            return parseIncrementViewsRecipeData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_FEED_INCREMENT_VIEW_API {
            return parseIncrementViewsFeedData(responseData: responseData as! NSDictionary) as AnyObject
        }
        
        if apiURL == GET_LATEST_RECIPE_API {
            return parseLatestRecipesListData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_NUTRITIONIST_PROFILE_API {
            return parseNutritionistProfiledData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_SOCIAL_PAGE_DETAIL_API {
            return parseSocialPageData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == ADD_RETTING_API {
            return parseNutritionistProfiledData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == ADD_RETTING_SOCIAL_API {
            return parseSocialPageData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == GET_CONNECTED_CENTER_API {
            return parseConnectedCenterListData(responseData: responseData as! NSArray)
        }
        if apiURL == RESET_PASS_API {
            return parseResetPasswordData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == FORGOT_PASS_API {
            return parseForgetPasswordData(responseData: responseData as! NSDictionary) as AnyObject
        }
//        if apiURL == CHANGE_PASSWORS_API{
//             return parseForgetPasswordData(responseData: responseData as! NSDictionary) as AnyObject
//        }
        if apiURL == GENERAL_SETTING_API{
            return parsGeneralSettingData(responseData: responseData as! NSDictionary) as AnyObject
        }
        if apiURL == CHECK_APP_UPDATE_API{
            return parsUpdateAppData(responseData: responseData as! NSDictionary) as AnyObject
        }
        return [] as AnyObject
    }
    
    func parseUserData(responseData: NSDictionary) -> Any? {
        Preferences.saveLoginResponse(response: responseData)
        let model = UserModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseUserProfileData(responseData: NSDictionary) -> Any? {
        let model = UserModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parsePaymentListData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = PaymentListModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseAttendanceListData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = AttendanceModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parsePackageListData(responseData : NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = PackageListModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseExerciseListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = ExerciseListModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseCountryListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = CountryModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    func parseStateListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = StateModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseCityListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = CityModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseOfferListData(responseData: NSDictionary) -> Any?
    {
        let model = OffersModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parseLatestFeedData(responseData:NSDictionary) -> NSDictionary
    {
        let arrToSend = NSMutableArray()
        for dict in responseData.object(forKey: "feeds") as! NSArray {
            let model = FeedsModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        
        let tags = NSMutableArray()
        for dict in responseData.object(forKey: "tags") as! NSArray {
            let model = OptionsModel(fromDictionary: dict as! [String : Any])
            tags.add(model)
        }
        
        if successClosure2Param != nil{
            successClosure2Param!(arrToSend,tags)
        }
        
        
        return [:]
    }
    func parseNormalUserLoginData(responseData: NSDictionary) -> Any? {
        Preferences.saveNormalUserLoginResponse(response: responseData)
        let model = UserModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    func parseNotificationListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = NotificationModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseLikeFeedData(responseData: NSDictionary) -> Any?
    {
        let model = FeedsModel.init(fromDictionary:responseData as! [String : Any])
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    func parseUnlikeFeedData(responseData: NSDictionary) -> Any?
    {
        let model = FeedsModel.init(fromDictionary:responseData as! [String : Any])
   
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
       return [:]
    }
    func parseLikeRecipeData(responseData: NSDictionary) -> Any?
    {
        let model = Recipe.init(fromDictionary:responseData as! [String : Any])
        
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    func parseUnlikeRecipeData(responseData: NSDictionary) -> Any?
    {
        let model = Recipe.init(fromDictionary:responseData as! [String : Any])
        
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    
    func parseViewsFeedData(responseData: NSDictionary) -> Any?
    {
        let model = FeedsModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parseOfferDetaildData(responseData: NSDictionary) -> Any?
    {
        let model = OfferDetailModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    
    func parseIncrementViewsRecipeData(responseData: NSDictionary) -> Any?
    {
        let model = Recipe.init(fromDictionary:responseData as! [String : Any])
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    func parseIncrementViewsFeedData(responseData: NSDictionary) -> Any?
    {
        let model = FeedsModel.init(fromDictionary:responseData as! [String : Any])
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    
    
    func parseLatestRecipesListData(responseData:NSDictionary) -> NSDictionary
    {
        let arrToSend = NSMutableArray()
        for dict in responseData.object(forKey: "recipes") as! NSArray {
            let model = Recipe(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        
        let tags = NSMutableArray()
        for dict in responseData.object(forKey: "tags") as! NSArray {
            let model = OptionsModel(fromDictionary: dict as! [String : Any])
            tags.add(model)
        }
        
        if successClosure2Param != nil{
            successClosure2Param!(arrToSend,tags)
        }
        
        print(tags)
        
        return [:]
    }
    
    
    func parseNutritionistProfiledData(responseData: NSDictionary) -> Any?
    {
        let model = NutritionistProfileModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    
    func parseSocialPageData(responseData: NSDictionary) -> Any?
    {
        let model = SocialPageModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parseConnectedCenterListData(responseData:NSArray) -> NSArray
    {
        let arrToSend = NSMutableArray()
        for dict in responseData {
            let model = ConnectedCenterModel(fromDictionary: dict as! [String : Any])
            arrToSend.add(model)
        }
        return arrToSend
    }
    
    func parseResetPasswordData(responseData: NSDictionary) -> Any?
    {
        let model = UserModel.init(fromDictionary:responseData as! [String : Any])
        
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    func parseForgetPasswordData(responseData: NSDictionary) -> Any?
    {
        let model = UserModel.init(fromDictionary:responseData as! [String : Any])
        
        if successClosure1Param != nil{
            successClosure1Param!(model)
        }
        return [:]
    }
    
    func parsGeneralSettingData(responseData: NSDictionary) -> Any?
    {
        let model = GeneralSettingModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    func parsUpdateAppData(responseData: NSDictionary) -> Any?
    {
        let model = AppUpdateModel.init(fromDictionary:responseData as! [String : Any])
        return model
    }
    
    
    
}
