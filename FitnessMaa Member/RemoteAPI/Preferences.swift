//
//  Preferences.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import UIKit
let USER_DEFAULTS  = UserDefaults.standard
class Preferences
{
    class func saveLoginResponse(response: NSDictionary) {
        let responseData = NSKeyedArchiver.archivedData(withRootObject: response)

        USER_DEFAULTS.set(responseData, forKey: "LoginResponse")
        USER_DEFAULTS.synchronize()
    }
    
    class func getLoginResponse() -> NSDictionary? {
        let loginDict = USER_DEFAULTS.object(forKey: "LoginResponse")

        if loginDict == nil {
            return nil
        }
        let responseData = NSKeyedUnarchiver.unarchiveObject(with: loginDict as! Data)
        return responseData as? NSDictionary
    }
    
    class func saveNormalUserLoginResponse(response: NSDictionary) {
        let responseData = NSKeyedArchiver.archivedData(withRootObject: response)
        USER_DEFAULTS.set(responseData, forKey: "NormalUserLoginResponse")
        USER_DEFAULTS.synchronize()
    }
    
    class func getNormalUserLoginResponse() -> NSDictionary? {
        let loginDict = USER_DEFAULTS.object(forKey: "NormalUserLoginResponse")
        if loginDict == nil {
                        return nil
                    }
        let responseData = NSKeyedUnarchiver.unarchiveObject(with: loginDict as! Data)
        return responseData as? NSDictionary
        
    }
    class func clearLoginResponse() {
        USER_DEFAULTS.removeObject(forKey: "LoginResponse")
        USER_DEFAULTS.synchronize()
    }
    
    class func saveDeviceToken(token: String) {
        USER_DEFAULTS.set(token, forKey: "DeviceToken")
        USER_DEFAULTS.synchronize()
    }
    
    class func getDeviceToken() -> String {
        let deviceToken = USER_DEFAULTS.value(forKey: "DeviceToken")
        if deviceToken == nil {
            return ""
        }
        return deviceToken as! String
    }
    
    class func setAppLanguage(lang:String) {
        USER_DEFAULTS.set(lang, forKey: "AppLanguage")
        USER_DEFAULTS.synchronize()
    }
    
    class func getAppLanguage() -> String
    {
        let lang = USER_DEFAULTS.value(forKey: "AppLanguage")
        if lang == nil {
            return ENGLISH
        }
        return lang as! String
    }
    
    
    class func saveRecentSearchLocationList(arrCity: NSMutableArray) {
        let responseData = NSKeyedArchiver.archivedData(withRootObject: arrCity)
        USER_DEFAULTS.set(responseData, forKey: "RecentCityList")
        USER_DEFAULTS.synchronize()
    }
    
    class func getRecentSearchLocationList() -> NSMutableArray {
        let recentCity = USER_DEFAULTS.object(forKey: "RecentCityList")
        if recentCity == nil{
            return []
        }
        let responseData = NSKeyedUnarchiver.unarchiveObject(with: recentCity as! Data)

        return (responseData as? NSMutableArray)!
    }
    
    class func saveUserCurrentCity(userPreferedLocationModel: NSDictionary?) {
        USER_DEFAULTS.set(userPreferedLocationModel, forKey: "RecentCity")
        USER_DEFAULTS.synchronize()
    }
    
    class func getUserCurrentCity() -> UserPreferedLocationModel? {
        
        if USER_DEFAULTS.object(forKey: "RecentCity") == nil {
            return nil
        }
        let cityDict = UserPreferedLocationModel.init(fromDictionary: USER_DEFAULTS.object(forKey: "RecentCity") as! [String : Any])
        return cityDict as? UserPreferedLocationModel
    }
    
    class func saveGeneralSettingModelData(generalSettingModel : NSDictionary){
        USER_DEFAULTS.set(generalSettingModel, forKey: "general")
        USER_DEFAULTS.synchronize()
    }
    
    class func getGeneralSettingModelData() -> GeneralSettingModel?{
        if USER_DEFAULTS.object(forKey: "general") == nil{
            return nil
        }
        
        let generalDict = USER_DEFAULTS.object(forKey: "general") as! [String : Any]
        return GeneralSettingModel(fromDictionary: generalDict)
    }
}
