//
//  RemoteAPI.swift
//  FitnessMaa
//
//  Created by iLoma on 31/12/18.
//  Copyright © 2018 iLoma. All rights reserved.
//

import Foundation
import Alamofire



class RemoteAPI{
    
    
   // var successClosure : (AnyObject) -> Void
    
    typealias SuccessBlock1Param = (AnyObject) -> Void
    typealias SuccessBlock2Param = (AnyObject,String) -> Void
    typealias SuccessBlock3Param = (AnyObject,AnyObject,String) -> Void
    typealias ErrorBlock = (String?) -> Void
    typealias ResponseBlock = (NSDictionary) -> Void


    
    func callPOSTApi(apiName:String,params:[String: AnyObject]?,apiType:HTTPMethod = .post,completion:@escaping SuccessBlock3Param,failed:@escaping ErrorBlock )
    {
        
        processAPICall(apiName: apiName, params: params, failed: failed) { (serverResponse) in
            
            let modelParser = ModelParser()
            
            modelParser.onSuccess(closure: { (response1, response2) in
                completion(response1,response2,serverResponse["message"] as! String)
            })
            let result = modelParser.modelParserWithData(apiURL: apiName, responseData: serverResponse.value(forKey: "response") as AnyObject)
            print(result)
        }
        
    }
    
    func callPOSTApi(apiName:String,params:[String: AnyObject]?,apiType:HTTPMethod = .post,completion:@escaping SuccessBlock2Param,failed:@escaping ErrorBlock )
    {
        
        processAPICall(apiName: apiName, params: params, failed: failed) { (serverResponse) in
            
            let modelParser = ModelParser()
            
            modelParser.onSuccess(closure: { (response) in
                completion(response,serverResponse["message"] as! String)
            })
           let result =  modelParser.modelParserWithData(apiURL: apiName, responseData: serverResponse.value(forKey: "response") as AnyObject)
            print(result)
        }
        
    }
    
    func callPOSTApi(apiName:String,params:[String: AnyObject]?,apiType:HTTPMethod = .post,completion:@escaping SuccessBlock1Param,failed:@escaping ErrorBlock )
    {
        
        
        processAPICall(apiName: apiName, params: params, failed: failed) { (serverResponse) in
            
            if apiName == CHANGE_PASSWORS_API {
                completion(serverResponse.value(forKey: "message") as AnyObject)
                return;
            }
            
            let obj = ModelParser().modelParserWithData(apiURL: apiName, responseData: serverResponse.value(forKey: "response") as AnyObject)
            completion(obj)
        }
    
    }
    
    
    func processAPICall(apiName:String,params:[String: AnyObject]?,apiType:HTTPMethod = .post,failed:@escaping ErrorBlock,successBlock:@escaping ResponseBlock ){
        
        var param = NSMutableDictionary()
        if apiType == .post {
            param = NSMutableDictionary(dictionary: params!)
            param.setValue(API_SECRET, forKey: "api_secret")
        }
        
        Alamofire.request(apiName, method: apiType, parameters: param as!  [String : AnyObject], encoding: JSONEncoding.default).validate(contentType: ["application/json","text/html"]).responseJSON { response in
            
            debugPrint(response)
            if let ERR = response.result.error
            {
                print(ERR);
                if (ERR as NSError).code == -1009 {
                    failed(NO_INTERNET)
                }
                else {
                    failed("Invalid Request")
                }
                return
            }
            
            if let JSON = response.result.value
            {
                let serverResponse = JSON as? NSDictionary
                if serverResponse?.value(forKey: "status") as! String == "ACCESS_DENIED"
                {
                    app_del.setLoginAsRoot()
                    return
                }
                if serverResponse?.value(forKey: "status") as! String == "ERROR"
                {
                    failed(serverResponse?.value(forKey: "message") as? String)
                    return
                }

                
                
                successBlock(serverResponse!)
            }
        }
        
    }
    
   
    
    func uploadImageToServer(apiName:String,params:[String: AnyObject],completion: @escaping (AnyObject) -> Void,failed:@escaping (_ errorMsg:String?) -> Void)
    {
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            ]
//        let fullAPI = "\(MEDIA_BASE_URL)\(apiName)"
        let fullAPI = apiName
        
        let url = URL(string: fullAPI)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            let reqKeys = NSMutableDictionary(dictionary: params).allKeys
            for k in reqKeys
            {
                let key = k as! String
                if key == "file_path"  || key == "file"
                {
                    let fileUrl = URL(fileURLWithPath:params[key] as! String)
                    multipartFormData.append(fileUrl, withName: key)
                }
                else{
                    multipartFormData.append((params[key] as! String).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (encodingResult) in
            switch encodingResult
            {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    if let serverResponse = response.result.value as? NSDictionary {
                        if serverResponse.value(forKey: "status") as! String == "ERROR"
                        {
                            failed(serverResponse.value(forKey: "message") as? String)
                            return
                        }
                        let obj = ModelParser().modelParserWithData(apiURL: apiName, responseData: serverResponse.value(forKey: "response") as AnyObject)
                        completion(obj)
                    }
                    else{
                        failed("Something is wrong")
                        return
                        
                    }
                })
                break
                
            case .failure(let error):
                failed(error.localizedDescription)
                break
            }
        }
    }
    
}
