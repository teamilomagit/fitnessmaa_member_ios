//
//  AppUpdateModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 12, 2019

import Foundation


class AppUpdateModel : NSObject, NSCoding{

    var createdAt : String!
    var deviceType : String!
    var id : String!
    var isOptional : Bool!
    var msg : String!
    var showPopup : Bool!
    var updatedAt : String!
    var versionName : String!
    var versionNo : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? String
        deviceType = dictionary["device_type"] as? String
        id = dictionary["id"] as? String
        isOptional = dictionary["is_optional"] as? Bool
        msg = dictionary["msg"] as? String
        showPopup = dictionary["show_popup"] as? Bool
        updatedAt = dictionary["updated_at"] as? String
        versionName = dictionary["version_name"] as? String
        versionNo = dictionary["version_no"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if deviceType != nil{
            dictionary["device_type"] = deviceType
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isOptional != nil{
            dictionary["is_optional"] = isOptional
        }
        if msg != nil{
            dictionary["msg"] = msg
        }
        if showPopup != nil{
            dictionary["show_popup"] = showPopup
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if versionName != nil{
            dictionary["version_name"] = versionName
        }
        if versionNo != nil{
            dictionary["version_no"] = versionNo
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        deviceType = aDecoder.decodeObject(forKey: "device_type") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        isOptional = aDecoder.decodeObject(forKey: "is_optional") as? Bool
        msg = aDecoder.decodeObject(forKey: "msg") as? String
        showPopup = aDecoder.decodeObject(forKey: "show_popup") as? Bool
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        versionName = aDecoder.decodeObject(forKey: "version_name") as? String
        versionNo = aDecoder.decodeObject(forKey: "version_no") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if deviceType != nil{
            aCoder.encode(deviceType, forKey: "device_type")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isOptional != nil{
            aCoder.encode(isOptional, forKey: "is_optional")
        }
        if msg != nil{
            aCoder.encode(msg, forKey: "msg")
        }
        if showPopup != nil{
            aCoder.encode(showPopup, forKey: "show_popup")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if versionName != nil{
            aCoder.encode(versionName, forKey: "version_name")
        }
        if versionNo != nil{
            aCoder.encode(versionNo, forKey: "version_no")
        }
    }
}