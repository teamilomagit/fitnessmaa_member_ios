//
//  UserModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 25, 2019

import Foundation


class UserModel : NSObject, NSCoding{

    var cityId : String!
    var countryId : String!
    var createdAt : String!
    var dob : AnyObject!
    var email : String!
    var fullName : String!
    var gender : AnyObject!
    var id : String!
    var image : AnyObject!
    var mobile : String!
    var refarralCode : String!
    var refferedBy : AnyObject!
    var stateId : String!
    var updatedAt : String!
    var country_name : String!
    var state_name : String!
    var city_name : String!
    
    
    var address : String!
    var deviceId : String!
    var gym : String!
    var gymId : String!
    var gymName : String!
    var gymProfilePic : String!
    var imageUrl : String!
    var isDeleted : Bool!
    var joinDate : String!
    var registrationFee : String!
    var status : String!
    var totalPackageAmount : String!
    var totalPaidAmount : String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cityId = dictionary["city_id"] as? String
        countryId = dictionary["country_id"] as? String
        createdAt = dictionary["created_at"] as? String
        dob = dictionary["dob"] as? AnyObject
        email = dictionary["email"] as? String
        fullName = dictionary["full_name"] as? String
        gender = dictionary["gender"] as? AnyObject
        id = dictionary["id"] as? String
        image = dictionary["image"] as? AnyObject
        mobile = dictionary["mobile"] as? String
        refarralCode = dictionary["refarral_code"] as? String
        refferedBy = dictionary["reffered_by"] as? AnyObject
        stateId = dictionary["state_id"] as? String
        updatedAt = dictionary["updated_at"] as? String
        country_name = dictionary["country_name"] as? String
        state_name = dictionary["state_name"] as? String
        city_name = dictionary["city_name"] as? String
        
        address = dictionary["address"] as? String
        deviceId = dictionary["deviceId"] as? String
        gym = dictionary["gym"] as? String
        gymId = dictionary["gymId"] as? String
        gymName = dictionary["gymName"] as? String
        gymProfilePic = dictionary["gymProfilePic"] as? String
        imageUrl = dictionary["imageUrl"] as? String
        isDeleted = dictionary["isDeleted"] as? Bool
        joinDate = dictionary["joinDate"] as? String
        registrationFee = dictionary["registrationFee"] as? String
         status = dictionary["status"] as? String
         totalPackageAmount = dictionary["totalPackageAmount"] as? String
         totalPaidAmount = dictionary["totalPaidAmount"] as? String
    
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if dob != nil{
            dictionary["dob"] = dob
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if refarralCode != nil{
            dictionary["refarral_code"] = refarralCode
        }
        if refferedBy != nil{
            dictionary["reffered_by"] = refferedBy
        }
        if stateId != nil{
            dictionary["state_id"] = stateId
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if country_name != nil{
            dictionary["country_name"] = updatedAt
        }
        if state_name != nil{
            dictionary["state_name"] = updatedAt
        }
        if city_name != nil{
            dictionary["city_name"] = updatedAt
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cityId = aDecoder.decodeObject(forKey: "city_id") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        dob = aDecoder.decodeObject(forKey: "dob") as? AnyObject
        email = aDecoder.decodeObject(forKey: "email") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? AnyObject
        id = aDecoder.decodeObject(forKey: "id") as? String
        image = aDecoder.decodeObject(forKey: "image") as? AnyObject
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        refarralCode = aDecoder.decodeObject(forKey: "refarral_code") as? String
        refferedBy = aDecoder.decodeObject(forKey: "reffered_by") as? AnyObject
        stateId = aDecoder.decodeObject(forKey: "state_id") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if refarralCode != nil{
            aCoder.encode(refarralCode, forKey: "refarral_code")
        }
        if refferedBy != nil{
            aCoder.encode(refferedBy, forKey: "reffered_by")
        }
        if stateId != nil{
            aCoder.encode(stateId, forKey: "state_id")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}
