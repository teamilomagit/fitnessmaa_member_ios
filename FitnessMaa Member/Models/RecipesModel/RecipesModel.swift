//
//  RecipesModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 1, 2019

import Foundation


class RecipesModel : NSObject, NSCoding{

    var cuisine : [Cuisine]!
    var recipes : [Recipe]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cuisine = [Cuisine]()
        if let cuisineArray = dictionary["cuisine"] as? [[String:Any]]{
            for dic in cuisineArray{
                let value = Cuisine(fromDictionary: dic)
                cuisine.append(value)
            }
        }
        recipes = [Recipe]()
        if let recipesArray = dictionary["recipes"] as? [[String:Any]]{
            for dic in recipesArray{
                let value = Recipe(fromDictionary: dic)
                recipes.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cuisine != nil{
            var dictionaryElements = [[String:Any]]()
            for cuisineElement in cuisine {
                dictionaryElements.append(cuisineElement.toDictionary())
            }
            dictionary["cuisine"] = dictionaryElements
        }
        if recipes != nil{
            var dictionaryElements = [[String:Any]]()
            for recipesElement in recipes {
                dictionaryElements.append(recipesElement.toDictionary())
            }
            dictionary["recipes"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cuisine = aDecoder.decodeObject(forKey: "cuisine") as? [Cuisine]
        recipes = aDecoder.decodeObject(forKey: "recipes") as? [Recipe]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cuisine != nil{
            aCoder.encode(cuisine, forKey: "cuisine")
        }
        if recipes != nil{
            aCoder.encode(recipes, forKey: "recipes")
        }
    }
}