//
//  Cuisine.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 1, 2019

import Foundation


class Cuisine : NSObject, NSCoding{

    var cuisine : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cuisine = dictionary["cuisine"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cuisine != nil{
            dictionary["cuisine"] = cuisine
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cuisine = aDecoder.decodeObject(forKey: "cuisine") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cuisine != nil{
            aCoder.encode(cuisine, forKey: "cuisine")
        }
    }
}