//
//  Recipe.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 1, 2019

import Foundation


class Recipe : NSObject, NSCoding{

    var id : String!
    var imageUrl : String!
    var isPureVeg : Bool!
    var noOfLikes : String!
    var noOfViews : String!
    var postedByUserId : String!
    var title : String!
    var videoUrl : String!
    var liked_by_me : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["id"] as? String
        imageUrl = dictionary["image_url"] as? String
        isPureVeg = dictionary["is_pure_veg"] as? Bool
        noOfLikes = dictionary["no_of_likes"] as? String
        noOfViews = dictionary["no_of_views"] as? String
        postedByUserId = dictionary["posted_by_user_id"] as? String
        title = dictionary["title"] as? String
        videoUrl = dictionary["video_url"] as? String
        liked_by_me = dictionary["liked_by_me"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if isPureVeg != nil{
            dictionary["is_pure_veg"] = isPureVeg
        }
        if noOfLikes != nil{
            dictionary["no_of_likes"] = noOfLikes
        }
        if noOfViews != nil{
            dictionary["no_of_views"] = noOfViews
        }
        if postedByUserId != nil{
            dictionary["posted_by_user_id"] = postedByUserId
        }
        if title != nil{
            dictionary["title"] = title
        }
        if videoUrl != nil{
            dictionary["video_url"] = videoUrl
        }
        if liked_by_me != nil{
            dictionary["liked_by_me"] = videoUrl
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        isPureVeg = aDecoder.decodeObject(forKey: "is_pure_veg") as? Bool
        noOfLikes = aDecoder.decodeObject(forKey: "no_of_likes") as? String
        noOfViews = aDecoder.decodeObject(forKey: "no_of_views") as? String
        postedByUserId = aDecoder.decodeObject(forKey: "posted_by_user_id") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        videoUrl = aDecoder.decodeObject(forKey: "video_url") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if isPureVeg != nil{
            aCoder.encode(isPureVeg, forKey: "is_pure_veg")
        }
        if noOfLikes != nil{
            aCoder.encode(noOfLikes, forKey: "no_of_likes")
        }
        if noOfViews != nil{
            aCoder.encode(noOfViews, forKey: "no_of_views")
        }
        if postedByUserId != nil{
            aCoder.encode(postedByUserId, forKey: "posted_by_user_id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if videoUrl != nil{
            aCoder.encode(videoUrl, forKey: "video_url")
        }
    }
}
