//
//  ExerciseListModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 18, 2019

import Foundation


class ExerciseListModel : NSObject, NSCoding{

    var createdAt : String!
    var gymId : String!
    var id : Int!
    var memberId : String!
    var noOfWeeks : String!
    var planDetails : String!
    var planName : String!
    var updatedAt : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? String
        gymId = dictionary["gym_id"] as? String
        id = dictionary["id"] as? Int
        memberId = dictionary["member_id"] as? String
        noOfWeeks = dictionary["no_of_weeks"] as? String
        planDetails = dictionary["plan_details"] as? String
        planName = dictionary["plan_name"] as? String
        updatedAt = dictionary["updated_at"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if gymId != nil{
            dictionary["gym_id"] = gymId
        }
        if id != nil{
            dictionary["id"] = id
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if noOfWeeks != nil{
            dictionary["no_of_weeks"] = noOfWeeks
        }
        if planDetails != nil{
            dictionary["plan_details"] = planDetails
        }
        if planName != nil{
            dictionary["plan_name"] = planName
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        gymId = aDecoder.decodeObject(forKey: "gym_id") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        noOfWeeks = aDecoder.decodeObject(forKey: "no_of_weeks") as? String
        planDetails = aDecoder.decodeObject(forKey: "plan_details") as? String
        planName = aDecoder.decodeObject(forKey: "plan_name") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if gymId != nil{
            aCoder.encode(gymId, forKey: "gym_id")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if noOfWeeks != nil{
            aCoder.encode(noOfWeeks, forKey: "no_of_weeks")
        }
        if planDetails != nil{
            aCoder.encode(planDetails, forKey: "plan_details")
        }
        if planName != nil{
            aCoder.encode(planName, forKey: "plan_name")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}