//
//  PackageListModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 11, 2019

import Foundation


class PackageListModel : NSObject, NSCoding{

    var amount : String!
    var createdAt : String!
    var discount : String!
    var duration : String!
    var endDate : String!
    var finalAmount : String!
    var gymId : String!
    var id : String!
    var memberId : String!
    var noOfDays : String!
    var packageId : String!
    var packageName : String!
    var registrationFee : String!
    var startDate : String!
    var updatedAt : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        amount = dictionary["amount"] as? String
        createdAt = dictionary["created_at"] as? String
        discount = dictionary["discount"] as? String
        duration = dictionary["duration"] as? String
        endDate = dictionary["end_date"] as? String
        finalAmount = dictionary["final_amount"] as? String
        gymId = dictionary["gym_id"] as? String
        id = dictionary["id"] as? String
        memberId = dictionary["member_id"] as? String
        noOfDays = dictionary["no_of_days"] as? String
        packageId = dictionary["package_id"] as? String
        packageName = dictionary["package_name"] as? String
        registrationFee = dictionary["registration_fee"] as? String
        startDate = dictionary["start_date"] as? String
        updatedAt = dictionary["updated_at"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amount != nil{
            dictionary["amount"] = amount
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if discount != nil{
            dictionary["discount"] = discount
        }
        if duration != nil{
            dictionary["duration"] = duration
        }
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if finalAmount != nil{
            dictionary["final_amount"] = finalAmount
        }
        if gymId != nil{
            dictionary["gym_id"] = gymId
        }
        if id != nil{
            dictionary["id"] = id
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if noOfDays != nil{
            dictionary["no_of_days"] = noOfDays
        }
        if packageId != nil{
            dictionary["package_id"] = packageId
        }
        if packageName != nil{
            dictionary["package_name"] = packageName
        }
        if registrationFee != nil{
            dictionary["registration_fee"] = registrationFee
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        amount = aDecoder.decodeObject(forKey: "amount") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        discount = aDecoder.decodeObject(forKey: "discount") as? String
        duration = aDecoder.decodeObject(forKey: "duration") as? String
        endDate = aDecoder.decodeObject(forKey: "end_date") as? String
        finalAmount = aDecoder.decodeObject(forKey: "final_amount") as? String
        gymId = aDecoder.decodeObject(forKey: "gym_id") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        noOfDays = aDecoder.decodeObject(forKey: "no_of_days") as? String
        packageId = aDecoder.decodeObject(forKey: "package_id") as? String
        packageName = aDecoder.decodeObject(forKey: "package_name") as? String
        registrationFee = aDecoder.decodeObject(forKey: "registration_fee") as? String
        startDate = aDecoder.decodeObject(forKey: "start_date") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if amount != nil{
            aCoder.encode(amount, forKey: "amount")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if discount != nil{
            aCoder.encode(discount, forKey: "discount")
        }
        if duration != nil{
            aCoder.encode(duration, forKey: "duration")
        }
        if endDate != nil{
            aCoder.encode(endDate, forKey: "end_date")
        }
        if finalAmount != nil{
            aCoder.encode(finalAmount, forKey: "final_amount")
        }
        if gymId != nil{
            aCoder.encode(gymId, forKey: "gym_id")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if noOfDays != nil{
            aCoder.encode(noOfDays, forKey: "no_of_days")
        }
        if packageId != nil{
            aCoder.encode(packageId, forKey: "package_id")
        }
        if packageName != nil{
            aCoder.encode(packageName, forKey: "package_name")
        }
        if registrationFee != nil{
            aCoder.encode(registrationFee, forKey: "registration_fee")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "start_date")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}