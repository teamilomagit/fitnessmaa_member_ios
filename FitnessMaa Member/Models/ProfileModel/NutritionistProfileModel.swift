//
//  NutritionistProfileModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 6, 2019

import Foundation


class NutritionistProfileModel : NSObject, NSCoding{

    var aboutUs : String!
    var cityId : String!
    var clinicAddress : String!
    var clinicName : String!
    var connectedUserId : AnyObject!
    var contactNo : String!
    var createdAt : String!
    var discountInPercent : String!
    var email : String!
    var fees : String!
    var hideContactNo : String!
    var hideEmail : String!
    var id : Int!
    var image : String!
    var name : String!
    var qualification : String!
    var services : String!
    var timings : String!
    var updatedAt : String!
    var yearOfExp : String!
    var avg_rating : String!
    var currency_symbol : String!
    var currency_id : String!
    var my_rating : String!
  
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aboutUs = dictionary["about_us"] as? String
        cityId = dictionary["city_id"] as? String
        clinicAddress = dictionary["clinic_address"] as? String
        clinicName = dictionary["clinic_name"] as? String
        connectedUserId = dictionary["connected_user_id"] as? AnyObject
        contactNo = dictionary["contact_no"] as? String
        createdAt = dictionary["created_at"] as? String
        discountInPercent = dictionary["discount_in_percent"] as? String
        email = dictionary["email"] as? String
        fees = dictionary["fees"] as? String
        hideContactNo = dictionary["hide_contact_no"] as? String
        hideEmail = dictionary["hide_email"] as? String
        id = dictionary["id"] as? Int
        image = dictionary["image"] as? String
        name = dictionary["name"] as? String
        qualification = dictionary["qualification"] as? String
        services = dictionary["services"] as? String
        timings = dictionary["timings"] as? String
        updatedAt = dictionary["updated_at"] as? String
        yearOfExp = dictionary["year_of_exp"] as? String
        avg_rating = dictionary["avg_rating"] as? String
        currency_symbol = dictionary["currency_symbol"] as? String
        currency_id = dictionary["currency_id"] as? String
        my_rating = dictionary["my_rating"] as? String
    
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aboutUs != nil{
            dictionary["about_us"] = aboutUs
        }
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if clinicAddress != nil{
            dictionary["clinic_address"] = clinicAddress
        }
        if clinicName != nil{
            dictionary["clinic_name"] = clinicName
        }
        if connectedUserId != nil{
            dictionary["connected_user_id"] = connectedUserId
        }
        if contactNo != nil{
            dictionary["contact_no"] = contactNo
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if discountInPercent != nil{
            dictionary["discount_in_percent"] = discountInPercent
        }
        if email != nil{
            dictionary["email"] = email
        }
        if fees != nil{
            dictionary["fees"] = fees
        }
        if hideContactNo != nil{
            dictionary["hide_contact_no"] = hideContactNo
        }
        if hideEmail != nil{
            dictionary["hide_email"] = hideEmail
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if name != nil{
            dictionary["name"] = name
        }
        if qualification != nil{
            dictionary["qualification"] = qualification
        }
        if services != nil{
            dictionary["services"] = services
        }
        if timings != nil{
            dictionary["timings"] = timings
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if yearOfExp != nil{
            dictionary["year_of_exp"] = yearOfExp
        }
        if avg_rating != nil{
            dictionary["avg_rating"] = avg_rating
        }
        if currency_symbol != nil{
            dictionary["currency_symbol"] = currency_symbol
        }
        if currency_id != nil{
            dictionary["currency_id"] = currency_id
        }
        if my_rating != nil{
            dictionary["my_rating"] = my_rating
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        aboutUs = aDecoder.decodeObject(forKey: "about_us") as? String
        cityId = aDecoder.decodeObject(forKey: "city_id") as? String
        clinicAddress = aDecoder.decodeObject(forKey: "clinic_address") as? String
        clinicName = aDecoder.decodeObject(forKey: "clinic_name") as? String
        connectedUserId = aDecoder.decodeObject(forKey: "connected_user_id") as? AnyObject
        contactNo = aDecoder.decodeObject(forKey: "contact_no") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        discountInPercent = aDecoder.decodeObject(forKey: "discount_in_percent") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        fees = aDecoder.decodeObject(forKey: "fees") as? String
        hideContactNo = aDecoder.decodeObject(forKey: "hide_contact_no") as? String
        hideEmail = aDecoder.decodeObject(forKey: "hide_email") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        qualification = aDecoder.decodeObject(forKey: "qualification") as? String
        services = aDecoder.decodeObject(forKey: "services") as? String
        timings = aDecoder.decodeObject(forKey: "timings") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        yearOfExp = aDecoder.decodeObject(forKey: "year_of_exp") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if aboutUs != nil{
            aCoder.encode(aboutUs, forKey: "about_us")
        }
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if clinicAddress != nil{
            aCoder.encode(clinicAddress, forKey: "clinic_address")
        }
        if clinicName != nil{
            aCoder.encode(clinicName, forKey: "clinic_name")
        }
        if connectedUserId != nil{
            aCoder.encode(connectedUserId, forKey: "connected_user_id")
        }
        if contactNo != nil{
            aCoder.encode(contactNo, forKey: "contact_no")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if discountInPercent != nil{
            aCoder.encode(discountInPercent, forKey: "discount_in_percent")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if fees != nil{
            aCoder.encode(fees, forKey: "fees")
        }
        if hideContactNo != nil{
            aCoder.encode(hideContactNo, forKey: "hide_contact_no")
        }
        if hideEmail != nil{
            aCoder.encode(hideEmail, forKey: "hide_email")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if qualification != nil{
            aCoder.encode(qualification, forKey: "qualification")
        }
        if services != nil{
            aCoder.encode(services, forKey: "services")
        }
        if timings != nil{
            aCoder.encode(timings, forKey: "timings")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if yearOfExp != nil{
            aCoder.encode(yearOfExp, forKey: "year_of_exp")
        }
    }
}
