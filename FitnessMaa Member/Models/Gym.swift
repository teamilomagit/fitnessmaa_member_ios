//
//  Gym.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 11, 2019

import Foundation


class Gym : NSObject, NSCoding{

    var address : String!
    var advanceRegistrationFee : String!
    var amenities : String!
    var amenityIds : String!
    var city : String!
    var cityId : String!
    var closeDays : String!
    var countryId : String!
    var createdAt : String!
    var currencyId : String!
    var email : String!
    var eveningTiming : String!
    var gymName : String!
    var gymType : String!
    var id : Int!
    var imageUrl : String!
    var isApproved : Bool!
    var keyFeatures : String!
    var latitude : String!
    var longitude : String!
    var mobile : String!
    var monthlyFee : String!
    var morningTiming : String!
    var ownerFullName : String!
    var speciality : String!
    var state : String!
    var stateId : String!
    var status : String!
    var updatedAt : String!
    var website : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        advanceRegistrationFee = dictionary["advance_registration_fee"] as? String
        amenities = dictionary["amenities"] as? String
        amenityIds = dictionary["amenity_ids"] as? String
        city = dictionary["city"] as? String
        cityId = dictionary["city_id"] as? String
        closeDays = dictionary["close_days"] as? String
        countryId = dictionary["country_id"] as? String
        createdAt = dictionary["created_at"] as? String
        currencyId = dictionary["currency_id"] as? String
        email = dictionary["email"] as? String
        eveningTiming = dictionary["evening_timing"] as? String
        gymName = dictionary["gym_name"] as? String
        gymType = dictionary["gym_type"] as? String
        id = dictionary["id"] as? Int
        imageUrl = dictionary["image_url"] as? String
        isApproved = dictionary["is_approved"] as? Bool
        keyFeatures = dictionary["key_features"] as? String
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        mobile = dictionary["mobile"] as? String
        monthlyFee = dictionary["monthly_fee"] as? String
        morningTiming = dictionary["morning_timing"] as? String
        ownerFullName = dictionary["owner_full_name"] as? String
        speciality = dictionary["speciality"] as? String
        state = dictionary["state"] as? String
        stateId = dictionary["state_id"] as? String
        status = dictionary["status"] as? String
        updatedAt = dictionary["updated_at"] as? String
        website = dictionary["website"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if advanceRegistrationFee != nil{
            dictionary["advance_registration_fee"] = advanceRegistrationFee
        }
        if amenities != nil{
            dictionary["amenities"] = amenities
        }
        if amenityIds != nil{
            dictionary["amenity_ids"] = amenityIds
        }
        if city != nil{
            dictionary["city"] = city
        }
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if closeDays != nil{
            dictionary["close_days"] = closeDays
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if currencyId != nil{
            dictionary["currency_id"] = currencyId
        }
        if email != nil{
            dictionary["email"] = email
        }
        if eveningTiming != nil{
            dictionary["evening_timing"] = eveningTiming
        }
        if gymName != nil{
            dictionary["gym_name"] = gymName
        }
        if gymType != nil{
            dictionary["gym_type"] = gymType
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if isApproved != nil{
            dictionary["is_approved"] = isApproved
        }
        if keyFeatures != nil{
            dictionary["key_features"] = keyFeatures
        }
        if latitude != nil{
            dictionary["latitude"] = latitude
        }
        if longitude != nil{
            dictionary["longitude"] = longitude
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if monthlyFee != nil{
            dictionary["monthly_fee"] = monthlyFee
        }
        if morningTiming != nil{
            dictionary["morning_timing"] = morningTiming
        }
        if ownerFullName != nil{
            dictionary["owner_full_name"] = ownerFullName
        }
        if speciality != nil{
            dictionary["speciality"] = speciality
        }
        if state != nil{
            dictionary["state"] = state
        }
        if stateId != nil{
            dictionary["state_id"] = stateId
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if website != nil{
            dictionary["website"] = website
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        advanceRegistrationFee = aDecoder.decodeObject(forKey: "advance_registration_fee") as? String
        amenities = aDecoder.decodeObject(forKey: "amenities") as? String
        amenityIds = aDecoder.decodeObject(forKey: "amenity_ids") as? String
        city = aDecoder.decodeObject(forKey: "city") as? String
        cityId = aDecoder.decodeObject(forKey: "city_id") as? String
        closeDays = aDecoder.decodeObject(forKey: "close_days") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        currencyId = aDecoder.decodeObject(forKey: "currency_id") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        eveningTiming = aDecoder.decodeObject(forKey: "evening_timing") as? String
        gymName = aDecoder.decodeObject(forKey: "gym_name") as? String
        gymType = aDecoder.decodeObject(forKey: "gym_type") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        isApproved = aDecoder.decodeObject(forKey: "is_approved") as? Bool
        keyFeatures = aDecoder.decodeObject(forKey: "key_features") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        monthlyFee = aDecoder.decodeObject(forKey: "monthly_fee") as? String
        morningTiming = aDecoder.decodeObject(forKey: "morning_timing") as? String
        ownerFullName = aDecoder.decodeObject(forKey: "owner_full_name") as? String
        speciality = aDecoder.decodeObject(forKey: "speciality") as? String
        state = aDecoder.decodeObject(forKey: "state") as? String
        stateId = aDecoder.decodeObject(forKey: "state_id") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        website = aDecoder.decodeObject(forKey: "website") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if advanceRegistrationFee != nil{
            aCoder.encode(advanceRegistrationFee, forKey: "advance_registration_fee")
        }
        if amenities != nil{
            aCoder.encode(amenities, forKey: "amenities")
        }
        if amenityIds != nil{
            aCoder.encode(amenityIds, forKey: "amenity_ids")
        }
        if city != nil{
            aCoder.encode(city, forKey: "city")
        }
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if closeDays != nil{
            aCoder.encode(closeDays, forKey: "close_days")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if currencyId != nil{
            aCoder.encode(currencyId, forKey: "currency_id")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if eveningTiming != nil{
            aCoder.encode(eveningTiming, forKey: "evening_timing")
        }
        if gymName != nil{
            aCoder.encode(gymName, forKey: "gym_name")
        }
        if gymType != nil{
            aCoder.encode(gymType, forKey: "gym_type")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if isApproved != nil{
            aCoder.encode(isApproved, forKey: "is_approved")
        }
        if keyFeatures != nil{
            aCoder.encode(keyFeatures, forKey: "key_features")
        }
        if latitude != nil{
            aCoder.encode(latitude, forKey: "latitude")
        }
        if longitude != nil{
            aCoder.encode(longitude, forKey: "longitude")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if monthlyFee != nil{
            aCoder.encode(monthlyFee, forKey: "monthly_fee")
        }
        if morningTiming != nil{
            aCoder.encode(morningTiming, forKey: "morning_timing")
        }
        if ownerFullName != nil{
            aCoder.encode(ownerFullName, forKey: "owner_full_name")
        }
        if speciality != nil{
            aCoder.encode(speciality, forKey: "speciality")
        }
        if state != nil{
            aCoder.encode(state, forKey: "state")
        }
        if stateId != nil{
            aCoder.encode(stateId, forKey: "state_id")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if website != nil{
            aCoder.encode(website, forKey: "website")
        }
    }
}