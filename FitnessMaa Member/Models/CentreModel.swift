//
//  CentreModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 28, 2019

import Foundation


class CentreModel : NSObject, NSCoding{

    var address : String!
    var centre : String!
    var centreId : String!
    var centreName : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        centre = dictionary["centre"] as? String
        centreId = dictionary["centre_id"] as? String
        centreName = dictionary["centre_name"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if centre != nil{
            dictionary["centre"] = centre
        }
        if centreId != nil{
            dictionary["centre_id"] = centreId
        }
        if centreName != nil{
            dictionary["centre_name"] = centreName
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        centre = aDecoder.decodeObject(forKey: "centre") as? String
        centreId = aDecoder.decodeObject(forKey: "centre_id") as? String
        centreName = aDecoder.decodeObject(forKey: "centre_name") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if centre != nil{
            aCoder.encode(centre, forKey: "centre")
        }
        if centreId != nil{
            aCoder.encode(centreId, forKey: "centre_id")
        }
        if centreName != nil{
            aCoder.encode(centreName, forKey: "centre_name")
        }
    }
}