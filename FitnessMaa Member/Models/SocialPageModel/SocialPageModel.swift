//
//  SocialPageModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 8, 2019

import Foundation


class SocialPageModel : NSObject, NSCoding{

    var aboutInfo : String!
    var banerImage : String!
    var createdAt : String!
    var fbUrl : String!
    var fullName : String!
    var id : String!
    var instaUrl : String!
    var linkedinUrl : String!
    var profileImage : String!
    var twitterUrl : String!
    var updatedAt : String!
    var youtubeUrl : String!
    var avg_rating : String!
    var  my_rating : String!



    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        aboutInfo = dictionary["about_info"] as? String
        banerImage = dictionary["baner_image"] as? String
        createdAt = dictionary["created_at"] as? String
        fbUrl = dictionary["fb_url"] as? String
        fullName = dictionary["full_name"] as? String
        id = dictionary["id"] as? String
        instaUrl = dictionary["insta_url"] as? String
        linkedinUrl = dictionary["linkedin_url"] as? String
        profileImage = dictionary["profile_image"] as? String
        twitterUrl = dictionary["twitter_url"] as? String
        updatedAt = dictionary["updated_at"] as? String
        youtubeUrl = dictionary["youtube_url"] as? String
        avg_rating = dictionary["avg_rating"] as? String
        my_rating = (dictionary["my_rating"] as? String)
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if aboutInfo != nil{
            dictionary["about_info"] = aboutInfo
        }
        if banerImage != nil{
            dictionary["baner_image"] = banerImage
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if fbUrl != nil{
            dictionary["fb_url"] = fbUrl
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if instaUrl != nil{
            dictionary["insta_url"] = instaUrl
        }
        if linkedinUrl != nil{
            dictionary["linkedin_url"] = linkedinUrl
        }
        if profileImage != nil{
            dictionary["profile_image"] = profileImage
        }
        if twitterUrl != nil{
            dictionary["twitter_url"] = twitterUrl
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if youtubeUrl != nil{
            dictionary["youtube_url"] = youtubeUrl
        }
        if avg_rating != nil{
            dictionary["avg_rating"] = avg_rating
        }
        if my_rating != nil{
            dictionary["my_rating"] = my_rating
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        aboutInfo = aDecoder.decodeObject(forKey: "about_info") as? String
        banerImage = aDecoder.decodeObject(forKey: "baner_image") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        fbUrl = aDecoder.decodeObject(forKey: "fb_url") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        instaUrl = aDecoder.decodeObject(forKey: "insta_url") as? String
        linkedinUrl = aDecoder.decodeObject(forKey: "linkedin_url") as? String
        profileImage = aDecoder.decodeObject(forKey: "profile_image") as? String
        twitterUrl = aDecoder.decodeObject(forKey: "twitter_url") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        youtubeUrl = aDecoder.decodeObject(forKey: "youtube_url") as? String
         avg_rating = aDecoder.decodeObject(forKey: "avg_rating") as? String
        my_rating = (aDecoder.decodeObject(forKey: "my_rating") as? String)
        
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if aboutInfo != nil{
            aCoder.encode(aboutInfo, forKey: "about_info")
        }
        if banerImage != nil{
            aCoder.encode(banerImage, forKey: "baner_image")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if fbUrl != nil{
            aCoder.encode(fbUrl, forKey: "fb_url")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if instaUrl != nil{
            aCoder.encode(instaUrl, forKey: "insta_url")
        }
        if linkedinUrl != nil{
            aCoder.encode(linkedinUrl, forKey: "linkedin_url")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "profile_image")
        }
        if twitterUrl != nil{
            aCoder.encode(twitterUrl, forKey: "twitter_url")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if youtubeUrl != nil{
            aCoder.encode(youtubeUrl, forKey: "youtube_url")
        }
    }
}
