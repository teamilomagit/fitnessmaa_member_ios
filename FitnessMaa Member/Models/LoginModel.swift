//
//  LoginModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 11, 2019

import Foundation


class LoginModel : NSObject, NSCoding{

    var address : String!
    var createdAt : String!
    var deviceId : String!
    var fullName : String!
    var gender : String!
    var gym : Gym!
    var gymId : String!
    var gymName : String!
    var gymProfilePic : String!
    var id : Int!
    var imageUrl : String!
    var isDeleted : Bool!
    var joinDate : String!
    var mobile : String!
    var registrationFee : String!
    var status : String!
    var updatedAt : String!
    var totalPackageAmount : String!
    var totalPaidAmount : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? String
        createdAt = dictionary["created_at"] as? String
        deviceId = dictionary["device_id"] as? String
        fullName = dictionary["full_name"] as? String
        gender = dictionary["gender"] as? String
        gymId = dictionary["gym_id"] as? String
        gymName = dictionary["gym_name"] as? String
        gymProfilePic = dictionary["gym_profile_pic"] as? String
        id = dictionary["id"] as? Int
        imageUrl = dictionary["image_url"] as? String
        isDeleted = dictionary["isDeleted"] as? Bool
        joinDate = dictionary["join_date"] as? String
        mobile = dictionary["mobile"] as? String
        registrationFee = dictionary["registration_fee"] as? String
        status = dictionary["status"] as? String
        updatedAt = dictionary["updated_at"] as? String
        if let gymData = dictionary["gym"] as? [String:Any]{
            gym = Gym(fromDictionary: gymData)
        }
        totalPackageAmount = dictionary["total_package_amount"] as? String
        totalPaidAmount = dictionary["total_paid_amount"] as? String

    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if deviceId != nil{
            dictionary["device_id"] = deviceId
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if gender != nil{
            dictionary["gender"] = gender
        }
        if gymId != nil{
            dictionary["gym_id"] = gymId
        }
        if gymName != nil{
            dictionary["gym_name"] = gymName
        }
        if gymProfilePic != nil{
            dictionary["gym_profile_pic"] = gymProfilePic
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if isDeleted != nil{
            dictionary["isDeleted"] = isDeleted
        }
        if joinDate != nil{
            dictionary["join_date"] = joinDate
        }
        if mobile != nil{
            dictionary["mobile"] = mobile
        }
        if registrationFee != nil{
            dictionary["registration_fee"] = registrationFee
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if gym != nil{
            dictionary["gym"] = gym.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        deviceId = aDecoder.decodeObject(forKey: "device_id") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        gym = aDecoder.decodeObject(forKey: "gym") as? Gym
        gymId = aDecoder.decodeObject(forKey: "gym_id") as? String
        gymName = aDecoder.decodeObject(forKey: "gym_name") as? String
        gymProfilePic = aDecoder.decodeObject(forKey: "gym_profile_pic") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        isDeleted = aDecoder.decodeObject(forKey: "isDeleted") as? Bool
        joinDate = aDecoder.decodeObject(forKey: "join_date") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
        registrationFee = aDecoder.decodeObject(forKey: "registration_fee") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if deviceId != nil{
            aCoder.encode(deviceId, forKey: "device_id")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if gender != nil{
            aCoder.encode(gender, forKey: "gender")
        }
        if gym != nil{
            aCoder.encode(gym, forKey: "gym")
        }
        if gymId != nil{
            aCoder.encode(gymId, forKey: "gym_id")
        }
        if gymName != nil{
            aCoder.encode(gymName, forKey: "gym_name")
        }
        if gymProfilePic != nil{
            aCoder.encode(gymProfilePic, forKey: "gym_profile_pic")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if isDeleted != nil{
            aCoder.encode(isDeleted, forKey: "isDeleted")
        }
        if joinDate != nil{
            aCoder.encode(joinDate, forKey: "join_date")
        }
        if mobile != nil{
            aCoder.encode(mobile, forKey: "mobile")
        }
        if registrationFee != nil{
            aCoder.encode(registrationFee, forKey: "registration_fee")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}
