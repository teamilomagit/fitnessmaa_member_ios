//
//  FeedsModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 25, 2019

import Foundation


class FeedsModel : NSObject, NSCoding{

    var createdAt : String!
    var id : String!
    var imageUrl : String!
    var isDeleted : Bool!
    var tag : String!
    var title : String!
    var type : String!
    var updatedAt : String!
    var url : String!
    var no_of_views : String!
    var no_of_likes : String!
    var liked_by_me : String!
    var nutritionist_name : String!
    var posted_by_user_type : String!
    var posted_by_user_id : String!
    var nutritionist_image : String!
    var social_page_name : String!
    var social_page_image : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? String
        id = dictionary["id"] as? String
        imageUrl = dictionary["image_url"] as? String
        isDeleted = dictionary["isDeleted"] as? Bool
        tag = dictionary["tag"] as? String
        title = dictionary["title"] as? String
        type = dictionary["type"] as? String
        updatedAt = dictionary["updated_at"] as? String
        no_of_views = dictionary["no_of_views"] as? String
         no_of_likes = dictionary["no_of_likes"] as? String
         liked_by_me = dictionary["liked_by_me"] as? String
         nutritionist_name = dictionary["nutritionist_name"] as? String
        posted_by_user_type = dictionary["posted_by_user_type"] as? String
        posted_by_user_id = dictionary["posted_by_user_id"] as? String
        nutritionist_image = dictionary["nutritionist_image"] as? String
         social_page_name = dictionary["social_page_name"] as? String
        social_page_image = dictionary["social_page_image"] as? String
        
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if isDeleted != nil{
            dictionary["isDeleted"] = isDeleted
        }
        if tag != nil{
            dictionary["tag"] = tag
        }
        if title != nil{
            dictionary["title"] = title
        }
        if type != nil{
            dictionary["type"] = type
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if url != nil{
            dictionary["url"] = url
        }
        if no_of_views != nil{
            dictionary["no_of_views"] = url
        }
        if no_of_likes != nil{
            dictionary["no_of_likes"] = url
        }
        if nutritionist_name != nil{
            dictionary["nutritionist_name"] = url
        }
        if posted_by_user_type != nil{
            dictionary["posted_by_user_type"] = url
        }
        if liked_by_me != nil{
            dictionary["liked_by_me"] = url
        }
        if posted_by_user_id != nil{
            dictionary["posted_by_user_id"] = url
        }
        if nutritionist_image != nil{
            dictionary["nutritionist_image"] = url
        }
        if social_page_name != nil{
            dictionary["social_page_name"] = url
        }
        if social_page_image != nil{
            dictionary["social_page_image"] = url
        }
        
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        isDeleted = aDecoder.decodeObject(forKey: "isDeleted") as? Bool
        tag = aDecoder.decodeObject(forKey: "tag") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
         no_of_likes = aDecoder.decodeObject(forKey: "no_of_likes") as? String
         no_of_views = aDecoder.decodeObject(forKey: "no_of_views") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if isDeleted != nil{
            aCoder.encode(isDeleted, forKey: "isDeleted")
        }
        if tag != nil{
            aCoder.encode(tag, forKey: "tag")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
    }
}
