//
//  OptionsModel.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on June 1, 2019

import Foundation


class OptionsModel : NSObject, NSCoding{
    
    var isSelected : Bool!
    var tag : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        isSelected = dictionary["isSelected"] as? Bool
        tag = dictionary["tag"] as? String
        isSelected = false
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if isSelected != nil{
            dictionary["isSelected"] = isSelected
        }
        if tag != nil{
            dictionary["tag"] = tag
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        isSelected = aDecoder.decodeObject(forKey: "isSelected") as? Bool
        tag = aDecoder.decodeObject(forKey: "tag") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if isSelected != nil{
            aCoder.encode(isSelected, forKey: "isSelected")
        }
        if tag != nil{
            aCoder.encode(tag, forKey: "tag")
        }
    }
}
