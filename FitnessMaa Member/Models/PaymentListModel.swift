//
//  PaymentListModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 11, 2019

import Foundation


class PaymentListModel : NSObject, NSCoding{

    var id : String!
    var memberId : String!
    var memberName : String!
    var paidAmount : String!
    var paymentDate : String!
    var totalPackageAmount : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        id = dictionary["id"] as? String
        memberId = dictionary["member_id"] as? String
        memberName = dictionary["member_name"] as? String
        paidAmount = dictionary["paid_amount"] as? String
        paymentDate = dictionary["payment_date"] as? String
        totalPackageAmount = dictionary["total_package_amount"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if id != nil{
            dictionary["id"] = id
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if memberName != nil{
            dictionary["member_name"] = memberName
        }
        if paidAmount != nil{
            dictionary["paid_amount"] = paidAmount
        }
        if paymentDate != nil{
            dictionary["payment_date"] = paymentDate
        }
        if totalPackageAmount != nil{
            dictionary["total_package_amount"] = totalPackageAmount
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? String
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        memberName = aDecoder.decodeObject(forKey: "member_name") as? String
        paidAmount = aDecoder.decodeObject(forKey: "paid_amount") as? String
        paymentDate = aDecoder.decodeObject(forKey: "payment_date") as? String
        totalPackageAmount = aDecoder.decodeObject(forKey: "total_package_amount") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if memberName != nil{
            aCoder.encode(memberName, forKey: "member_name")
        }
        if paidAmount != nil{
            aCoder.encode(paidAmount, forKey: "paid_amount")
        }
        if paymentDate != nil{
            aCoder.encode(paymentDate, forKey: "payment_date")
        }
        if totalPackageAmount != nil{
            aCoder.encode(totalPackageAmount, forKey: "total_package_amount")
        }
    }
}