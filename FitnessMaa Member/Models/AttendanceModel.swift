//
//  AttendanceModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 11, 2019

import Foundation


class AttendanceModel : NSObject, NSCoding{

    var createdAt : String!
    var fullName : String!
    var gymId : String!
    var id : String!
    var imageUrl : String!
    var inTime : String!
    var memberId : String!
    var outTime : String!
    var status : String!
    var updatedAt : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        createdAt = dictionary["created_at"] as? String
        fullName = dictionary["full_name"] as? String
        gymId = dictionary["gym_id"] as? String
        id = dictionary["id"] as? String
        imageUrl = dictionary["image_url"] as? String
        inTime = dictionary["in_time"] as? String
        memberId = dictionary["member_id"] as? String
        outTime = dictionary["out_time"] as? String
        status = dictionary["status"] as? String
        updatedAt = dictionary["updated_at"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if gymId != nil{
            dictionary["gym_id"] = gymId
        }
        if id != nil{
            dictionary["id"] = id
        }
        if imageUrl != nil{
            dictionary["image_url"] = imageUrl
        }
        if inTime != nil{
            dictionary["in_time"] = inTime
        }
        if memberId != nil{
            dictionary["member_id"] = memberId
        }
        if outTime != nil{
            dictionary["out_time"] = outTime
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        gymId = aDecoder.decodeObject(forKey: "gym_id") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        imageUrl = aDecoder.decodeObject(forKey: "image_url") as? String
        inTime = aDecoder.decodeObject(forKey: "in_time") as? String
        memberId = aDecoder.decodeObject(forKey: "member_id") as? String
        outTime = aDecoder.decodeObject(forKey: "out_time") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if gymId != nil{
            aCoder.encode(gymId, forKey: "gym_id")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if imageUrl != nil{
            aCoder.encode(imageUrl, forKey: "image_url")
        }
        if inTime != nil{
            aCoder.encode(inTime, forKey: "in_time")
        }
        if memberId != nil{
            aCoder.encode(memberId, forKey: "member_id")
        }
        if outTime != nil{
            aCoder.encode(outTime, forKey: "out_time")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
    }
}