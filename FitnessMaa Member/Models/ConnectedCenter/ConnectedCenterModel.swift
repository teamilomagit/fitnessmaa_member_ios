//
//  ConnectedCenterModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 8, 2019

import Foundation


class ConnectedCenterModel : NSObject, NSCoding{

    var centerId : String!
    var createdAt : String!
    var gym : Gym!
    var id : Int!
    var isDeleted : Bool!
    var joinDate : String!
    var registrationFee : String!
    var status : String!
    var updatedAt : String!
    var userId : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        centerId = dictionary["center_id"] as? String
        createdAt = dictionary["created_at"] as? String
        id = dictionary["id"] as? Int
        isDeleted = dictionary["isDeleted"] as? Bool
        joinDate = dictionary["join_date"] as? String
        registrationFee = dictionary["registration_fee"] as? String
        status = dictionary["status"] as? String
        updatedAt = dictionary["updated_at"] as? String
        userId = dictionary["user_id"] as? String
        if let gymData = dictionary["gym"] as? [String:Any]{
            gym = Gym(fromDictionary: gymData)
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if centerId != nil{
            dictionary["center_id"] = centerId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isDeleted != nil{
            dictionary["isDeleted"] = isDeleted
        }
        if joinDate != nil{
            dictionary["join_date"] = joinDate
        }
        if registrationFee != nil{
            dictionary["registration_fee"] = registrationFee
        }
        if status != nil{
            dictionary["status"] = status
        }
        if updatedAt != nil{
            dictionary["updated_at"] = updatedAt
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if gym != nil{
            dictionary["gym"] = gym.toDictionary()
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        centerId = aDecoder.decodeObject(forKey: "center_id") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        gym = aDecoder.decodeObject(forKey: "gym") as? Gym
        id = aDecoder.decodeObject(forKey: "id") as? Int
        isDeleted = aDecoder.decodeObject(forKey: "isDeleted") as? Bool
        joinDate = aDecoder.decodeObject(forKey: "join_date") as? String
        registrationFee = aDecoder.decodeObject(forKey: "registration_fee") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if centerId != nil{
            aCoder.encode(centerId, forKey: "center_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if gym != nil{
            aCoder.encode(gym, forKey: "gym")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isDeleted != nil{
            aCoder.encode(isDeleted, forKey: "isDeleted")
        }
        if joinDate != nil{
            aCoder.encode(joinDate, forKey: "join_date")
        }
        if registrationFee != nil{
            aCoder.encode(registrationFee, forKey: "registration_fee")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if updatedAt != nil{
            aCoder.encode(updatedAt, forKey: "updated_at")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
    }
}