//
//  UserPreferedLocationModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 30, 2019

import Foundation


class UserPreferedLocationModel : NSObject, NSCoding{

    var cityId : String!
    var cityName : String!
    var countryId : String!
    var countryName : String!
    var stateId : String!
    var stateName : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cityId = dictionary["city_id"] as? String
        cityName = dictionary["city_name"] as? String
        countryId = dictionary["country_id"] as? String
        countryName = dictionary["country_name"] as? String
        stateId = dictionary["state_id"] as? String
        stateName = dictionary["state_name"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cityId != nil{
            dictionary["city_id"] = cityId
        }
        if cityName != nil{
            dictionary["city_name"] = cityName
        }
        if countryId != nil{
            dictionary["country_id"] = countryId
        }
        if countryName != nil{
            dictionary["country_name"] = countryName
        }
        if stateId != nil{
            dictionary["state_id"] = stateId
        }
        if stateName != nil{
            dictionary["state_name"] = stateName
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cityId = aDecoder.decodeObject(forKey: "city_id") as? String
        cityName = aDecoder.decodeObject(forKey: "city_name") as? String
        countryId = aDecoder.decodeObject(forKey: "country_id") as? String
        countryName = aDecoder.decodeObject(forKey: "country_name") as? String
        stateId = aDecoder.decodeObject(forKey: "state_id") as? String
        stateName = aDecoder.decodeObject(forKey: "state_name") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cityId != nil{
            aCoder.encode(cityId, forKey: "city_id")
        }
        if cityName != nil{
            aCoder.encode(cityName, forKey: "city_name")
        }
        if countryId != nil{
            aCoder.encode(countryId, forKey: "country_id")
        }
        if countryName != nil{
            aCoder.encode(countryName, forKey: "country_name")
        }
        if stateId != nil{
            aCoder.encode(stateId, forKey: "state_id")
        }
        if stateName != nil{
            aCoder.encode(stateName, forKey: "state_name")
        }
    }
}