//
//  OfferDetailModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 31, 2019

import Foundation


class OfferDetailModel : NSObject, NSCoding{

    var address : AnyObject!
    var contactNo : AnyObject!
    var endDate : String!
    var id : String!
    var image : String!
    var moreInfoLink : String!
    var priority : String!
    var startDate : String!
    var termsOfUse : AnyObject!
    var title : String!
     var details : String!
    var totalInterestedPeople : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        address = dictionary["address"] as? AnyObject
        contactNo = dictionary["contact_no"] as? AnyObject
        endDate = dictionary["end_date"] as? String
        id = dictionary["id"] as? String
        image = dictionary["image"] as? String
        moreInfoLink = dictionary["more_info_link"] as? String
        priority = dictionary["priority"] as? String
        startDate = dictionary["start_date"] as? String
        termsOfUse = dictionary["terms_of_use"] as? AnyObject
        title = dictionary["title"] as? String
        details = dictionary["details"] as? String
        totalInterestedPeople = dictionary["total_interested_people"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if contactNo != nil{
            dictionary["contact_no"] = contactNo
        }
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if moreInfoLink != nil{
            dictionary["more_info_link"] = moreInfoLink
        }
        if priority != nil{
            dictionary["priority"] = priority
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if termsOfUse != nil{
            dictionary["terms_of_use"] = termsOfUse
        }
        if title != nil{
            dictionary["title"] = title
        }
        if details != nil{
            dictionary["details"] = details
        }
        if totalInterestedPeople != nil{
            dictionary["total_interested_people"] = totalInterestedPeople
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? AnyObject
        contactNo = aDecoder.decodeObject(forKey: "contact_no") as? AnyObject
        endDate = aDecoder.decodeObject(forKey: "end_date") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        moreInfoLink = aDecoder.decodeObject(forKey: "more_info_link") as? String
        priority = aDecoder.decodeObject(forKey: "priority") as? String
        startDate = aDecoder.decodeObject(forKey: "start_date") as? String
        termsOfUse = aDecoder.decodeObject(forKey: "terms_of_use") as? AnyObject
        title = aDecoder.decodeObject(forKey: "title") as? String
        totalInterestedPeople = aDecoder.decodeObject(forKey: "total_interested_people") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if contactNo != nil{
            aCoder.encode(contactNo, forKey: "contact_no")
        }
        if endDate != nil{
            aCoder.encode(endDate, forKey: "end_date")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if moreInfoLink != nil{
            aCoder.encode(moreInfoLink, forKey: "more_info_link")
        }
        if priority != nil{
            aCoder.encode(priority, forKey: "priority")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "start_date")
        }
        if termsOfUse != nil{
            aCoder.encode(termsOfUse, forKey: "terms_of_use")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if totalInterestedPeople != nil{
            aCoder.encode(totalInterestedPeople, forKey: "total_interested_people")
        }
    }
}
