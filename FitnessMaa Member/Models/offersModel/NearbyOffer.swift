//
//  NearbyOffer.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 24, 2019

import Foundation


class NearbyOffer : NSObject, NSCoding{

    var endDate : String!
    var id : String!
    var image : String!
    var priority : String!
    var startDate : String!
    var title : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        endDate = dictionary["end_date"] as? String
        id = dictionary["id"] as? String
        image = dictionary["image"] as? String
        priority = dictionary["priority"] as? String
        startDate = dictionary["start_date"] as? String
        title = dictionary["title"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if endDate != nil{
            dictionary["end_date"] = endDate
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if priority != nil{
            dictionary["priority"] = priority
        }
        if startDate != nil{
            dictionary["start_date"] = startDate
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        endDate = aDecoder.decodeObject(forKey: "end_date") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        priority = aDecoder.decodeObject(forKey: "priority") as? String
        startDate = aDecoder.decodeObject(forKey: "start_date") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if endDate != nil{
            aCoder.encode(endDate, forKey: "end_date")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if priority != nil{
            aCoder.encode(priority, forKey: "priority")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "start_date")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
    }
}