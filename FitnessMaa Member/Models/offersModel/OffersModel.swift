//
//  OffersModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 24, 2019

import Foundation


class OffersModel : NSObject, NSCoding{

    var cityOffers : [CityOffer]!
    var nearbyOffers : [NearbyOffer]!
    var topOffers : [TopOffer]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        cityOffers = [CityOffer]()
        if let cityOffersArray = dictionary["city_offers"] as? [[String:Any]]{
            for dic in cityOffersArray{
                let value = CityOffer(fromDictionary: dic)
                cityOffers.append(value)
            }
        }
        nearbyOffers = [NearbyOffer]()
        if let nearbyOffersArray = dictionary["nearby_offers"] as? [[String:Any]]{
            for dic in nearbyOffersArray{
                let value = NearbyOffer(fromDictionary: dic)
                nearbyOffers.append(value)
            }
        }
        topOffers = [TopOffer]()
        if let topOffersArray = dictionary["top_offers"] as? [[String:Any]]{
            for dic in topOffersArray{
                let value = TopOffer(fromDictionary: dic)
                topOffers.append(value)
            }
        }
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cityOffers != nil{
            var dictionaryElements = [[String:Any]]()
            for cityOffersElement in cityOffers {
                dictionaryElements.append(cityOffersElement.toDictionary())
            }
            dictionary["cityOffers"] = dictionaryElements
        }
        if nearbyOffers != nil{
            var dictionaryElements = [[String:Any]]()
            for nearbyOffersElement in nearbyOffers {
                dictionaryElements.append(nearbyOffersElement.toDictionary())
            }
            dictionary["nearbyOffers"] = dictionaryElements
        }
        if topOffers != nil{
            var dictionaryElements = [[String:Any]]()
            for topOffersElement in topOffers {
                dictionaryElements.append(topOffersElement.toDictionary())
            }
            dictionary["topOffers"] = dictionaryElements
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        cityOffers = aDecoder.decodeObject(forKey: "city_offers") as? [CityOffer]
        nearbyOffers = aDecoder.decodeObject(forKey: "nearby_offers") as? [NearbyOffer]
        topOffers = aDecoder.decodeObject(forKey: "top_offers") as? [TopOffer]
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if cityOffers != nil{
            aCoder.encode(cityOffers, forKey: "city_offers")
        }
        if nearbyOffers != nil{
            aCoder.encode(nearbyOffers, forKey: "nearby_offers")
        }
        if topOffers != nil{
            aCoder.encode(topOffers, forKey: "top_offers")
        }
    }
}