//
//  GeneralSettingModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 11, 2019

import Foundation


class GeneralSettingModel : NSObject, NSCoding{

    var defualtCityId : Int!
    var defualtCityName : String!
    var defualtCountryId : Int!
    var defualtCountryName : String!
    var defualtStateId : Int!
    var defualtStateName : String!
    var supportEmail : String!
    var supportMobile : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        defualtCityId = dictionary["defualt_city_id"] as? Int
        defualtCityName = dictionary["defualt_city_name"] as? String
        defualtCountryId = dictionary["defualt_country_id"] as? Int
        defualtCountryName = dictionary["defualt_country_name"] as? String
        defualtStateId = dictionary["defualt_state_id"] as? Int
        defualtStateName = dictionary["defualt_state_name"] as? String
        supportEmail = dictionary["support_email"] as? String
        supportMobile = dictionary["support_mobile"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if defualtCityId != nil{
            dictionary["defualt_city_id"] = defualtCityId
        }
        if defualtCityName != nil{
            dictionary["defualt_city_name"] = defualtCityName
        }
        if defualtCountryId != nil{
            dictionary["defualt_country_id"] = defualtCountryId
        }
        if defualtCountryName != nil{
            dictionary["defualt_country_name"] = defualtCountryName
        }
        if defualtStateId != nil{
            dictionary["defualt_state_id"] = defualtStateId
        }
        if defualtStateName != nil{
            dictionary["defualt_state_name"] = defualtStateName
        }
        if supportEmail != nil{
            dictionary["support_email"] = supportEmail
        }
        if supportMobile != nil{
            dictionary["support_mobile"] = supportMobile
        }
        return dictionary
    }

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        defualtCityId = aDecoder.decodeObject(forKey: "defualt_city_id") as? Int
        defualtCityName = aDecoder.decodeObject(forKey: "defualt_city_name") as? String
        defualtCountryId = aDecoder.decodeObject(forKey: "defualt_country_id") as? Int
        defualtCountryName = aDecoder.decodeObject(forKey: "defualt_country_name") as? String
        defualtStateId = aDecoder.decodeObject(forKey: "defualt_state_id") as? Int
        defualtStateName = aDecoder.decodeObject(forKey: "defualt_state_name") as? String
        supportEmail = aDecoder.decodeObject(forKey: "support_email") as? String
        supportMobile = aDecoder.decodeObject(forKey: "support_mobile") as? String
    }

    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if defualtCityId != nil{
            aCoder.encode(defualtCityId, forKey: "defualt_city_id")
        }
        if defualtCityName != nil{
            aCoder.encode(defualtCityName, forKey: "defualt_city_name")
        }
        if defualtCountryId != nil{
            aCoder.encode(defualtCountryId, forKey: "defualt_country_id")
        }
        if defualtCountryName != nil{
            aCoder.encode(defualtCountryName, forKey: "defualt_country_name")
        }
        if defualtStateId != nil{
            aCoder.encode(defualtStateId, forKey: "defualt_state_id")
        }
        if defualtStateName != nil{
            aCoder.encode(defualtStateName, forKey: "defualt_state_name")
        }
        if supportEmail != nil{
            aCoder.encode(supportEmail, forKey: "support_email")
        }
        if supportMobile != nil{
            aCoder.encode(supportMobile, forKey: "support_mobile")
        }
    }
}